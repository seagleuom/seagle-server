select nvm.metricValue_id, mv.value, m.name, nvm.node_id, v.name from node_version_metric nvm
join metric_value mv on nvm.metricValue_id = mv.id
join metric m on mv.metric_id = m.id
join version v on nvm.version_id = v.id
where node_id = 32