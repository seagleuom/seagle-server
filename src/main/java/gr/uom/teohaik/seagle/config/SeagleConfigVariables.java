package gr.uom.teohaik.seagle.config;

/**
 *
 * @author Theodore Chaikalis
 */

public class SeagleConfigVariables {
    
    /**
    * The default value of seagle name
    * <p>
    */
   public static final String SEAGLE_NAME = "seagle";

   /**
    * The default location of configuration.
    * <p>
    */
   public static final String SEAGLE_CONFIG_FOLDER_NAME = "config";
   /**
    * The seagle domain, where most seagle properties will be stored.
    * <p>
    */
   public static final String SEAGLE_DOMAIN = "seagle_domain";

   /**
    * The name of the property of the seagle home folder.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String SEAGLE_HOME_PROPERTY = "seagleHome";


   /**
    * The name of the property of the data home folder.
    * <p>
    * The data property should be relative to seagle home, and not an absolute
    * path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String DATA_HOME_PROPERTY = "dataHome";

   /**
    * The default value of data home in case there is not a configuration.
    * <p>
    */
   public static final String DATA_HOME = "data";

   /**
    * The name of the property of the projects home folder.
    * <p>
    * Projects home is relative to data home, and shouldn't be absolute path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String PROJECTS_HOME_PROPERTY = "projectsHome";

   /**
    * The default value of projects home in case there is not a configuration.
    * <p>
    */
   public static final String PROJECTS_HOME = "projects";

   /**
    * The name of the property of the repositories home folder.
    * <p>
    * Repositories home is relative to projects home, and shouldn't be absolute
    * path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String REPOS_HOME_PROPERTY = "reposHome";

   /**
    * The default value of repositories home in case there is not a
    * configuration.
    * <p>
    */
   public static final String REPOS_HOME = "repositories";

   /**
    * The name of the property of the checked out projects home folder.
    * <p>
    * Projects sources home is relative to projects home, and shouldn't be
    * absolute path.
    * <p>
    * Use {@link ConfigManager} to get the value of home folder.
    */
   public static final String PROJECTS_SOURCE_CODE_HOME_PROPERTY = "projectSourceCodeHome";

   /**
    * The default value of projects source home in case there is not a
    * configuration.
    * <p>
    */
   public static final String PROJECTS_SOURCE_CODE_HOME = "projectsSourceCode";
   
    /**
    * Smell detection threshold FEW for calculating smell metrics
    * <p>
    */
   public static final String SMELL_DETECTOR_FEW_VALUE = "smellDetectionFew";
   
    /**
    * Smell detection threshold MANY for calculating smell metrics
    * <p>
    */
   public static final String SMELL_DETECTOR_MANY_VALUE = "smellDetectionMany";
   
    /**
    * Smell detection threshold SEVERAL for calculating smell metrics
    * <p>
    */
   public static final String SMELL_DETECTOR_SEVERAL_VALUE = "smellDetectionSeveral";
   
   
    /**
    * Smell detection threshold for Feature Envy LAA Limit for identifying Feature Envy smells
    * <p>
    */
   public static final String SMELL_DETECTOR_FE_LAA_LIMIT = "featureEnvyLAALevel";
   
}
