package gr.uom.teohaik.seagle.config;

import org.apache.deltaspike.core.api.config.PropertyFileConfig;

/**
 *
 * @author Theodore Chaikalis
 */
public class SeaglePropertyConfigFile implements PropertyFileConfig {

    @Override
    public String getPropertyFileName() {
        return "seagle.properties";
    }

    @Override
    public boolean isOptional() {
        return false;
    }

}
