package gr.uom.teohaik.seagle.services;

import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.util.exception.SeagleException;
import gr.uom.teohaik.seagle.v3.analysis.metrics.graph.AbstractGraphExecutableMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.AbstractJavaExecutableMetric;
import gr.uom.teohaik.seagle.v3.metric.api.ExecutableMetric;
import gr.uom.teohaik.seagle.v3.model.repositories.MetricCategoryRepository;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class ExecutableMetricService {
    
     @Inject
    @SeagleLogger
    org.slf4j.Logger logger;

    @Inject
    BeanManager beanManager;
    
    @Inject
    MetricCategoryRepository metricCategoryRepository;
    
    @EJB
    private MetricService metricService;
    
        
    public AbstractJavaExecutableMetric getSourceCodeMetric(String metricImplementationClassName){
          Set<Bean<?>> beans = beanManager.getBeans(AbstractJavaExecutableMetric.class, new AnnotationLiteral<SourceCodeMetric>(){});
        
        AbstractJavaExecutableMetric metric = null;
        
        for(Bean<?>  bean : beans){
          if(  AbstractJavaExecutableMetric.class.isAssignableFrom(bean.getBeanClass())){
              if(bean.getBeanClass().getName().equals(metricImplementationClassName)){
                  try {
                      metric = (AbstractJavaExecutableMetric)bean.getBeanClass().newInstance();
                      return metric;
                  } catch (InstantiationException | IllegalAccessException ex) {
                      logger.error(null, ex);
                  }
              }
          }
        }
        if(metric == null) throw new SeagleException("Cound not found implementation for metric "+metricImplementationClassName);
        return metric;
    }
    
    public AbstractGraphExecutableMetric getGraphMetric(String metricImplementationClassName){
          Set<Bean<?>> beans = beanManager.getBeans(AbstractGraphExecutableMetric.class, new AnnotationLiteral<GraphMetric>(){});
        
        AbstractGraphExecutableMetric metric = null;
        
        for(Bean<?>  bean : beans){
          if(  AbstractGraphExecutableMetric.class.isAssignableFrom(bean.getBeanClass())){
              if(bean.getBeanClass().getName().equals(metricImplementationClassName)){
                  try {
                      metric = (AbstractGraphExecutableMetric)bean.getBeanClass().newInstance();
                      return metric;
                  } catch (InstantiationException | IllegalAccessException ex) {
                      logger.error(null, ex);
                  }
              }
          }
        }
        if(metric == null) throw new SeagleException("Cound not found implementation for metric "+metricImplementationClassName);
        return metric;
    }
}
