package gr.uom.teohaik.seagle.services;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.events.AnalysisCompletedEvent;
import gr.uom.teohaik.seagle.v3.events.CloneFinishedEvent;
import gr.uom.teohaik.seagle.v3.events.MetricSavingFinishedEvent;
import gr.uom.teohaik.seagle.v3.events.RequestForWorkEvent;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectTimeline;
import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import gr.uom.teohaik.seagle.v3.ws.rest.services.AnalysisRestService;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.ObservesAsync;
import javax.inject.Inject;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class GenericEventHandlerService {

    @Inject
    @SeagleLogger
    private Logger logger;

    @Inject
    private ProjectService projectService;

    @Inject
    private VersionService versionService;
    
    @Inject
    private Event<AnalysisCompletedEvent> analysisCompletedEvent;

    public void catchClonedFinishedEvent(@Observes CloneFinishedEvent cfe) {
        logger.debug("CloneFinishedEvent caught!");
        Project p = projectService.findProjectByRepoUrl(cfe.getProjectUrl());
        ProjectTimeline pt = new ProjectTimeline();
        pt.setActionType(ActionType.CLONED);
        p.addTimeLine(pt);
    }

    public void catchAnalysisFinishedEvent(@Observes AnalysisCompletedEvent ace) {
        logger.info("AnalysisCompletedEvent caught!");
        logger.info("Analysis and metric calculation completed for project {}", ace.getProject().getName());
        Project p = projectService.findProjectById(ace.getProject().getId());
        ProjectTimeline pt = new ProjectTimeline();
        pt.setActionType(ActionType.ANALYSED);
        p.addTimeLine(pt);
        projectService.save(p);
        versionService.setAnalyzedVersions(ace.getProject().getVersions());
    }

    public void catchMetricSavedFinishedEvent(@Observes MetricSavingFinishedEvent msfe) {
        logger.debug("MetricSavingFinishedEvent caught!");
        analysisCompletedEvent.fire(new AnalysisCompletedEvent(msfe.getProject()));
    }

    public void catchRequestForWork(@ObservesAsync RequestForWorkEvent we) {
        int workType = we.getWorkType();
        if (workType == 1) {
            logger.debug("RequestForWork 1 caught!");
            work1();
        } else if (workType == 2) {
            logger.debug("RequestForWork 2 caught!");
            work2();
        }
    }

    public void work1() {
        logger.debug("start to do work 1");
        try {
            Thread.sleep(8000);
            logger.debug("work 1 done!");
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(AnalysisRestService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void work2() {
        logger.debug("start to do work 2");
        try {
            Thread.sleep(2000);
            logger.debug("work 2 done!");
        } catch (InterruptedException ex) {
            java.util.logging.Logger.getLogger(AnalysisRestService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
