//package gr.uom.teohaik.seagle.services;
//
//import gr.uom.teohaik.seagle.util.SeagleLogger;
//import gr.uom.teohaik.seagle.v3.events.DBStorageRequest;
//import gr.uom.teohaik.seagle.v3.model.entities.Project;
//import gr.uom.teohaik.seagle.v3.model.entities.Version;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.nio.file.Path;
//import java.util.List;
//import java.util.stream.Collectors;
//import javax.ejb.EJB;
//import javax.ejb.Stateless;
//import javax.enterprise.event.Observes;
//import javax.inject.Inject;
//import org.apache.commons.io.IOUtils;
//import org.eclipse.jgit.api.*;
//import org.eclipse.jgit.api.errors.GitAPIException;
//import org.eclipse.jgit.blame.BlameResult;
//import org.eclipse.jgit.diff.DiffEntry;
//import org.eclipse.jgit.diff.DiffFormatter;
//import org.eclipse.jgit.lib.ObjectId;
//import org.eclipse.jgit.lib.ObjectLoader;
//import org.eclipse.jgit.lib.ObjectReader;
//import org.eclipse.jgit.lib.Repository;
//import org.eclipse.jgit.revwalk.RevCommit;
//import org.eclipse.jgit.revwalk.RevTree;
//import org.eclipse.jgit.revwalk.RevWalk;
//import org.eclipse.jgit.treewalk.AbstractTreeIterator;
//import org.eclipse.jgit.treewalk.CanonicalTreeParser;
//import org.eclipse.jgit.treewalk.TreeWalk;
//import org.eclipse.jgit.treewalk.filter.PathFilter;
//import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
//import org.slf4j.Logger;
//
///**
// *
// * @author Theodore Chaikalis
// */
//public class GitAnalyzer {
//
//    @Inject
//    @SeagleLogger
//    private Logger logger;
//
//    @EJB
//    private VersionService versionService;
//
//    @EJB
//    private RepositoryService repositoryService;
//
//    public void gitDiffExploration( DBStorageRequest dbStorageRequest) {
//        Project project = dbStorageRequest.getProject();
//        Path repositoryFolder = repositoryService.getRepositoryFolder(project.getRemoteRepoPath());
//
//        try (Git git = Git.init().setDirectory(repositoryFolder.toFile()).call()) {
//
//            List<Version> versions = project.getVersions().stream().collect(Collectors.toList());
//
//            int i = 0;
//            while (i < versions.size() - 1) {
//                Version v1 = versions.get(i);
//                Version v2 = versions.get(i + 1);
//                String commit1 = v1.getCommitID();
//                String commit2 = v2.getCommitID();
//                logger.debug("Comparing versions {} ({}) and {} ({})", v1.getName(), commit1, v2.getName(), commit2);
//
//                AbstractTreeIterator oldTreeParser = prepareTreeParser(git.getRepository(), commit1);
//                AbstractTreeIterator newTreeParser = prepareTreeParser(git.getRepository(), commit2);
//
//                List<DiffEntry> diffEntries = git.diff()
//                        .setOldTree(oldTreeParser)
//                        .setNewTree(newTreeParser)
//                        .setPathFilter(PathSuffixFilter.create(".java"))
//                        .call();
//
//                for (DiffEntry entry : diffEntries) {
//                    logger.debug("Entry: " + entry + ", from: " + entry.getOldId() + ", to: " + entry.getNewId());
//                    
//                    String filePath = entry.getNewPath();
//
//                    try (DiffFormatter formatter = new DiffFormatter(System.out)) {
//                        formatter.setRepository(git.getRepository());
//                        formatter.format(entry);
//                    }
//                    
//                    
//                    showBlamedLines(git.getRepository(), commit2, filePath);
//                    
//                }
//                i++;
//            }
//
//        } catch (GitAPIException | IOException ex) {
//            logger.error("git error ", ex);
//        }
//    }
//
//    private void showBlamedLines(Repository repository, String commitID, String filePath) throws GitAPIException, IOException, GitAPIException {
//        logger.debug("Showing blamed lines for commit {} , file {}",commitID, filePath);
//        
//        BlameCommand blamer = new BlameCommand(repository);
//        ObjectId commitObject = ObjectId.fromString(commitID);
//        blamer.setStartCommit(commitObject);
//        blamer.setFilePath(filePath);
//        BlameResult blame = blamer.call();
//
//        // read the number of lines from the given revision, this excludes changes from the last two commits due to the "~~" above
//        int lines = countLinesOfFileInCommit(repository, commitObject, filePath);
//        for (int i = 0; i < lines; i++) {
//            RevCommit commit = blame.getSourceCommit(i);
//            logger.debug("Line: {} commit ID: {}", (i+1), commit.name());
//        }
//
//        logger.debug("Displayed commits responsible for {} lines ", lines);
//    }
//
//    private static AbstractTreeIterator prepareTreeParser(Repository repository, String commitID) throws IOException {
//        // from the commit we can build the tree which allows us to construct the TreeParser
//        //noinspection Duplicates
//        try (RevWalk walk = new RevWalk(repository)) {
//            RevCommit commit = walk.parseCommit(ObjectId.fromString(commitID));
//            RevTree tree = walk.parseTree(commit.getTree().getId());
//
//            CanonicalTreeParser treeParser = new CanonicalTreeParser();
//            try (ObjectReader reader = repository.newObjectReader()) {
//                treeParser.reset(reader, tree.getId());
//            }
//            walk.dispose();
//            return treeParser;
//        }
//    }
//
//    private static int countLinesOfFileInCommit(Repository repository, ObjectId commitID, String name) throws IOException {
//        try (RevWalk revWalk = new RevWalk(repository)) {
//            RevCommit commit = revWalk.parseCommit(commitID);
//            RevTree tree = commit.getTree();
//
//            // now try to find a specific file
//            try (TreeWalk treeWalk = new TreeWalk(repository)) {
//                treeWalk.addTree(tree);
//                treeWalk.setRecursive(true);
//                treeWalk.setFilter(PathFilter.create(name));
//                if (!treeWalk.next()) {
//                    throw new IllegalStateException("Did not find expected file " + name);
//                }
//
//                ObjectId objectId = treeWalk.getObjectId(0);
//                ObjectLoader loader = repository.open(objectId);
//
//                // load the content of the file into a stream
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                loader.copyTo(stream);
//
//                revWalk.dispose();
//
//                return IOUtils.readLines(new ByteArrayInputStream(stream.toByteArray()), "UTF-8").size();
//            }
//        }
//    }
//}
