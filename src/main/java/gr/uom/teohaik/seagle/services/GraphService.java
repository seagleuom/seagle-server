package gr.uom.teohaik.seagle.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.GraphFactory;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph.JavaDemeterLawEdgeCreationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph.JavaGraphFactory;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import gr.uom.teohaik.seagle.v3.model.entities.Node;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.model.entities.VersionGraph;
import gr.uom.teohaik.seagle.v3.model.repositories.NodeRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.VersionGraphRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.VersionRepository;
import gr.uom.teohaik.seagle.v3.ws.rest.model.NodeDTO;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class GraphService {

    @Inject
    private NodeRepository nodeRepository;

    @Inject
    private VersionRepository versionRepository;
    
    @Inject
    private VersionGraphRepository versionGraphRepository;
    
    @Inject
    @SeagleLogger
    Logger logger;
    
    
    public List<Node> findNodesByVersion(Long versionId){
        return nodeRepository.findByVersion_id(versionId);
    }

    public List<NodeDTO> findNodesDTOByVersion(Long versionId){
        return nodeRepository.findNodesByVersion(versionId);
    }

    public void persistGraphNodes(SoftwareGraph<AbstractNode, AbstractEdge> graph) {
        // List<Node> persistedGraphNodes = nodeRepository.findByVersion(graph.getVersion().getId());
        //Map<String, Node> nodeMap = Node.nodeListToTreeMap(persistedGraphNodes);
        Optional<Version> optionalVersion = versionRepository.findOptionalById(graph.getVersion().getVersionDbId());
        if (optionalVersion.isPresent()) {
            Version version = optionalVersion.get();
            for (AbstractNode graphNode : graph.getVertices()) {
                Node dbNode = new Node();
                //  if (!nodeMap.containsKey(node.getName())) {
                String name = graphNode.getName();
                dbNode.setName(name);
                dbNode.setSimpleName(name.substring(name.lastIndexOf(".") + 1, name.length()));
                dbNode.setVersion(version);
                dbNode.setSourceFilePath(graphNode.getSourceFilePath());
                graphNode.setDbNode(dbNode);
                nodeRepository.persist(dbNode);
            }
        }
    }

    public void persistJsonGraph(SoftwareGraph<AbstractNode, AbstractEdge> graph) {
        Optional<Version> optionalById = versionRepository.findOptionalById(graph.getVersion().getVersionDbId());
        if(optionalById.isPresent()){
            VersionGraph versionGraph = new VersionGraph(optionalById.get(), getGraphAsJson(graph));
            versionGraphRepository.persist(versionGraph);
        }
        else {
            logger.warn("Version {} does not exist", graph.getVersion());
        }
    }

    public String getGraphAsJson(SoftwareGraph graph){
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = "";
        try {
            jsonInString = mapper.writeValueAsString(graph);
        } catch (JsonProcessingException ex) {
            logger.error("Error in json building",ex);
        }
        return jsonInString;
    }


    public SoftwareGraph<AbstractNode, AbstractEdge> createJavaGraph(SystemObject systemObject, JavaProject javaProject) throws Exception {
        GraphFactory graphFactory = new JavaGraphFactory(systemObject, javaProject);
        SoftwareGraph<AbstractNode, AbstractEdge> systemGraph = graphFactory.getSystemGraph(new JavaDemeterLawEdgeCreationStrategy());
        if (systemGraph == null) {
            throw new RuntimeException("Graph for version " + javaProject.getProjectVersion() + " is null");
        }
        systemGraph.setVersion(javaProject.getProjectVersion());
        persistGraphNodes(systemGraph);
        persistJsonGraph(systemGraph);
        return systemGraph;
    }

}
