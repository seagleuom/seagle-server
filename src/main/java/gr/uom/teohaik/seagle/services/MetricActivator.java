package gr.uom.teohaik.seagle.services;

import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.graph.*;
import gr.uom.teohaik.seagle.v3.analysis.metrics.graph.evolutionary.NodeAge;
import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.*;
import gr.uom.teohaik.seagle.v3.model.entities.Metric;
import gr.uom.teohaik.seagle.v3.model.entities.MetricCategory;
import gr.uom.teohaik.seagle.v3.model.repositories.MetricCategoryRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.MetricRepository;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Inject;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Singleton
@Startup
public class MetricActivator {

    @Inject
    @SeagleLogger
    Logger logger;

    @Inject
    MetricRepository metricRepository;

    @Inject
    MetricCategoryRepository metricCategoryRepository;

    @Inject
    BeanManager beanManager;

    private final Map<String, AbstractAnalysisMetric> systemRegisteredGraphMetrics = new LinkedHashMap<>();
    private final Map<String, AbstractAnalysisMetric> systemRegisteredSourceCodeMetrics = new LinkedHashMap<>();
    private final Map<String, Metric> systemRegisteredMetrics = new LinkedHashMap<>();
    

    @PostConstruct
    public void createSystemMetrics() {
        createMetricCategories();
        createGraphMetrics();
        createSourceCodeMetrics();
    }

    private void createMetricCategories() {
        for (MetricCategory.CATEGORY_NAMES catName : MetricCategory.CATEGORY_NAMES.values()) {
            Optional<MetricCategory> optionalCategory = metricCategoryRepository.findOptionalByCategoryName(catName.name());
            if (!optionalCategory.isPresent()) {
                MetricCategory category = new MetricCategory();
                category.setCategoryName(catName.name());
                category.setDescription(catName.name());
                metricCategoryRepository.save(category);
            }
        }
    }

    private void createSourceCodeMetrics() {

        Set<Bean<?>> beans = beanManager.getBeans(AbstractJavaExecutableMetric.class, new AnnotationLiteral<SourceCodeMetric>() {
        });
        MetricCategory metricCategory = metricCategoryRepository.
                findOptionalByCategoryName(MetricCategory.CATEGORY_NAMES.SourceCodeMetrics.name()).get();

        for (Bean<?> bean : beans) {
            if (AbstractJavaExecutableMetric.class.isAssignableFrom(bean.getBeanClass())) {
                try {

                    AbstractJavaExecutableMetric metric = (AbstractJavaExecutableMetric) bean.getBeanClass().newInstance();
                    String mnemonic = metric.getMnemonic();

                    checkAndCreateMetricIfNeeded(metric, metricCategory);
                    systemRegisteredSourceCodeMetrics.put(mnemonic, metric);

                } catch (InstantiationException | IllegalAccessException ex) {
                    logger.error(" ", ex);
                }
            }
        }

    }

    private void createGraphMetrics() {
        Set<Bean<?>> beans = beanManager.getBeans(AbstractGraphExecutableMetric.class, new AnnotationLiteral<GraphMetric>() {
        });

        MetricCategory metricCategory = metricCategoryRepository.
                findOptionalByCategoryName(MetricCategory.CATEGORY_NAMES.GraphMetrics.name()).get();

        for (Bean<?> bean : beans) {
            if (AbstractGraphExecutableMetric.class.isAssignableFrom(bean.getBeanClass())) {
                try {
                    AbstractGraphExecutableMetric metric = (AbstractGraphExecutableMetric) bean.getBeanClass().newInstance();
                    String mnemonic = metric.getMnemonic();

                    checkAndCreateMetricIfNeeded(metric, metricCategory);

                    systemRegisteredGraphMetrics.put(mnemonic, metric);

                } catch (InstantiationException | IllegalAccessException ex) {
                    logger.error(" ", ex);
                }
            }
        }

//        systemRegisteredGraphMetrics.put(AverageInDegree.MNEMONIC, new AverageInDegree());
//        systemRegisteredGraphMetrics.put(ExistingNodesPerVersion.MNEMONIC, new ExistingNodesPerVersion());
//        systemRegisteredGraphMetrics.put(DeletedNodesPerVersion.MNEMONIC, new DeletedNodesPerVersion());
//        systemRegisteredGraphMetrics.put(NewNodesPerVersion.MNEMONIC, new NewNodesPerVersion());
//        systemRegisteredGraphMetrics.put(EdgesToExistingNodes.MNEMONIC, new EdgesToExistingNodes());
    }

    private void checkAndCreateMetricIfNeeded(AbstractAnalysisMetric metric, MetricCategory metricCategory) {
        Optional<Metric> optionalMetric = metricRepository.findOptionalByMnemonic(metric.getMnemonic());
        if (!optionalMetric.isPresent()) {
            Metric dbMetric = new Metric();
            dbMetric.setName(metric.getName());
            dbMetric.setMnemonic(metric.getMnemonic());
            dbMetric.setDescription(metric.getDescription());
            dbMetric.setCategory(metricCategory);
            dbMetric.setImplementationClass(metric.getClass().getName());
            logger.info("Loading up metric {} - {}", metric.getMnemonic(), metric.getName());
                
            metricRepository.save(dbMetric);
            systemRegisteredMetrics.put(metric.getMnemonic(), dbMetric);
        }
        else{
            systemRegisteredMetrics.put(metric.getMnemonic(), optionalMetric.get());
        }

    }

    public Set<String> getMetricMnemonics() {
        return systemRegisteredMetrics.keySet();
    }

    public Metric getMetric(String mnemonic) {
        return systemRegisteredMetrics.get(mnemonic);
    }

    /*
     Age Updater needs special handling because it must be the first updater to run. 
     So it must be provided separately from the other metrics
     */
    public AbstractAnalysisMetric getAgeUpdater() {
        return systemRegisteredGraphMetrics.get(NodeAge.MNEMONIC);
    }

    public Collection<AbstractAnalysisMetric> getCreatedGraphMetrics() {
        return systemRegisteredGraphMetrics.values();
    }

    public Collection<AbstractAnalysisMetric> getCreatedSourceCodeMetrics() {
        return systemRegisteredSourceCodeMetrics.values();
    }

}
