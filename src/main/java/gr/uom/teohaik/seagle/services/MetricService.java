package gr.uom.teohaik.seagle.services;

import static gr.uom.teohaik.seagle.v3.SeagleConstants.HIBERNATE_BATCH_SIZE;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import gr.uom.teohaik.seagle.v3.model.entities.Method;
import gr.uom.teohaik.seagle.v3.model.entities.Metric;
import gr.uom.teohaik.seagle.v3.model.entities.MetricValue;
import gr.uom.teohaik.seagle.v3.model.entities.Node;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectMetric;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectVersionMetric;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.model.repositories.MetricRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.MetricValueRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.NodeRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectMetricRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectVersionMetricRepository;
import gr.uom.teohaik.seagle.v3.ws.rest.model.MetricValueDTO;
import gr.uom.teohaik.seagle.v3.ws.rest.model.NodeDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class MetricService {

    @Inject
    @SeagleLogger
    private Logger logger;

    @Inject
    private VersionService versionService;

    @Inject
    private GraphService graphService;

    @Inject
    private MetricRepository metricRepository;

    @Inject
    private ProjectVersionMetricRepository projectVersionMetricRepository;

    @Inject
    private ProjectMetricRepository projectMetricRepository;

    @Inject
    private MetricValueRepository metricValueRepository;

    @Inject
    private NodeRepository nodeRepository;

    @Inject
    private EntityManager em;

    @PostConstruct
    public void init() {
    }

    @org.jboss.ejb3.annotation.TransactionTimeout(value=45, unit= TimeUnit.MINUTES)
    public void handleNodeLevelMetrics(
            SoftwareProject softwareProject) throws RuntimeException {

        logger.info("Handling node level metrics for all classes");

        Version version = versionService.findVersionByCommitId(softwareProject.getProjectVersion().getCommitHashId());
        Map<String, Map<String, Double>> metricValuesNodeLevel = softwareProject.getMetricValuesNodeLevel();
        Map<String, Set<Method>> metricValuesForMethods = softwareProject.getMetricValuesForMethods();
        List<Node> persistedGraphNodes = graphService.findNodesByVersion(version.getId());
        Map<String, Node> graphNodesMap = Node.nodeListToTreeMap(persistedGraphNodes);
        logger.info("Graph nodes size = {}",graphNodesMap.keySet().size());
        logger.info("Methods nodes size = {}",graphNodesMap.keySet().size());

        int[] counter = new int[1];
        for (String className : graphNodesMap.keySet()) {
            Node node = graphNodesMap.get(className);
            if (node != null) {
                handleNodeMetrics(metricValuesNodeLevel, node, version.getId(), counter);


//                if (metricValuesForMethods.size() > 0) {
//                    Set<Method> methodsOfThisClass = metricValuesForMethods.get(className);
//                    if(methodsOfThisClass != null) {
//                        handleNodeMethods(methodsOfThisClass, node);
//                    }
//                }
            } else {
                logger.info("Cannot find node for class {}", className);
            }
        }
    }

    private void handleNodeMetrics(Map<String, Map<String, Double>> metricValuesNodeLevel,
            Node node,
            Long versionId,
            int[] counter ) {
        logger.info("Handling metrics of class {}", node.getSimpleName());

        for (String metricMnemonic : metricValuesNodeLevel.keySet()) {
            Long metricId = findMetricIdByMnemonic(metricMnemonic);
            Map<String, Double> valuePerClass = metricValuesNodeLevel.get(metricMnemonic);
            Double value = valuePerClass.get(node.getName());
            value = checkValueForInfinity(value);
            createNodeMetric(versionId, node, metricId, value);

            increaseCounterAndFlush(counter);
        }
    }

    private void increaseCounterAndFlush(int[] counter) {
        if ( ++counter[0] % HIBERNATE_BATCH_SIZE == 0 ) { //20, same as the JDBC batch size
            //flush a batch of inserts and release memory:
            logger.debug("Batch size reached ({}). Flushing...", HIBERNATE_BATCH_SIZE);
            em.flush();
            em.clear();
        }
    }

    private Double checkValueForInfinity(Double value) {
        if (value.isInfinite() || value.isNaN()) {
            value = -1.0;
        }
        return value;
    }

    private void createNodeMetric(
            Long versionId,
            Node node,
            Long metricId,
            Double value) {

        MetricValue metricValue = createMetricValue(value, metricId, node.getId(), versionId);

//        Collection<NodeVersionMetric> computedMetrics = node.getComputedMetrics();
//        boolean existedBefore = false;
//
//        for (NodeVersionMetric nvm : computedMetrics) {
//            Metric computedMetric = nvm.getMetricValue().getMetric();
//            //metric value has been persisted before, needs update
//            if (metric.getName().equals(computedMetric.getName()) && nvm.getVersion().equals(versionId)) {
//                MetricValue metricValue1 = nvm.getMetricValue();
//                metricValueRepository.save(metricValue1);
//                existedBefore = true;
//                break;
//            }
//        }
//        if (!existedBefore) {
//            createNodeVersionMetric(version, metricValue, node);
//        }
    }

    protected void handleNodeMethods(Set<Method> methodsOfThisClass, Node node1) {
        logger.info("Methods of class {}",methodsOfThisClass);
        
        if (methodsOfThisClass != null) {
            for (Method method : methodsOfThisClass) {
                node1.addMethod(method);
                Map<String, Number> metricValuesMap = method.getMetricValuesMap();
                logger.info("Metric values of method {} are \n {}", method, metricValuesMap);
                for (String metricMnemonic : metricValuesMap.keySet()) {
                    Long metricId = findMetricIdByMnemonic(metricMnemonic);
                    MetricValue mv = new MetricValue();
                    mv.setMetricId(metricId);
                    Number metricValueFromMap = method.getMetricValueFromMap(metricMnemonic);
                    if (metricValueFromMap == null) {
                        mv.setValue(-1);
                    } else {
                        Double value = metricValueFromMap.doubleValue();
                        value = checkValueForInfinity(value);
                        mv.setValue(value);
                    }
                    method.addMetricValue(mv);
                }
            }
        }
    }

    public void handleVersionLevelMetrics(SoftwareProject softwareProject) {
        logger.info("Handling Project version metrics");
        Map<String, Number> projectLevelMetricValues = softwareProject.getProjectLevelMetricValues();
        Version version = versionService.findVersionByCommitId(softwareProject.getProjectVersion().getCommitHashId());
        for (String metricMnemonic : projectLevelMetricValues.keySet()) {
            createProjectVersionMetric(metricMnemonic, projectLevelMetricValues, version);
        }
    }

    private void createProjectVersionMetric(String metricMnemonic, Map<String, Number> projectLevelMetricValues, Version version) {
        Long metricId = findMetricIdByMnemonic(metricMnemonic);
        double value = projectLevelMetricValues.get(metricMnemonic).doubleValue();
        MetricValue metricValue = createMetricValue(value, metricId, null, version.getId());
        projectVersionMetricRepository.saveProjectVersionMetric(version, metricValue);
    }

    /**
     * Given a number value and a metric, create a metric value.
     * <p>
     * @param value the value of the metric
     * @return the newly created metric value
     */
    protected MetricValue createMetricValue(Number value, Long metricId, Long nodeId, Long versionId) {
        MetricValue metricValue = new MetricValue();
        metricValue.setMetricId(metricId);
        metricValue.setNodeId(nodeId);
        metricValue.setVersionId(versionId);
        if (Double.isNaN(value.doubleValue())) {
            metricValue.setValue(-1);
        } else {
            metricValue.setValue(value.doubleValue());
        }
        return metricValueRepository.save(metricValue);
    }

    public void persistNodeMetrics(Map<Version, Map<AbstractNode, Double>> metricValues,
            String metricMnemonic) {

        logger.info("persisting node level metrics");

        Long metricId = findMetricIdByMnemonic(metricMnemonic);
        for (Version version : metricValues.keySet()) {
            List<Node> persistedGraphNodes = graphService.findNodesByVersion(version.getId());

            Map<String, Node> graphNodesMap = Node.nodeListToTreeMap(persistedGraphNodes);
            Map<AbstractNode, Double> metricValuesForThisVersion = metricValues.get(version);

            for (AbstractNode node : metricValuesForThisVersion.keySet()) {
                Double value = metricValuesForThisVersion.get(node);
                Node dbNode = graphNodesMap.get(node.getName());
                MetricValue metricValue = createMetricValue(value, metricId, dbNode.getId(), version.getId());
            }
        }
    }

    public Long findMetricIdByMnemonic(String metricMnemonic) {
        if(metricMnemonic == null) return null;
        return metricRepository.findMetricIdByMnemonicCached(metricMnemonic);
    }


    public List<ProjectVersionMetric> findProjectVersionMetricsByVersion(Version version) {
        return projectVersionMetricRepository.findByVersion_id(version.getId());
    }

    public List<ProjectMetric> findProjectMetricByProject(Long projectId) {
        return projectMetricRepository.findByProject_id(projectId);
    }

    public List<Metric> findMetricByCategoryName(String categoryName) {
        return metricRepository.findByCategory_categoryName(categoryName);
    }

    public Metric findMetricById(Long metricId) {
        return metricRepository.findMetricById(metricId);
    }

    public List<MetricValue> findMetricValuesByMetricIdAndVersionId(Long metricId, Long versionId){
        return metricValueRepository.findByMetricIdAndVersionId(metricId, versionId);
    }

    public List<MetricValueDTO> findMetricValueDTOsByMetricIdAndVersionId(Long metricId, Long versionId){
        return metricValueRepository.findByMetricValueDTOsByMetricIdAndVersionId(metricId, versionId);
    }

    public List<MetricValueDTO> findMetricValueDTOsByNodeNameAndVersionId(String className, String metricMnemonic, Long versionId) {
        logger.debug("finding {} metric Values for node [{}] version [{}]", metricMnemonic, className, versionId);
        Long metricId = findMetricIdByMnemonic(metricMnemonic);
        NodeDTO nodeDTO = nodeRepository.findNodesByClassNameAndVersion(className, versionId);
        if(nodeDTO == null){
            return new ArrayList<>();
        }
        return metricValueRepository.findByMetricValueDTOsByNodeIdAndVersionId(nodeDTO.getNodeId(), metricId, versionId );
    }
}
