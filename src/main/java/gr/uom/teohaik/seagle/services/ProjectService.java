package gr.uom.teohaik.seagle.services;

import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.teohaik.seagle.util.ArgsCheck;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectTimeline;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectTimelineRepository;
import gr.uom.teohaik.seagle.v3.policy.FilePolicyService;
import gr.uom.teohaik.seagle.v3.project.repository.nameResolvers.GithubNameResolver;
import gr.uom.teohaik.seagle.v3.project.repository.nameResolvers.ProjectNameResolver;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.slf4j.Logger;

/**
 * @author Theodore Chaikalis
 */
@Stateless
public class ProjectService {

    @Inject
    @SeagleLogger
    Logger logger;

    @Inject
    ProjectRepository projectRepository;

    @Inject
    ProjectTimelineRepository projectTimelineRepository;

    @Inject
    FilePolicyService filePolicy;

    @Inject
    RepositoryService repositoryService;

    @Inject
    EntityManager em;

    public Project findProjectById(Long projectId) {
        return projectRepository.findById(projectId);
    }

    public Project findProjectByName(String name) {
        return projectRepository.findByNameExact(name);
    }

    public Project findProjectByRepoUrl(String repoUrl) {
        Optional<Project> optionalProject = projectRepository.findByRemoteRepoPath(repoUrl);
        return optionalProject.orElse(null);
    }

    public ProjectDTO findProjectDTOByRepoUrl(String repoUrl) {
        return projectRepository.findDTOByRemoteRepoPath(repoUrl);
    }

    public ProjectDTO findProjectDTOByName(String name) {
        return projectRepository.findDTOByName(name);
    }

    public boolean isProjectAnalyzed(String pUrl) {
        Optional<Project> optionalProject = projectRepository.findByRemoteRepoPath(pUrl);
        if (optionalProject.isPresent()) {
            Project project = optionalProject.get();
            return project.getDateAnalyzed() != null;
        }
        return false;
    }

    public String resolveProjectName(String projectUrl) {
        if (isProjectAnalyzed(projectUrl)) {
            return projectRepository.getProjectName(projectUrl);
        } else {
            return getProjectNameResolver(projectUrl).resolveName(projectUrl);
        }
    }

    public ProjectNameResolver getProjectNameResolver(String projectUrl) {
        //if(projectUrl.contains("github")) 
        return GithubNameResolver.getInstance();
    }

    public void clone(Project project, String remoteRepoPath) {
        ArgsCheck.notEmpty("remoteRepoPath", remoteRepoPath);

        // Get the location were to store this new repo
        Path localRepoPath = repositoryService.getRepositoryFolder(remoteRepoPath);
        if (isProjectCloned(project.getId())) {
            logger.info("Project Already Cloned. Not cloning again!");
            return;
        }
        if (Files.isDirectory(localRepoPath)) {
            filePolicy.writeLockPath(localRepoPath);

            try (VCSRepository repo = repositoryService.getRepositoryForRemoteUrl(remoteRepoPath)) {
                logger.debug("cloning started for [{}]", remoteRepoPath);

                // Clone repo
                repo.cloneRemote();
                addTimelineEvent(project, ActionType.CLONED);
                logger.info("cloning finished for [{}]", remoteRepoPath);

            } catch (VCSRepositoryException ex) {
                logger.warn("Cannot clone remote repo, network problem or repo already cloned");
            } finally {
                filePolicy.writeUnlockPath(localRepoPath);
            }
        }
    }

    public Project createProject(String creatorEmail, String projectRemoteRepoPath) {
        Project project = new Project();
        project.setCreatedBy(creatorEmail);
        project.setName(resolveProjectName(projectRemoteRepoPath));
        project.setRemoteRepoPath(projectRemoteRepoPath);

        Project projectByRepoUrl = findProjectByRepoUrl(projectRemoteRepoPath);
        if (projectByRepoUrl == null)
            project = projectRepository.save(project);
        else
            project = projectByRepoUrl;
        return project;
    }

    public ProjectDTO convertProjectToDTO(Project project) {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setCreatedBy(project.getCreatedBy());
        projectDTO.setDateCreated(project.getCreationDate().toString());
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setRemoteRepoPath(project.getRemoteRepoPath());
        return projectDTO;
    }


    public Project save(Project project) {
        return projectRepository.save(project);
    }

    public Collection<Version> getProjectVersions(Long projectId) {
        Project project = projectRepository.findById(projectId);
        return project.getVersions();
    }

    public void addTimelineEvent(Project project, ActionType actionType) {
        project = em.find(Project.class, project.getId());
        ProjectTimeline timeline = new ProjectTimeline();
        timeline.setActionType(actionType);
        project.addTimeLine(timeline);
        projectRepository.save(project);
    }

    private boolean isProjectCloned(Long projectId) {
        List<ProjectTimeline> timelines = projectTimelineRepository.findByProjectIdAndType(projectId, ActionType.CLONED);
        return !timelines.isEmpty();
    }


    public List<ProjectDTO> findAllAnalyzed() {
        return projectRepository.findAllByActionTypeStatus(ActionType.ANALYSED);
    }

    public List<ProjectDTO> findAllCloned() {
        return projectRepository.findAllByActionTypeStatus(ActionType.CLONED);
    }

    public List<ProjectDTO> findAllByActionTypeStatus(ActionType actionType) {
        return projectRepository.findAllByActionTypeStatus(actionType);
    }

}
