package gr.uom.teohaik.seagle.services;

import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.se.vcs.jgit.VCSRepositoryImp;
import gr.uom.teohaik.seagle.util.ArgsCheck;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.SeagleConstants;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import gr.uom.teohaik.seagle.v3.policy.FilePolicyService;
import gr.uom.teohaik.seagle.v3.project.repository.nameResolvers.RemoteGitRepositoryInfoResolver;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.apache.deltaspike.core.api.config.ConfigProperty;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class RepositoryService {
    
    @Inject
    @SeagleLogger
    Logger logger;
    
    @Inject
    private FilePolicyService filePolicy;
    
    @ConfigProperty(name="seagleHome")
    private String seagleHome;

    public Path getRepositoryFolder(String remoteUrl) {

        ArgsCheck.notEmpty("repoUrl", remoteUrl);

        //SeaglePathConfig pathConfig = resolvePathConfig();
        String pName = extractProjectName(remoteUrl);
        //Path reposPath = pathConfig.getRepositoriesPath();
        //return reposPath.resolve(pName);
        String userHome = getSeagleHomeFolder();
        Path path = Paths.get(userHome, SeagleConstants.SEAGLE_HOME, SeagleConstants.REPOS_HOME, pName);
        try {
            Files.createDirectories(path);
        } catch (IOException ex) {
            logger.error("IO Exception",ex);
        }
        logger.debug("Repository Folder path = [{}]",path);
        return path;
    }

    /**
     * Get the location where checkouts should be stored.
     */
    private Path resolveProjectSourceCodePath(String repoUrl) {
        ArgsCheck.notEmpty("repoUrl", repoUrl);

        String pName = extractProjectName(repoUrl);

        String userHome = getSeagleHomeFolder();
        Path path = Paths.get(userHome, SeagleConstants.SEAGLE_HOME, SeagleConstants.PROJECTS_SOURCE_CODE_HOME, pName);
        try {
            Files.createDirectories(path);
        } catch (IOException ex) {
            logger.error("IO Exception",ex);
        }
        return path;
    }

    private String getSeagleHomeFolder() {
        String userHome = (seagleHome != null) ? seagleHome : System.getProperty("user.home");
        return userHome;
    }

    private String extractProjectName(String repoUrl) {
        return RemoteGitRepositoryInfoResolver.getInstance().tryResolveName(repoUrl);
    }

    public VCSRepository getRepositoryForRemoteUrl(String remoteUrl) {
        gr.uom.se.util.validation.ArgsCheck.notNull("remoteUrl", remoteUrl);

        String localFolder = getRepositoryFolder(remoteUrl).toString();
        try {
            logger.info("Opening repo for {}",remoteUrl);
            return new VCSRepositoryImp(localFolder, remoteUrl);
        } catch (VCSRepositoryException e) {
            throw new IllegalStateException(e);
        }
    }

    public List<JavaProject> createJavaProjectForEachVersion(ProjectDTO project) {
        String repoUrl = project.getRemoteRepoPath();
        List<JavaProject> javaProjects = new ArrayList<>();
        try (VCSRepository repo = getRepositoryForRemoteUrl(repoUrl)) {
            for (VersionDTO version : project.getVersions()) {
                logger.info("Creating Java Project for version {}", version);
                Path checkoutFolder = getSourceCodePath(repoUrl, version);
                VCSCommit vcsCommit = repo.resolveCommit(version.getCommitHashId());
                JavaProject javaProject = new JavaProject(version, project.getName(), checkoutFolder.toString(), vcsCommit);
                javaProjects.add(javaProject);
            }
        } catch (Exception ex) {
            logger.error("Exception",ex);
        }
        return javaProjects;
    }

    private Path getSourceCodePath(String repoUrl, VersionDTO version) {
        Path sourceCodeParentFolder = resolveProjectSourceCodePath(repoUrl);
        Path checkoutFolder = resolveCheckoutFolder(sourceCodeParentFolder, version.getCommitHashId());
        return checkoutFolder;
    }
    
    /**
    * Get the checkout out folder.
    */
   private Path resolveCheckoutFolder(Path sourceFolder, String cid) {
      return sourceFolder.resolve(cid);
   }
   
    /**
    * {@inheritDoc}
    * <p>
    * A repository object will be loaded and it will try to resolve the commit
    * with the given ID, if it can not be resolved it will throw an exception.
    * The repository local path and source path will be locked while this method
    * is executing.
    */
    public void checkout(String repoUrl, String commitID) {

        // Get the folder of source codes
        Path sourcePath = resolveProjectSourceCodePath(repoUrl);
        
        // Get the folder of repository for the given url
        Path repoPath = getRepositoryFolder(repoUrl);

//        // We should lock now with write lock the source path
//        // with write lock the checkout path
//        // and with read lock the repository path (we will be only reading from)
//        // First of is to lock the repo with reading so we are sure
//        // no one can update/delete the repo
//        filePolicy.readLockPath(repoPath);
//        // The whole source path will be locked with write
//        // so no other can write or read until the checkout is
//        // performed (no need to lock the checkout path)
//        filePolicy.writeLockPath(sourcePath);
        try {
            // If the repo doesn't exist we should throw an exception
            // because we can not create the repo object
            if (!Files.isDirectory(repoPath)) {
                throw new RuntimeException("repo doesn't exist for " + repoUrl);
            }
            // Get the folder where to checkout out the given commit
            // for the given repository
            Path checkoutPath = resolveCheckoutFolder(sourcePath, commitID);

            // We should check now if the folder of source codes exist
            // if not we should create it for the first time
            // This will ensure that the folder of source code
            // will be present
            Files.createDirectories(sourcePath);

            // Now we should check if the checkout folder
            // is not present. If its not present we should
            // make the checkout.
            if (!Files.isDirectory(checkoutPath)) {
                try (VCSRepository repo = getRepositoryForRemoteUrl(repoUrl)) {
                    // Resolve the commit
                    VCSCommit commit = repo.resolveCommit(commitID);

                    // Call the checkout method
                    checkout(commit, checkoutPath);
                }
            }


            logger.info("Checkout completed for {} - commit: [{}]", repoUrl, commitID);

        } catch (VCSRepositoryException | IOException ex) {
            throw new RuntimeException(ex);
        } finally {

//            filePolicy.writeUnlockPath(sourcePath);
//            filePolicy.readUnlockPath(repoPath);
        }
    }
    
        /**
    * {@inheritDoc}
    * <p>
    * A repository object will be loaded and it will try to resolve the commit
    * with the given ID, if it can not be resolved it will throw an exception.
    * The repository local path and source path will be locked while this method
    * is executing. This is the same as calling
    * {@link #checkout(VCSRepository, VCSCommit)} after the resolving of commit
    * object.
    */
    private void checkout(VCSRepository repo, String cid) {
      ArgsCheck.notNull("repo", repo);
      ArgsCheck.notEmpty("cid", cid);
      try {
         VCSCommit commit = repo.resolveCommit(cid);
         checkout(repo, commit);
      } catch (VCSRepositoryException ex) {
         throw new RuntimeException(ex);
      }
   }
    
    /**
    * {@inheritDoc}
    * <p>
    * The repository local path and source path will be locked while this method
    * is executing.
    */
   private void checkout(VCSRepository repo, VCSCommit commit) {

      // Get the folder of source codes
      Path sourcePath = resolveProjectSourceCodePath(repo.getRemotePath());
      // Get the folder of repository for the given url
      Path repoPath = Paths.get(repo.getLocalPath());

      // We should lock now with write lock the source path
      // with write lock the checkout path
      // and with read lock the repository path (we will be only reading from)
      // First of is to lock the repo with reading so we are sure
      // no one can update/delete the repo
      filePolicy.readLockPath(repoPath);
      // The whole source path will be locked with write
      // so no other can write or read until the checkout is
      // performed (no need to lock the checkout path)
      filePolicy.writeLockPath(sourcePath);
      try {

         // Get the folder where to checkout out the given commit
         // for the given repository
         Path checkoutPath = resolveCheckoutFolder(sourcePath, commit.getID());

         // We should check now if the folder of source codes exist
         // if not we should create it for the first time
         // This will ensure that the folder of source code
         // will be present
         Files.createDirectories(sourcePath);

         // Call the checkout method
         checkout(commit, checkoutPath);

      } catch (IOException ex) {
         throw new RuntimeException(ex);
      } finally {
         filePolicy.writeUnlockPath(sourcePath);
         filePolicy.readUnlockPath(repoPath);
      }
   }

   /**
    * Checkout with rolling back if any exception occurs.
    * <p>
    *
    * @param commit
    * @param checkoutPath
    */
   private void checkout(VCSCommit commit, Path checkoutPath) {
      // Now we should check if the checkout folder
      // is not present. If its not present we should
      // make the checkout.
      if (!Files.isDirectory(checkoutPath)) {
         try {
            // The commit is resolved so we should create the checkout folder
            Files.createDirectories(checkoutPath);
            commit.checkout(checkoutPath.toAbsolutePath().toString());
         } catch (Exception ex) {
            // Rollback if any problem occurs
            FileUtils.deleteQuietly(checkoutPath.toFile());
            throw new RuntimeException(ex);
         }
      }
   }
   
    public void delete(String remoteRepoPath) {
        // Get the file policy to lock down folders
        //  FilePolicy filePolicy = resolveFilePolicy();
        // Get the path of the repo
        //   Path repoPath = resolveRepoPath(remoteRepoPath);
        //   Path sourcePath = resolveProjectSourceCodePath(remoteRepoPath);
//
//      filePolicy.writeLockPath(repoPath);
//      filePolicy.writeLockPath(sourcePath);
//      try {
//         // If the path exists and is a directory
//         // delete it
//         if (Files.isDirectory(repoPath)) {
//            try {
//               FileUtils.deleteDirectory(repoPath.toFile());
//            } finally {
//               // Delete project source codes to
//               if (Files.isDirectory(sourcePath)) {
//                  FileUtils.deleteDirectory(sourcePath.toFile());
//               }
//            }
//
//            // Trigger a DELETE event
//            eventManager.trigger(createEvent(ProjectEventType.REMOVE,
//                  remoteRepoPath, repoPath.toString()));
//         }
//
//      } catch (IOException ex) {
//         throw new RuntimeException(ex);
//      } finally {
//         filePolicy.writeUnlockPath(sourcePath);
//         filePolicy.writeUnlockPath(repoPath);
//      }
    }

}
