package gr.uom.teohaik.seagle.services;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author Theodore Chaikalis
 */
@Singleton
public class SeagleManager {
    
    
    @PostConstruct
    public void init(){
        System.out.println("seagle manager started");
    }

}
