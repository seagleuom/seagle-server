package gr.uom.teohaik.seagle.services;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Singleton
@Startup
public class VersionProvider {

    @Inject
    EntityManager em;
   
    @Inject
    @SeagleLogger
    Logger logger;
    
    @PostConstruct
    public void init(){
        logger.info("Startup EJB. Executing demo query....");
        List resultList = em.createQuery("select p from Project p where p.name = ?1")
                .setParameter(1, "test")
                .getResultList();
        int size = resultList.size();
        logger.info("Result list size = {}",size);
    }
}
