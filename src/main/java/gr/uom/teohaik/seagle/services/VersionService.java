package gr.uom.teohaik.seagle.services;

import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.BranchTagProvider;
import gr.uom.se.vcs.analysis.version.provider.ConnectedTagVersionNameProvider;
import gr.uom.se.vcs.analysis.version.provider.DefaultVersionNameResolver;
import gr.uom.se.vcs.analysis.version.provider.VersionNameResolver;
import gr.uom.se.vcs.analysis.version.provider.VersionString;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.teohaik.seagle.util.ArgsCheck;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.model.repositories.VersionRepository;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author Theodore Chaikalis
 */
@Stateless
public class VersionService {

    @Inject
    @SeagleLogger
    private org.slf4j.Logger logger;
    
    @Inject
    private VersionRepository versionRepository;

    @Inject
    ProjectService projectService;

    public List<Version> findAllByProject(String projectUrl){
        Project project = projectService.findProjectByRepoUrl(projectUrl);
        return versionRepository.findAll(project.getId());
    }

    public List<Version> findAnalyzedByProject(String projectUrl){
        Project project = projectService.findProjectByRepoUrl(projectUrl);
        return versionRepository.findAllAnalyzed(project.getId());
    }

    public Optional<Version> findOptionalVersionByCommitId(String commitId){
        return versionRepository.findOptionalByCommitID(commitId);
    }
    
    public Version findVersionByCommitId(String commitId){
        Optional<Version> optionalVersion = versionRepository.findOptionalByCommitID(commitId);
        if(optionalVersion.isPresent()){
            return optionalVersion.get();
        }
        else{
            logger.warn("Version with commitId "+commitId+" not found");
            return null;
        }
    }

    public Version findVersionById(Long id){
        Optional<Version> optionalVersion = versionRepository.findOptionalById(id);
        if(optionalVersion.isPresent()){
            return optionalVersion.get();
        }
        else{
            logger.warn("Version with Id "+id+" not found");
            return null;
        }
    }

    public Project addVersionsToProject(Project project, VCSRepository repo) {
        logger.info("Determining versions of project {}", project.getName());
        try {
            ArgsCheck.notEmpty("tags collection", repo.getTags());
            ConnectedTagVersionNameProvider versionProvider = getVersionProvider(repo);
            Iterator<VCSCommit> versionIterator = versionProvider.iterator();

            if(versionProvider.getVersions().size() == 0) {
                String commitId = repo.getHead().getID();
                Version v = new Version();
                v.setCommitID(commitId);
                v.setDate(repo.getHead().getCommitDate());
                v.setName("Head");
                project.addVersion(v);
                logger.info("Project does not contain version info. Created a single version from head commit: "
                        + "{}", v.getCommitID());
            }
            else {
                while (versionIterator.hasNext()) {
                    VCSCommit versionCommit = versionIterator.next();
                    Version v = new Version();
                    v.setCommitID(versionCommit.getID());
                    v.setDate(versionCommit.getCommitDate());
                    v.setName(versionProvider.getName(versionCommit));
                    project.addVersion(v);
                    logger.info("Created version {} - Name: {}", v.getCommitID(), v.getName());
                }
            }
            project = projectService.save(project);

        } catch (VCSRepositoryException ex) {
            Logger.getLogger(VersionService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return project;
    }
    
    private ConnectedTagVersionNameProvider getVersionProvider(VCSRepository repo) throws VCSRepositoryException{
         BranchTagProvider branchTagProvider = new BranchTagProvider(repo);
            VersionNameResolver versionNameResolver = new DefaultVersionNameResolver();
            Comparator<VersionString> defaultComparator = new DefaultVersionComparator();
            return new ConnectedTagVersionNameProvider(branchTagProvider, 
                            versionNameResolver, 
                            defaultComparator);
    }

    public void setAnalyzedVersions(Collection<VersionDTO> versions) {
        for(VersionDTO versionDTO : versions){
            Optional<Version> optionalById = versionRepository.findOptionalById(versionDTO.getVersionDbId());
            if(optionalById.isPresent()){
                Version version = optionalById.get();
                version.setAnalyzed(true);
                versionRepository.save(version);
                logger.info("Marking the following version as analyzed: {} ", version.getId());
            }
        }
    }

    public class DefaultVersionComparator implements Comparator<VersionString>{

        @Override
        public int compare(VersionString o1, VersionString o2) {
            return o1.compareTo(o2);
        }
        
    }

}
