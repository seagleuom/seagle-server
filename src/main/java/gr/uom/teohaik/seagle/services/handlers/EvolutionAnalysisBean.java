package gr.uom.teohaik.seagle.services.handlers;

import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.services.ExecutableMetricService;
import gr.uom.teohaik.seagle.services.GraphService;
import gr.uom.teohaik.seagle.services.MetricService;
import gr.uom.teohaik.seagle.services.RepositoryService;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph.ASTReader;
import gr.uom.teohaik.seagle.v3.analysis.metrics.graph.AbstractGraphExecutableMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.AbstractJavaExecutableMetric;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import gr.uom.teohaik.seagle.v3.model.entities.Metric;
import gr.uom.teohaik.seagle.v3.model.entities.MetricCategory;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Dependent
public class EvolutionAnalysisBean {

    public static final String mnemonic = "SourceCodeEvolutionAnalysis";

    @EJB
    private GraphService graphService;

    @EJB
    private MetricService metricService;

    @EJB
    private ExecutableMetricService executableMetricService;

    @EJB
    private RepositoryService repositoryService;

    @Inject
    @SeagleLogger
    private Logger logger;

    public void runEvolutionAnalysis(ProjectDTO project) {
        //2. javaAnalysis
        List<JavaProject> javaProjects = repositoryService.createJavaProjectForEachVersion(project);
        project.setSoftwareProjects(javaProjects);
        createGraphs(javaProjects);
    }

    private List<SoftwareGraph> createGraphs(List<JavaProject> javaProjects) {
        List<SoftwareGraph> createdGraphs = new ArrayList<>();
        for (JavaProject softwareProject : javaProjects) {
            logger.info("Creating graph for project {} version {}",softwareProject.getProjectName(), softwareProject.getProjectVersion());
            try {
                logger.info("Parsing AST...");
                SystemObject systemObject = ASTReader.parseASTAndCreateSystemObject(softwareProject);

                logger.info("Creating System Graph...");
                SoftwareGraph<AbstractNode, AbstractEdge> graph = graphService.createJavaGraph(systemObject, softwareProject);
                softwareProject.setGraph(graph);

                logger.info("Calculating Source Code metrics");
                calculateSourceCodeMetrics(systemObject, softwareProject);

                logger.info("Calculating Graph metrics");
                calculateGraphMetrics(softwareProject);
                createdGraphs.add(graph);
                //  javaProject.persist();
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
        return createdGraphs;
    }

    private void calculateSourceCodeMetrics(final SystemObject systemObject, final JavaProject javaProject) throws InterruptedException, ExecutionException {
        List<Metric> sourceCodeMetrics = metricService.findMetricByCategoryName(MetricCategory.CATEGORY_NAMES.SourceCodeMetrics.name());
        sourceCodeMetrics.forEach(metric->{
            AbstractJavaExecutableMetric javaMetric = executableMetricService.getSourceCodeMetric(metric.getImplementationClass());
                logger.info(" | calculating Source Code Metric: {} | Version : {}", new Object[]{javaMetric.getMnemonic(), javaProject.getProjectVersion().getName()});
                javaMetric.calculate(systemObject, javaProject);
        });
    }

    private void calculateGraphMetrics(SoftwareProject softwareProject) {
        logger.info("Graph metrics calculation started");
        List<Metric> graphMetrics = metricService.findMetricByCategoryName(MetricCategory.CATEGORY_NAMES.GraphMetrics.name());
        graphMetrics.forEach(metric->{
            AbstractGraphExecutableMetric graphMetric = executableMetricService.getGraphMetric(metric.getImplementationClass());
                logger.info("-------calculating metric: {}", graphMetric.getMnemonic());
                graphMetric.calculate(softwareProject);
        });
        logger.info("Graph metrics calculation ENDED. ");
    }


}
