package gr.uom.teohaik.seagle.services.handlers;

import gr.uom.teohaik.seagle.services.MetricService;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import gr.uom.teohaik.seagle.v3.events.MetricSavingFinishedEvent;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import org.slf4j.Logger;

@Dependent
public class MetricPersistingHandlerBean  {

    @Inject
    @SeagleLogger
    private Logger logger;

    @EJB
    private MetricService metricService;

    @Inject
    private Event<MetricSavingFinishedEvent> metricSavingFinishedEvent;


    public void persistMetrics(ProjectDTO project) {

        List<? extends SoftwareProject> softwareProjects = project.getSoftwareProjects();

        for (SoftwareProject softwareProject : softwareProjects) {
            logger.info("persisting node level metrics for project {} version {}",softwareProject.getProjectName(), softwareProject.getProjectVersion().getName());
            metricService.handleNodeLevelMetrics(softwareProject);

            logger.info("persisting version level metrics for project {} version {}",softwareProject.getProjectName(), softwareProject.getProjectVersion().getName());
            metricService.handleVersionLevelMetrics(softwareProject);
        }

        metricSavingFinishedEvent.fire(new MetricSavingFinishedEvent(project));
    }

}
