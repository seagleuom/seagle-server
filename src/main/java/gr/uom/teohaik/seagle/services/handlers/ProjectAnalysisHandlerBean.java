package gr.uom.teohaik.seagle.services.handlers;

import gr.uom.se.vcs.VCSRepository;
import gr.uom.teohaik.seagle.services.ProjectService;
import gr.uom.teohaik.seagle.services.RepositoryService;
import gr.uom.teohaik.seagle.services.VersionService;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.util.exception.SeagleException;
import gr.uom.teohaik.seagle.v3.events.ProjectAnalysisRequest;
import gr.uom.teohaik.seagle.v3.events.VersionDeterminationRequest;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectRepository;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import javax.enterprise.event.ObservesAsync;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;

/**
 * @author Theodore Chaikalis
 */
@Named
public class ProjectAnalysisHandlerBean {

    @Inject
    @SeagleLogger
    Logger logger;

    @Inject
    ProjectRepository projectRepository;

    @Inject
    RepositoryService repositoryService;

    @Inject
    VersionService versionService;

    @Inject
    ProjectService projectService;

    @Inject
    RemoteRepoHandlerBean remoteRepoHandlerBean;

    @Inject
    EvolutionAnalysisBean evolutionAnalysisBean;

    @Inject
    MetricPersistingHandlerBean metricPersistingHandlerBean;



    /**
     * *********************************
     * Event Handling
     * *********************************
     */
    /**
     * Handles the event that signals the need for project cloning
     *
     * @param projectCloneRequest contains info about the project that should be cloned.
     */
    public void handleProject(@ObservesAsync ProjectAnalysisRequest projectCloneRequest) {
        logger.info("Analysis Async request caught! Request = {}", projectCloneRequest);
        try {
            Project project = projectService.findProjectByRepoUrl(projectCloneRequest.getRequestUrl());
            logger.info("Project fetched = {}", project);
            if (project != null && project.getVersions().isEmpty()) {
                String remoteRepoPath = projectCloneRequest.getRequestUrl();

                startAnalysis(projectCloneRequest.getProject());
            } else if (project != null) {
                startAnalysis(projectCloneRequest.getProject());
            } else {
                logger.warn("Nothing to do! Please try again!");
            }
        } catch (Exception ex) {
            logger.error("Exception during handling request {}", projectCloneRequest, ex);
            throw new SeagleException("Exception during handling project", ex);
        }
    }

    public void handleVersionAnalysisRequest(@ObservesAsync VersionDeterminationRequest versionDeterminationRequest) {

        logger.info("Version Determination Async request caught! Request = {}", versionDeterminationRequest);
        try {
            String repoUrl = versionDeterminationRequest.getRequestUrl();
            Project project = versionDeterminationRequest.getProject();
            logger.info("Project fetched = {}", project);

            projectService.clone(project, repoUrl);

            VCSRepository repo = repositoryService.getRepositoryForRemoteUrl(repoUrl);
            project = versionService.addVersionsToProject(project, repo);

            if(versionDeterminationRequest.shouldTriggerAnalysisAfterDeterminingVersions()){
                logger.info("client also wants evolution analysis for [{}], triggering now...", repoUrl);
                //startAnalysis(project);
            }
        } catch (Exception ex) {
            logger.error("Exception during handling request {}", versionDeterminationRequest, ex);
            throw new SeagleException("Exception during handling project", ex);
        }
    }

    private void startAnalysis(ProjectDTO project) {
        remoteRepoHandlerBean.checkoutRepositoriesForVersions(project);
        evolutionAnalysisBean.runEvolutionAnalysis(project);
        metricPersistingHandlerBean.persistMetrics(project);
    }

}
