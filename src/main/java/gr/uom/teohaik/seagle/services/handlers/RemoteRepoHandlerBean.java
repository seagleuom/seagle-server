package gr.uom.teohaik.seagle.services.handlers;

import gr.uom.teohaik.seagle.services.ProjectService;
import gr.uom.teohaik.seagle.services.RepositoryService;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

@Dependent
public class RemoteRepoHandlerBean {

    @Inject
    RepositoryService repositoryService;

    @Inject
    ProjectService projectService;

    public void checkoutRepositoriesForVersions(ProjectDTO projectDTO){

        projectDTO.getVersions().forEach(version ->{
            repositoryService.checkout(projectDTO.getRemoteRepoPath(), version.getCommitHashId());
        });
    }

}

