package gr.uom.teohaik.seagle.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Theodore Chaikalis
 */
@ApplicationScoped
public class EmProducer {

   @Produces
    @PersistenceContext(unitName="seaglePU")
    private EntityManager em;

    public void close(@Disposes EntityManager em)
    {
        if (em.isOpen())
        {
            em.close();
        }
    }
}
