package gr.uom.teohaik.seagle.util;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Theodore Chaikalis
 */
@ApplicationScoped
public class LoggerProducer {
    

    @Produces
    @SeagleLogger
    public Logger produceLogger(final InjectionPoint ip) {
        return LoggerFactory.getLogger(ip.getMember().getDeclaringClass().getName());
    }
}
