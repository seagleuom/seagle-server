
package gr.uom.teohaik.seagle.util.exception;

/**
 *
 * @author Theodore Chaikalis
 */
public class SeagleException extends RuntimeException {

    public SeagleException(String message){
        super(message);
    }
    
    public SeagleException(String message, Throwable t){
        super(message, t);
    }

    public SeagleException(String cound_not_found_implementation_for_metric, String metricImplementationClassName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
