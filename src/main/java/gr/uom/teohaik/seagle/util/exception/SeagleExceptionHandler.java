
package gr.uom.teohaik.seagle.util.exception;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import javax.inject.Inject;
import org.apache.deltaspike.core.api.exception.control.Handles;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
public class SeagleExceptionHandler {
    
        
    @Inject
    @SeagleLogger
    private Logger logger;
    
    
    public void handleSeagleException(@Handles SeagleException ex){
        logger.error(ex.getMessage());
    }

}
