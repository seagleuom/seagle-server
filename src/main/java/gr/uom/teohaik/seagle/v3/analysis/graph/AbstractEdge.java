
package gr.uom.teohaik.seagle.v3.analysis.graph;

import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 * 
 */
public abstract class AbstractEdge implements Serializable{
    
    private final AbstractNode sourceNode;
    private final AbstractNode targetNode;
    private final VersionDTO versionCreated;
    private String friendshipDescription;

    public AbstractEdge(AbstractNode sourceNode, AbstractNode targetNode, VersionDTO versionCreated) {
        this.sourceNode = sourceNode;
        this.targetNode = targetNode;
        this.versionCreated = versionCreated;
        friendshipDescription = "";
    }

    public AbstractNode getSourceNode() {
        return sourceNode;
    }

    public AbstractNode getTargetNode() {
        return targetNode;
    }

    public VersionDTO getVersionCreated() {
        return versionCreated;
    }

    public void setFriendshipDescription(String friendshipDescription) {
        this.friendshipDescription = friendshipDescription;
    }

    public String getFriendshipDescription() {
        return friendshipDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof AbstractEdge) {
            AbstractEdge edge = (AbstractEdge) o;
            return ((this.sourceNode.equals(edge.sourceNode))
                    && (this.targetNode.equals(edge.targetNode)));
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (friendshipDescription != null) {
            sb.append(sourceNode).append(" > ").append(this.friendshipDescription).append(" > ").append(targetNode).append("  ");
        } else {
            sb.append(sourceNode).append(" -> ").append(targetNode).append("  ");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int h1 = sourceNode.hashCode();
        int h2 = targetNode.hashCode();
        int hash = (h1 >>> 20) ^ (h1 >>> 12) ^ (h2 >>> 20) ^ (h2 >>> 12);
        return hash ^ (hash >>> 7) ^ (hash >>> 4);
    }
    
    
}
