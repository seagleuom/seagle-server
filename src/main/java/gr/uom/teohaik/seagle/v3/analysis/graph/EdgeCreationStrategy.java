
package gr.uom.teohaik.seagle.v3.analysis.graph;

import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;

/**
 *
 * @author Theodore Chaikalis
 */
public interface EdgeCreationStrategy {

    public abstract void createEdges(SoftwareGraph softwareGraph, SystemObject system);
    
}
