package gr.uom.teohaik.seagle.v3.analysis.graph;

/**
 *
 * @author Theodore Chaikalis
 */
public interface GraphFactory {

    /*
     * Returns the classes in ClassObject List as Graph vertices with NO edges between them.
     */
    public abstract SoftwareGraph getSystemGraph(EdgeCreationStrategy edgeCreationStrategy);
   
    
}
