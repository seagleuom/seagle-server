
package gr.uom.teohaik.seagle.v3.analysis.graph;


/**
 *
 * @author Theodore Chaikalis
 */
public interface GraphVisitor {
    
    public abstract void visit(SoftwareGraph<? extends AbstractNode, ? extends AbstractEdge> softwareGraph);

}
