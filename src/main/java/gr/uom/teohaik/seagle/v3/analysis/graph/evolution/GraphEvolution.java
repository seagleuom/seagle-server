
package gr.uom.teohaik.seagle.v3.analysis.graph.evolution;

import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class GraphEvolution {
    
    private final Collection<VersionDTO> projectEvolutionVersions;
    private final Map<VersionDTO, SoftwareGraph> softwareGraphEvolution;
    
    Map<VersionDTO, Set<AbstractEdge>> edgesBtwnExistingMap = new LinkedHashMap<>();
    Map<VersionDTO, Set<AbstractEdge>> edgesBtwnNewMap = new LinkedHashMap<>();
    Map<VersionDTO, Set<AbstractEdge>> edgesToExistingMap = new LinkedHashMap<>();
    Map<VersionDTO, Set<AbstractEdge>> edgesToNewMap = new LinkedHashMap<>();
    Map<VersionDTO, Set<AbstractNode>> existingNodesMap = new LinkedHashMap<>();
    Map<VersionDTO, Set<AbstractNode>> newNodesMap = new LinkedHashMap<>();

    public GraphEvolution(List<SoftwareGraph> graphs) {
        this.projectEvolutionVersions = new ArrayList<>();
        this.softwareGraphEvolution = new TreeMap<>();
        
        for(SoftwareGraph graph : graphs) {
            projectEvolutionVersions.add(graph.getVersion());
            softwareGraphEvolution.put(graph.getVersion(), graph);
        }
    }

    public void setNewNodesMap(Map<VersionDTO, Set<AbstractNode>> newNodesMap) {
        this.newNodesMap = newNodesMap;
    }

    public Map<VersionDTO, Set<AbstractNode>> getNewNodesMap() {
        return newNodesMap;
    }

    public void setExistingNodesMap(Map<VersionDTO, Set<AbstractNode>> existingNodesMap) {
        this.existingNodesMap = existingNodesMap;
    }

    public Map<VersionDTO, Set<AbstractNode>> getExistingNodesMap() {
        return existingNodesMap;
    }

    public void setEdgesToNewMap(Map<VersionDTO, Set<AbstractEdge>> edgesToNewMap) {
        this.edgesToNewMap = edgesToNewMap;
    }

    public Map<VersionDTO, Set<AbstractEdge>> getEdgesToNewMap() {
        return edgesToNewMap;
    }
    
    public void setEdgesToExistingMap(Map<VersionDTO, Set<AbstractEdge>> edgesToExistingMap) {
        this.edgesToExistingMap = edgesToExistingMap;
    }

    public Map<VersionDTO, Set<AbstractEdge>> getEdgesToExistingMap() {
        return edgesToExistingMap;
    }
    
    public Map<VersionDTO, Set<AbstractEdge>> getEdgesBtwnNewMap() {
        return edgesBtwnNewMap;
    }

    public void setEdgesBtwnNewMap(Map<VersionDTO, Set<AbstractEdge>> edgesBtwnNewMap) {
        this.edgesBtwnNewMap = edgesBtwnNewMap;
    }

    public void setEdgesBtwnExistingMap(Map<VersionDTO, Set<AbstractEdge>> edgesBtwnExistingMap) {
        this.edgesBtwnExistingMap = edgesBtwnExistingMap;
    }

    public Map<VersionDTO, Set<AbstractEdge>> getEdgesBtwnExistingMap() {
        return edgesBtwnExistingMap;
    }
    
    public Collection<VersionDTO> getProjectEvolutionVersions() {
        return projectEvolutionVersions;
    }
    
    public Iterator<VersionDTO> getVersionIterator(){
        return projectEvolutionVersions.iterator();
    }
    
    public Iterator<SoftwareGraph> getGraphIterator(){
        return softwareGraphEvolution.values().iterator();
    }
    
    public VersionDTO[] getVersionArray(){
        VersionDTO[] v = new VersionDTO[1];
        return projectEvolutionVersions.toArray(v);
    }
    
    public int getVersionsCount(){
        return projectEvolutionVersions.size();
    }

    public SoftwareGraph getGraphForAVersion(VersionDTO VersionDTO) {
        if(VersionDTO != null)
            return softwareGraphEvolution.get(VersionDTO);
        else
            return null;
    }
}