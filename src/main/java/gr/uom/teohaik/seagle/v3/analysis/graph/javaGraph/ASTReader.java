package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;

public class ASTReader {
    
    private final static Logger logger = Logger.getLogger(ASTReader.class.getName());

    public static SystemObject parseASTAndCreateSystemObject(JavaProject javaProject) throws Exception {
        ASTParser parser = ASTParser.newParser(AST.JLS8);
        parser.setKind(ASTParser.K_COMPILATION_UNIT);
        Map options = JavaCore.getOptions();
        JavaCore.setComplianceOptions(JavaCore.VERSION_1_8, options);
        parser.setCompilerOptions(options);

        String[] stringDemo = new String[1];

        String[] bindingKeys = new String[javaProject.getJavaFilePaths().size()];

        parser.setEnvironment(null, new String[]{javaProject.getSourceFolder()}, null, true);
        parser.setResolveBindings(true); // we need bindings later on
        parser.setStatementsRecovery(true);
        parser.setBindingsRecovery(true);

        SystemObject systemObject = new SystemObject();
                
        int i = 0;
        for (String javaFilePath : javaProject.getJavaFilePaths()) {
            try {
                bindingKeys[i] = createBindingKeyFromClassFile(javaFilePath, systemObject);
            } catch (IOException e) {
                logger.log(Level.SEVERE,null,e);
            }
            i++;
        }
        String[] canonicalPaths = javaProject.getJavaFilePaths().toArray(stringDemo);
        JavaASTFileReader myASTFileReader = new JavaASTFileReader(systemObject, javaProject);
        try {
            parser.createASTs(canonicalPaths, null, bindingKeys, myASTFileReader, null);
            return systemObject;
        } catch (Exception e) {
            logger.log(Level.SEVERE,null,e);
            return null;
        }
    }

    public static int sourceCodeLineCounter(String classCode) {
        String code = new Scanner(classCode).useDelimiter("\\A").next();
        // strip comments
        code = Pattern.compile("/\\*.*?\\*/|//.*?$", Pattern.MULTILINE | Pattern.DOTALL).matcher(code).replaceAll("");
        // split into array using non empty lines as delimiters
        String[] s = Pattern.compile("\\S.*?$", Pattern.MULTILINE).split(code.trim());
        // count
        return s.length;
    }

    private static String createBindingKeyFromClassFile(String filePath, SystemObject systemObject) throws IOException {
        String classString = new String(Files.readAllBytes(Paths.get(filePath)));
        String fullyQualifiedClassName = "";
        try {
            String packageName = "";
            int packageDeclarationStart = classString.indexOf("package");
            if (packageDeclarationStart >= 0) {
                int packageDeclarationEnd = classString.indexOf(";", packageDeclarationStart);
                String packageDeclarationLine = classString.substring(packageDeclarationStart, packageDeclarationEnd);
                packageName = packageDeclarationLine.substring(packageDeclarationLine.lastIndexOf("package") + 7, packageDeclarationLine.length());
            } else {
                packageName = "";
            }
            String className = filePath.substring(filePath.lastIndexOf(File.separator), filePath.indexOf(".java"));
            fullyQualifiedClassName = packageName + "." + className + ";";
            fullyQualifiedClassName = fullyQualifiedClassName.replace(".", File.separator);
            systemObject.addLinesOfCodeForAFile(filePath, sourceCodeLineCounter(classString));
        } catch (Exception e) {
            logger.log(Level.SEVERE,"error with class {0}",classString);
            logger.log(Level.SEVERE, e.getMessage(), e);
        }
        return fullyQualifiedClassName;
    }

}
