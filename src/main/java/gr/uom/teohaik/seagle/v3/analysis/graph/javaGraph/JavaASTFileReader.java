package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import gr.uom.java.ast.Access;
import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.ConstructorObject;
import gr.uom.java.ast.FieldObject;
import gr.uom.java.ast.MethodBodyObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.ParameterObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.ast.TypeObject;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.FileASTRequestor;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.slf4j.LoggerFactory;

public class JavaASTFileReader extends FileASTRequestor {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JavaASTFileReader.class.getName());

    private final IBinding[] bindings = new IBinding[1];
    private final CompilationUnit[] units = new CompilationUnit[1];
    private SystemObject systemObject;
    private JavaProject javaProject;

    public JavaASTFileReader(SystemObject systemObject, JavaProject javaProject) {
        this.systemObject = systemObject;
        this.javaProject = javaProject;
    }

    @Override
    public void acceptBinding(String bindingKey, IBinding binding) {
        bindings[0] = binding;
    }

    @Override
    public void acceptAST(String sourceFilePath, CompilationUnit ast) {
        try {
            logger.debug("Accepting AST Visitor....");
            CompilationUnit compilationUnit = ast;
            List<ClassObject> classObjects = new ArrayList<>();
            List<AbstractTypeDeclaration> topLevelTypeDeclarations = compilationUnit.types();

            for (AbstractTypeDeclaration abstractTypeDeclaration : topLevelTypeDeclarations) {

                if (abstractTypeDeclaration instanceof TypeDeclaration) {
                    TypeDeclaration topLevelTypeDeclaration = (TypeDeclaration) abstractTypeDeclaration;
                    List<TypeDeclaration> typeDeclarations = new ArrayList<>();
                    typeDeclarations.add(topLevelTypeDeclaration);
                    TypeDeclaration[] types = topLevelTypeDeclaration.getTypes();
                    for (TypeDeclaration type : types) {
                        typeDeclarations.add(type);
                    }
                    for (TypeDeclaration typeDeclaration : typeDeclarations) {
                        final ClassObject classObject = new ClassObject();
                        if (typeDeclaration.resolveBinding() != null) {
                            classObject.setName(typeDeclaration.resolveBinding().getQualifiedName());
                            classObject.setTypeDeclaration(typeDeclaration);

                            if (!classObject.getName().contains("Test") && !sourceFilePath.contains("test")) {

                                //Seagle Additions:
                                javaProject.putClassAndPath(classObject.getName(), sourceFilePath);
                                classObject.setLinesOfCode(systemObject.getLinesOfCodeForAFile(sourceFilePath));

                                if (typeDeclaration.isInterface()) {
                                    classObject.setInterface(true);
                                }

                                int modifiers = typeDeclaration.getModifiers();
                                if ((modifiers & Modifier.ABSTRACT) != 0) {
                                    classObject.setAbstract(true);
                                }

                                if ((modifiers & Modifier.PUBLIC) != 0) {
                                    classObject.setAccess(Access.PUBLIC);
                                } else if ((modifiers & Modifier.PROTECTED) != 0) {
                                    classObject.setAccess(Access.PROTECTED);
                                } else if ((modifiers & Modifier.PRIVATE) != 0) {
                                    classObject.setAccess(Access.PRIVATE);
                                }

                                if ((modifiers & Modifier.STATIC) != 0) {
                                    classObject.setStatic(true);
                                }

                                Type superclassType = typeDeclaration.getSuperclassType();
                                if (superclassType != null) {
                                    ITypeBinding binding = superclassType.resolveBinding();
                                    if (binding != null) {
                                        String qualifiedName = binding.getQualifiedName();
                                        TypeObject typeObject = TypeObject.extractTypeObject(qualifiedName);
                                        classObject.setSuperclass(typeObject);
                                    }
                                }

                                List<Type> superInterfaceTypes = typeDeclaration.superInterfaceTypes();
                                for (Type interfaceType : superInterfaceTypes) {
                                    ITypeBinding binding = interfaceType.resolveBinding();
                                    if (binding != null) {
                                        String qualifiedName = binding.getQualifiedName();
                                        TypeObject typeObject = TypeObject.extractTypeObject(qualifiedName);
                                        classObject.addInterface(typeObject);
                                    }
                                }
                                FieldDeclaration[] fieldDeclarations = typeDeclaration.getFields();
                                for (FieldDeclaration fieldDeclaration : fieldDeclarations) {
                                    Type fieldType = fieldDeclaration.getType();
                                    ITypeBinding binding = fieldType.resolveBinding();
                                    if (binding != null) {
                                        List<VariableDeclarationFragment> fragments = fieldDeclaration.fragments();
                                        for (VariableDeclarationFragment fragment : fragments) {
                                            String qualifiedName = binding.getQualifiedName();
                                            TypeObject typeObject = TypeObject.extractTypeObject(qualifiedName);
                                            typeObject.setArrayDimension(typeObject.getArrayDimension() + fragment.getExtraDimensions());
                                            FieldObject fieldObject = new FieldObject(typeObject, fragment.getName().getIdentifier());
                                            fieldObject.setClassName(classObject.getName());
                                            fieldObject.setVariableDeclarationFragment(fragment);

                                            int fieldModifiers = fieldDeclaration.getModifiers();
                                            if ((fieldModifiers & Modifier.PUBLIC) != 0) {
                                                fieldObject.setAccess(Access.PUBLIC);
                                            } else if ((fieldModifiers & Modifier.PROTECTED) != 0) {
                                                fieldObject.setAccess(Access.PROTECTED);
                                            } else if ((fieldModifiers & Modifier.PRIVATE) != 0) {
                                                fieldObject.setAccess(Access.PRIVATE);
                                            }

                                            if ((fieldModifiers & Modifier.STATIC) != 0) {
                                                fieldObject.setStatic(true);
                                            }

                                            classObject.addField(fieldObject);
                                        }
                                    }
                                }
                                MethodDeclaration[] methodDeclarations = typeDeclaration.getMethods();
                                for (MethodDeclaration methodDeclaration : methodDeclarations) {
                                    String methodName = methodDeclaration.getName().getIdentifier();
                                    final ConstructorObject constructorObject = new ConstructorObject();
                                    constructorObject.setMethodDeclaration(methodDeclaration);
                                    constructorObject.setName(methodName);
                                    constructorObject.setClassName(classObject.getName());

                                    int methodModifiers = methodDeclaration.getModifiers();
                                    if ((methodModifiers & Modifier.PUBLIC) != 0) {
                                        constructorObject.setAccess(Access.PUBLIC);
                                    } else if ((methodModifiers & Modifier.PROTECTED) != 0) {
                                        constructorObject.setAccess(Access.PROTECTED);
                                    } else if ((methodModifiers & Modifier.PRIVATE) != 0) {
                                        constructorObject.setAccess(Access.PRIVATE);
                                    }
                                    
 

                                    List<SingleVariableDeclaration> parameters = methodDeclaration.parameters();
                                    for (SingleVariableDeclaration parameter : parameters) {
                                        Type parameterType = parameter.getType();
                                        ITypeBinding binding = parameterType.resolveBinding();
                                        if (binding != null) {
                                            String qualifiedName = binding.getQualifiedName();
                                            TypeObject typeObject = TypeObject.extractTypeObject(qualifiedName);
                                            typeObject.setArrayDimension(typeObject.getArrayDimension() + parameter.getExtraDimensions());
                                            ParameterObject parameterObject = new ParameterObject(typeObject, parameter.getName().getIdentifier());
                                            parameterObject.setSingleVariableDeclaration(parameter);
                                            constructorObject.addParameter(parameterObject);
                                        }
                                    }
        if( 
              (constructorObject.getClassName().contains("call")&& classObject.getName().contains("FlatResponseOperator") )|| 
                (constructorObject.getClassName().contains("acquire")&& classObject.getName().contains("ConnectionPoolImpl") )||
               ( constructorObject.getClassName().contains("handle") && classObject.getName().contains("HttpConnectionHandler")  )
                ){
                                        System.out.println("");
                                        
                                    }
                                    Block methodBody = methodDeclaration.getBody();
                                    try {
                                        if (methodBody != null) {
                                            MethodBodyObject methodBodyObject = new MethodBodyObject(methodBody);
                                            constructorObject.setMethodBody(methodBodyObject);
                                        }
                                    } catch (Exception E) {
                                        logger.error( "Error while trying to create method body object: {}, Class {}, Source File Path {}", methodDeclaration.getName(), classObject.getName(), sourceFilePath);
                                    }

                                    if (methodDeclaration.isConstructor()) {
                                        classObject.addConstructor(constructorObject);
                                    } else {
                                        MethodObject methodObject = new MethodObject(constructorObject);
                                        List<IExtendedModifier> extendedModifiers = methodDeclaration.modifiers();
                                        for (IExtendedModifier extendedModifier : extendedModifiers) {
                                            if (extendedModifier.isAnnotation()) {
                                                Annotation annotation = (Annotation) extendedModifier;
                                                if (annotation.getTypeName().getFullyQualifiedName().equals("Test")) {
                                                    methodObject.setTestAnnotation(true);
                                                    break;
                                                }
                                            }
                                        }
                                        Type returnType = methodDeclaration.getReturnType2();
                                        if (returnType != null) {
                                            ITypeBinding binding = returnType.resolveBinding();
                                            if (binding != null) {
                                                String qualifiedName = binding.getQualifiedName();
                                                TypeObject typeObject = TypeObject.extractTypeObject(qualifiedName);
                                                methodObject.setReturnType(typeObject);
                                            }
                                        } else {
                                            methodObject.setReturnType(new TypeObject("void"));
                                        }
                                        if ((methodModifiers & Modifier.ABSTRACT) != 0) {
                                            methodObject.setAbstract(true);
                                        }
                                        if ((methodModifiers & Modifier.STATIC) != 0) {
                                            methodObject.setStatic(true);
                                        }
                                        if ((methodModifiers & Modifier.SYNCHRONIZED) != 0) {
                                            methodObject.setSynchronized(true);
                                        }
                                        if ((methodModifiers & Modifier.NATIVE) != 0) {
                                            methodObject.setNative(true);
                                        }

                                        classObject.addMethod(methodObject);
                                    }
                                }
                                classObjects.add(classObject);
                            }
                        }
                    }
                }
            }
            systemObject.addClasses(classObjects);
        } catch (Exception e) {
            logger.error("Exception:", e);
            logger.error( "Source file in {} exhibited a problem, it will be skipped ", sourceFilePath);
        }
    }
}
