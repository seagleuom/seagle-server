package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import gr.uom.teohaik.seagle.v3.analysis.graph.GraphVisitor;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaClassGraph extends SoftwareGraph<JavaNode, JavaEdge> implements Serializable {

    private Set<JavaPackage> packages;

    private Collection<JavaNode> nodes;
    private Collection<JavaEdge> edges;

    public JavaClassGraph() {
        packages = new HashSet<>();
        nodes = this.getVertices();
        edges = this.getEdges();
    }

    public void addPackage(JavaPackage _package) {
        packages.add(_package);
    }

    public JavaPackage getPackage(String packageName) {
        for (JavaPackage jp : packages) {
            if (jp.getName().equals(packageName)) {
                return jp;
            }
        }
        return null;
    }

    public Collection<JavaNode> getNodes() {
        for (JavaNode jn : this.getVertices()) {
            jn.toString();
        }
        return null;
    }

    @Override
    public JavaNode getNode(String name) {
        return super.getNode(name);
    }

    @Override
    public void accept(GraphVisitor graphVisitor) {
        graphVisitor.visit(this);
    }

}
