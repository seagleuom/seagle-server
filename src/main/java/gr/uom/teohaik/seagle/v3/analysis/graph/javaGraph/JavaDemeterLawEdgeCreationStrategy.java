
package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;


import gr.uom.teohaik.seagle.v3.analysis.graph.EdgeCreationStrategy;
import edu.uci.ics.jung.graph.util.EdgeType;
import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.se.util.validation.ArgsCheck;
import java.util.Collection;
import java.util.ListIterator;


/**
 *
 * @author Theodore Chaikalis
 */
public class JavaDemeterLawEdgeCreationStrategy implements EdgeCreationStrategy {

    @Override
    public void createEdges(SoftwareGraph softwareGraph, SystemObject systemObject) {
        ArgsCheck.notNull("softwareGraph", softwareGraph);
        ArgsCheck.notNull("systemObject", systemObject);
        
        ListIterator<ClassObject> classObjects = systemObject.getClassListIterator();
        Collection<JavaNode> classNames = softwareGraph.getVertices();
        while (classObjects.hasNext()) {
            ClassObject class1 = classObjects.next();
            for (JavaNode node2 : classNames) {
                if (!class1.getName().equals(node2.getName())) {
                    if (class1.isFriend(node2.getName())) {
                        AbstractNode node1 = softwareGraph.getNode(class1.getName());
                        JavaEdge myEdge = new JavaEdge(node1, node2, softwareGraph.getVersion());
                        softwareGraph.addEdge(myEdge, node1, node2, EdgeType.DIRECTED);
                        myEdge.setFriendshipDescription(class1.isFriend2(node2.getName()));
                    }
                }
            }
        }
    }

}
