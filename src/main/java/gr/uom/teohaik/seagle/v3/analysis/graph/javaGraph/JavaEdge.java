
package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 */
public  class JavaEdge extends AbstractEdge implements Serializable {

    private String from;
    private String to;
    
        
    public JavaEdge(AbstractNode sourceNode, AbstractNode targetNode, VersionDTO versionCreated) {
        super(sourceNode, targetNode, versionCreated);
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTo() {
        return to;
    }
   
}
