
package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaNode extends AbstractNode implements Serializable {

    private boolean isAbstract;
    private boolean isInterface;
    private transient JavaNode parent;
    private transient final JavaPackage javaPackage;
    private int id;
    private String label;
    public JavaNode(String name, JavaPackage _package) {
        super(name);
        javaPackage = _package;
        isAbstract = false;
        isInterface = false;
        parent = null;
    }

    public void setAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public void setInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }

    void setParent(JavaNode parentNode) {
        parent = parentNode;
    }

    public JavaNode getParent() {
        return parent;
    }

    public JavaPackage getJavaPackage() {
        return javaPackage;
    }

    public int getId() {
        return hashCode();
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return super.getName();
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public void setIsInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }

    public boolean isIsAbstract() {
        return isAbstract;
    }

    public boolean isIsInterface() {
        return isInterface;
    }
    
}
