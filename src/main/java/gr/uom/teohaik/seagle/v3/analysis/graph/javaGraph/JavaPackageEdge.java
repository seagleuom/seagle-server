
package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaPackageEdge extends AbstractEdge implements Serializable {

    public JavaPackageEdge(AbstractNode sourceNode, AbstractNode targetNode, VersionDTO versionCreated) {
        super(sourceNode, targetNode, versionCreated);
    }

    
}
