
package gr.uom.teohaik.seagle.v3.analysis.graph.javaGraph;

import gr.uom.teohaik.seagle.v3.analysis.graph.GraphVisitor;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import java.io.Serializable;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaPackageGraph extends SoftwareGraph<JavaPackage, JavaPackageEdge> implements Serializable {

    @Override
    public void accept(GraphVisitor graphVisitor) {
        graphVisitor.visit(this);
        
    }

}
