package gr.uom.teohaik.seagle.v3.analysis.metrics;

import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import gr.uom.teohaik.seagle.v3.metric.api.ExecutableMetric;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Thodoris Chaikalis
 */
public abstract class AbstractAnalysisMetric implements ExecutableMetric {

    private final Logger logger = Logger.getLogger(AbstractAnalysisMetric.class.getName());
    

    protected void storeValuesForAllNodesInMemory(String mnemonic, Map<String, Double> valuePerClass, SoftwareProject project) {
       project.putMetricValuePerClass(mnemonic, valuePerClass);
    }

    protected void storeProjectLevelAggregationMetricInMemory(String metricMnemonic, SoftwareProject project, Map<String, Double> valuePerClass, MetricAggregationStrategy aggregationStrategy) {
        Number aggregatedValue = aggregationStrategy.getAggregatedValue(valuePerClass.values());
       project.putProjectLevelMetric(metricMnemonic, aggregatedValue);
    }

}
