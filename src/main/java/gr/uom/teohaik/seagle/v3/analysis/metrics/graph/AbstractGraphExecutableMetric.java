package gr.uom.teohaik.seagle.v3.analysis.metrics.graph;

import gr.uom.teohaik.seagle.v3.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import gr.uom.teohaik.seagle.v3.metric.api.Category;
import gr.uom.teohaik.seagle.v3.model.entities.Metric;
import gr.uom.teohaik.seagle.v3.metric.api.ExecutableMetric;
import gr.uom.teohaik.seagle.v3.metric.api.Language;

/**
 *
 * @author Theodore Chaikalis
 */
public abstract class AbstractGraphExecutableMetric extends AbstractAnalysisMetric implements ExecutableMetric {

    public AbstractGraphExecutableMetric() {
        super();
    }

    public abstract void calculate(SoftwareProject softwareProject);

    @Override
    public String getCategory() {
        return Category.GRAPH_METRICS;
    }

    @Override
    public String[] getLanguages() {
        return new String[]{Language.UNSPECIFIED};
    }

    protected Metric resolveMetricByMnemonic(String mnemonic) {
        throw new UnsupportedOperationException("resolveMetricByMnemonic not implemented yet");
    }
    
}
