package gr.uom.teohaik.seagle.v3.analysis.metrics.graph;

import edu.uci.ics.jung.algorithms.metrics.Metrics;
import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
@SeagleMetric
@GraphMetric
public class ClusteringCoefficient extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "CLUSTERING_COEFFICIENT";

    @Override
    public void calculate(SoftwareProject softwareProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        SoftwareGraph<AbstractNode, AbstractEdge> softwareGraph = softwareProject.getProjectGraph();
        Map<AbstractNode, Double> clusteringCoefficients = Metrics.clusteringCoefficients(softwareGraph);
        for (AbstractNode node : softwareGraph.getVertices()) {
            double nodeBtwnCentr = clusteringCoefficients.get(node);
            valuePerClass.put(node.getName(), nodeBtwnCentr);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, softwareProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), softwareProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Calculates Clustering Coefficient of each graph node and the average of the whole graph";
    }

    @Override
    public String getName() {
        return "Clustering Coefficient";
    }

}
