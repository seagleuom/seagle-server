package gr.uom.teohaik.seagle.v3.analysis.metrics.graph;

import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;

/**
 *
 * @author Theodore Chaikalis
 */
@SeagleMetric
@GraphMetric
public class Density extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "DENSITY";

    @Override
    public void calculate(SoftwareProject softwareProject) {
        SoftwareGraph<AbstractNode, AbstractEdge> graph = softwareProject.getProjectGraph();
        double edges = graph.getEdges().size();
        double nodes = graph.getVertexCount();
        double density = edges / (nodes * (nodes - 1));
        softwareProject.putProjectLevelMetric(getMnemonic(), density);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Density of the graph."
                + "Density = |Nodes| / |Edges| * ( |Edges| - 1 )";
    }

    @Override
    public String getName() {
        return "Density";
    }

}
