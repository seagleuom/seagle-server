package gr.uom.teohaik.seagle.v3.analysis.metrics.graph;

import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;

/**
 *
 * @author Theodore Chaikalis
 */
@SeagleMetric
@GraphMetric
public class Edges extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "EDGES";


    @Override
    public void calculate(SoftwareProject softwareProject) {
        SoftwareGraph<AbstractNode, AbstractEdge> graph = softwareProject.getProjectGraph();
        int edgesCount = graph.getEdgeCount();
        softwareProject.putProjectLevelMetric(getMnemonic(), edgesCount);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of Edges for a version of the graph";
    }

    @Override
    public String getName() {
        return "Number of Edges";
    }

}
