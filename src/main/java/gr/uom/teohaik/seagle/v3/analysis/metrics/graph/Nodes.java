package gr.uom.teohaik.seagle.v3.analysis.metrics.graph;

import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;

/**
 *
 * @author Theodore Chaikalis
 */
@SeagleMetric
@GraphMetric
public class Nodes extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "NODES";

    @Override
    public void calculate(SoftwareProject softwareProject) {
         SoftwareGraph<AbstractNode, AbstractEdge> graph = softwareProject.getProjectGraph();
        int nodesCount = graph.getVertexCount();
        softwareProject.putProjectLevelMetric(getMnemonic(), nodesCount);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of nodes for a version of the graph";
    }

    @Override
    public String getName() {
        return "Number of Nodes";
    }

}
