
package gr.uom.teohaik.seagle.v3.analysis.metrics.graph;

import gr.uom.teohaik.seagle.util.GraphMetric;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
@SeagleMetric
@GraphMetric
public class OutDegree extends AbstractGraphExecutableMetric {
    
    public static final String MNEMONIC = "OUT_DEGREE";

    @Override
    public void calculate(SoftwareProject softwareProject) {
         Map<String, Double> valuePerClass = new LinkedHashMap<>();
         SoftwareGraph<AbstractNode, AbstractEdge> softwareGraph = softwareProject.getProjectGraph();
         for (AbstractNode node : softwareGraph.getVertices()) {
            int outDegree = softwareGraph.outDegree(node);
            valuePerClass.put(node.getName(), (double)outDegree);
        }    
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, softwareProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), softwareProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
         return "Out Degree of each node";
    }

    @Override
    public String getName() {
         return "Out Degree";
    }
    
}
