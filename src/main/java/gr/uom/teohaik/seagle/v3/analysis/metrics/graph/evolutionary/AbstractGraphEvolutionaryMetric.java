
package gr.uom.teohaik.seagle.v3.analysis.metrics.graph.evolutionary;

import gr.uom.teohaik.seagle.v3.analysis.graph.evolution.GraphEvolution;
import gr.uom.teohaik.seagle.v3.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.teohaik.seagle.v3.metric.api.Category;
import gr.uom.teohaik.seagle.v3.metric.api.ExecutableMetric;
import gr.uom.teohaik.seagle.v3.metric.api.Language;

/**
 *
 * @author Thodoris Chaikalis
 * created: 23/02/2016
 */
public abstract class AbstractGraphEvolutionaryMetric extends AbstractAnalysisMetric implements ExecutableMetric {

    public AbstractGraphEvolutionaryMetric(){
        super();
    }
    
    public abstract void calculate(GraphEvolution graphEvolution);

    @Override
    public String getCategory() {
        return Category.GRAPH_METRICS;
    }

    @Override
    public String[] getLanguages() {
        return new String[]{Language.UNSPECIFIED};
    }
    
}
