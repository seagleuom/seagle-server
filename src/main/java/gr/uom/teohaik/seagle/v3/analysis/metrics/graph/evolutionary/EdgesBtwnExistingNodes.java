package gr.uom.teohaik.seagle.v3.analysis.metrics.graph.evolutionary;

import gr.uom.teohaik.seagle.v3.analysis.distributions.StatUtils;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.evolution.GraphEvolution;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class EdgesBtwnExistingNodes extends AbstractGraphEvolutionaryMetric {
    
    public static final String MNEMONIC = "EDGES_BTWN_EXISTING";


    @Override
    public void calculate(GraphEvolution ge) {
        Map<VersionDTO, Set<AbstractEdge>> edgesBtwnExistingMap = new LinkedHashMap<>();
        Map<Integer, Integer> edgesBtwnExistingCountFrequencyMap = new TreeMap<>();

        VersionDTO[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            VersionDTO currentVersion = versionList[i];
            VersionDTO previousVersion = versionList[i - 1];
            
            Set<AbstractEdge> edgesBtwnExisting = new LinkedHashSet<>();
            Collection<AbstractEdge> edgesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getEdges();
            Collection<AbstractEdge> edgesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getEdges();
            
            Set<AbstractEdge> newEdges = new HashSet<>(edgesInCurrentVersion);
            newEdges.removeAll(edgesInPreviousVersion);

            Collection<AbstractNode> nodesInPreviousVersion = ge.getGraphForAVersion(previousVersion).getVertices();
            
            for(AbstractEdge edge : newEdges) {
                AbstractNode sourceNode = edge.getSourceNode();
                AbstractNode targetNode = edge.getTargetNode();
                if( nodesInPreviousVersion.contains(sourceNode) && nodesInPreviousVersion.contains(targetNode) ){
                    edgesBtwnExisting.add(edge);
                }
            }
            
            edgesBtwnExistingMap.put(currentVersion, edgesBtwnExisting);
            StatUtils.updateFrequencyMap(edgesBtwnExistingCountFrequencyMap, edgesBtwnExisting.size());
        }
        ge.setEdgesBtwnExistingMap(edgesBtwnExistingMap);
    }
    
    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of edges that attached between existing nodes during a version";
    }

    @Override
    public String getName() {
        return "Edges between existing nodes per version";
    }


}
