package gr.uom.teohaik.seagle.v3.analysis.metrics.graph.evolutionary;

import gr.uom.teohaik.seagle.v3.analysis.distributions.StatUtils;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.evolution.GraphEvolution;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class ExistingNodesPerVersion extends AbstractGraphEvolutionaryMetric {

    public static final String MNEMONIC = "EXISTING_NODES_PER_VERSION";

    @Override
    public void calculate(GraphEvolution ge) {
        Map<VersionDTO, Set<AbstractNode>> existingNodesMap = new LinkedHashMap<>();
        Map<Integer, Integer> existingNodeCountFrequencyMap = new TreeMap<>();
        VersionDTO[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            VersionDTO currentVersion = versionList[i];
            Set<AbstractNode> existingNodes = new LinkedHashSet<>();
            Collection<AbstractNode> nodesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getVertices();
            for (AbstractNode node : nodesInCurrentVersion) {
                if (node.getAge() > 0) {
                    existingNodes.add(node);
                }
            }
            existingNodesMap.put(currentVersion, existingNodes);
            StatUtils.updateFrequencyMap(existingNodeCountFrequencyMap, existingNodes.size());
        }
        ge.setExistingNodesMap(existingNodesMap);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Existing nodes per VersionDTO";
    }

    @Override
    public String getName() {
        return "Existing nodes per VersionDTO";
    }

}
