package gr.uom.teohaik.seagle.v3.analysis.metrics.graph.evolutionary;

import gr.uom.teohaik.seagle.v3.analysis.distributions.StatUtils;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.evolution.GraphEvolution;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 *
 * @author Theodore Chaikalis
 */
public class NewNodesPerVersion extends AbstractGraphEvolutionaryMetric {

    public static final String MNEMONIC = "NEW_NODES_PER_VERSION";

    @Override
    public void calculate(GraphEvolution ge) {
        Map<Integer, Integer> newNodeCountFrequencyMap = new TreeMap<>();
        Map<VersionDTO, Set<AbstractNode>> newNodesMap = new LinkedHashMap<>();
        VersionDTO[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            VersionDTO currentVersion = versionList[i];
            Set<AbstractNode> newNodes = new LinkedHashSet<>();
            Collection<AbstractNode> nodesInCurrentVersion = ge.getGraphForAVersion(currentVersion).getVertices();
            for (AbstractNode node : nodesInCurrentVersion) {
                if (node.getAge() == 0) {
                    newNodes.add(node);
                }
            }
            newNodesMap.put(currentVersion, newNodes);
            StatUtils.updateFrequencyMap(newNodeCountFrequencyMap, newNodes.size());
        }
        ge.setNewNodesMap(newNodesMap);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Nodes that have been introduced during the current version";
    }

    @Override
    public String getName() {
        return "New Nodes per Version";
    }

}
