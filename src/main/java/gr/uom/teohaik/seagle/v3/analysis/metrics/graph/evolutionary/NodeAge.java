package gr.uom.teohaik.seagle.v3.analysis.metrics.graph.evolutionary;

import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractEdge;
import gr.uom.teohaik.seagle.v3.analysis.graph.AbstractNode;
import gr.uom.teohaik.seagle.v3.analysis.graph.SoftwareGraph;
import gr.uom.teohaik.seagle.v3.analysis.graph.evolution.GraphEvolution;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
public class NodeAge extends AbstractGraphEvolutionaryMetric {

    public static final String MNEMONIC = "NODE_AGE";

    @Override
    public void calculate(GraphEvolution ge) {
        Map<VersionDTO, Map<AbstractNode, Double>> metricValues = new LinkedHashMap<>();
        VersionDTO[] versionList = ge.getVersionArray();
        for (int i = 1; i < versionList.length; i++) {
            VersionDTO currentVersion = versionList[i];
            VersionDTO previousVersion = versionList[i - 1];
            SoftwareGraph<AbstractNode, AbstractEdge> currentGraph = ge.getGraphForAVersion(currentVersion);
            SoftwareGraph<AbstractNode, AbstractEdge> previousGraph = ge.getGraphForAVersion(previousVersion);
            
            Map<AbstractNode, Double> valuePerClass = new LinkedHashMap<>();
            for (AbstractNode node : currentGraph.getVertices()) {
                try {
                    AbstractNode prevNode = previousGraph.getNode(node.getName());
                    if (prevNode != null) {
                        node.increaseAge(prevNode.getAge());
                        valuePerClass.put(node, (double)node.getAge());
                    }
                } catch (NullPointerException e) {
                    Logger.getLogger("UpdateNodeAges").log(Level.SEVERE, "Previous graph does not exist. Please check. Current version = " + currentGraph.getVersion(), e);
                }
            }
            metricValues.put(currentVersion, valuePerClass);
        }
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Age of each node";
    }

    @Override
    public String getName() {
        return "Node Age";
    }

}
