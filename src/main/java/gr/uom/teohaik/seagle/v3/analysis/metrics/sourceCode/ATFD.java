package gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.ConstructorObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import gr.uom.teohaik.seagle.v3.model.entities.Method;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Thodoris Chaikalis
 */
@SeagleMetric
@SourceCodeMetric
public class ATFD extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "ATFD";
    private static final Logger logger = Logger.getLogger(ATFD.class.getName());

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = getATFDForAllClasses(systemObject, javaProject);
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private Map<String, Double> getATFDForAllClasses(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();

        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            String className = classObject.getName();
            List<Method> methods = new ArrayList<>();
            calculateATFDForAClass(systemObject, classObject, valuePerClass, methods, javaProject.getProjectVersion());
            javaProject.putMetricValuesForMethodsOfAClass(className, methods);
        }
        return valuePerClass;
    }

    public void calculateATFDForAClass(SystemObject systemObject, ClassObject classObject,
            Map<String, Double> valuePerClass, List<Method> methods, VersionDTO version) {
        double accessToForeignData = 0.0;
        String className = classObject.getName();
        for (MethodObject method : classObject.getMethodList()) {
            Method simpleMethod = new Method(method);
            ConstructorObject con = method.getConstructorObject();
            int methodATFD = 0;
            methodATFD += con.getUsedAndDefinedFieldsOfForeignClasses().size();
            methodATFD += con.getForeignGetters(systemObject).size();
            
            simpleMethod.putMetricValue(getMnemonic(), methodATFD);
            methods.add(simpleMethod);
            accessToForeignData += methodATFD;
        }
        valuePerClass.put(className, accessToForeignData);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "ATFD represents the number of external classes from "
                + "which a given class accesses attributes, directly or via accessor-methods. "
                + "The higher the ATFD value for a class, the higher the probability that "
                + "the class is or is about to become a god-class.";
    }

    @Override
    public String getName() {
        return "Access to foreign data";
    }

}
