package gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode;

import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.v3.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import gr.uom.teohaik.seagle.v3.metric.api.Category;
import gr.uom.teohaik.seagle.v3.metric.api.ExecutableMetric;
import gr.uom.teohaik.seagle.v3.metric.api.Language;

/**
 *
 * @author Thodoris Chaikalis ver. 0.0.1 - 11/9/2015
 * ver. 0.0.2 - 6/12/15
 */
public abstract class AbstractJavaExecutableMetric extends AbstractAnalysisMetric implements ExecutableMetric {

    public abstract void calculate(SystemObject systemObject, JavaProject javaProject);

    @Override
    public String getCategory() {
        return Category.SRC_METRICS;
    }

    @Override
    public String[] getLanguages() {
        return new String[]{Language.JAVA};
    }

}
