package gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
@SeagleMetric
@SourceCodeMetric
public class CBO extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "CBO";


    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classCBO(valuePerClass, classObject, systemObject.getClassListIterator());
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private void classCBO(Map<String, Double> valuePerClass, ClassObject classObject, ListIterator<ClassObject> classIterator2) {
        int dependencies = 0;
        while (classIterator2.hasNext()) {
            String otherClass = classIterator2.next().getName();
            if (classObject.isFriend(otherClass)) {
                dependencies++;
            }
        }
        valuePerClass.put(classObject.getName(), (double) dependencies);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "The coupling between object classes (CBO) metric represents the number of classes coupled to a given class (efferent couplings, Ce). "
                + "This coupling can occur through method calls, field accesses, inheritance, arguments, return types, and exceptions.";
    }

    @Override
    public String getName() {
        return "Coupling Between Objects";
    }

}
