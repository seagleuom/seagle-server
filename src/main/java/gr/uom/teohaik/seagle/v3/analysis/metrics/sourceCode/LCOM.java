package gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.FieldObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
@SeagleMetric
@SourceCodeMetric
public class LCOM extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "LCOM2";

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classLCOM(valuePerClass, classObject);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private void classLCOM(Map<String, Double> valuePerClass, ClassObject classObject) {

        List<FieldObject> fields = classObject.getNonStaticFields();
        if (fields.isEmpty()) {
            valuePerClass.put(classObject.getName(), 0.0);
            //  lcom3Map.put(classObject.getName(), null);
        }
        List<MethodObject> methods = classObject.getNonAccessorMethods();

        if (methods.isEmpty()) {
            valuePerClass.put(classObject.getName(), 0.0);
        }

        /*
         if (methods.size() <= 1) {
         lcom3Map.put(classObject.getName(), null);
         }
         */
        int totalFieldAccesses = classObject.getNumberOfMethodsThatAccessAField();
        int a = fields.size();
        int m = methods.size();
        if (a > 0 && m > 0) {
            double lcom2 = 1.0 - (double) totalFieldAccesses / (double) (m * a);
            valuePerClass.put(classObject.getName(), lcom2);
        }
        /*
         if (a > 0 && m > 1) {
         double lcom3 = ((double) m - ((double) sum / (double) a)) / ((double) m - 1.0);
         lcom3Map.put(classObject.getName(), lcom3);
         }
         */
    }


    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "LCOM2 equals the percentage of methods that do not access a "
                + "specific attribute averaged over all attributes in the class. "
                + "If the number of methods or attributes is zero, LCOM2 is undefined and displayed as zero"
                + "Detailed specification at: http://www.aivosto.com/project/help/pm-oo-cohesion.html";
    }

    @Override
    public String getName() {
        return "Lack of Cohesion in Methods 2 (LCOM2)";
    }

}
