package gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
@SeagleMetric
@SourceCodeMetric
public class NOAM extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "NOAM";

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classNOAM(valuePerClass, classObject);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Sum);
    }

    private void classNOAM(Map<String, Double> valuePerClass, ClassObject classObject) {
        List<MethodObject> accessorMethods = classObject.getAccessorMethods();
        valuePerClass.put(classObject.getName(), (double) accessorMethods.size());

    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "NOAM is the number of the non-inherited accessor-methods declared in the interface of a class";
    }

    @Override
    public String getName() {
        return "Number of Accessor Methods of a class";
    }

}
