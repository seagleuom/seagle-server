package gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.teohaik.seagle.util.SeagleMetric;
import gr.uom.teohaik.seagle.util.SourceCodeMetric;
import gr.uom.teohaik.seagle.v3.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.teohaik.seagle.v3.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
@SeagleMetric
@SourceCodeMetric
public class WOC extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "WOC";

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classWOC(valuePerClass, classObject);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    private void classWOC(Map<String, Double> valuePerClass, ClassObject classObject) {
        List<MethodObject> nonAccessorMethods = classObject.getNonAccessorMethods();
        int numberOfPublicMembers = classObject.getNumberOfPublicMembers();
        double wocValue = 0.0;
        if (numberOfPublicMembers > 0) {
            wocValue = (double) nonAccessorMethods.size() / numberOfPublicMembers;
        }
        valuePerClass.put(classObject.getName(), wocValue);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Weight of Class is the number of non-accessor methods in a class divided by "
                + "the total number of members of the interface."
                + "";
    }

    @Override
    public String getName() {
        return "Weight of Class";
    }

}
