package gr.uom.teohaik.seagle.v3.analysis.project.evolution;

import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

/**
 *
 * @author Theodore Chaikalis
 */
public class JavaProject extends SoftwareProject implements Serializable {

    private HashMap<String, String> classNameToClassPathMap;

    public JavaProject() {
        super();
    }

    public JavaProject(VersionDTO version, String projectName, String sourceFolderForThisRevision, VCSCommit vcsCommit) throws VCSRepositoryException {
        super(projectName, version, sourceFolderForThisRevision);
        classNameToClassPathMap = new LinkedHashMap<>();
        createJavaFilePaths(vcsCommit);
    }

    public void setClassNameToClassPathMap(LinkedHashMap<String, String> classNameToClassPathMap) {
        this.classNameToClassPathMap = classNameToClassPathMap;
    }

    public void putClassAndPath(String classFullyQualifiedName, String classPath) {
        classNameToClassPathMap.put(classFullyQualifiedName, classPath);
    }

    public String getPathOfClass(String className) {
        return classNameToClassPathMap.get(className);
    }

    private void createJavaFilePaths(VCSCommit vcsCommit) throws VCSRepositoryException {
        JavaFileVisitor jff = new JavaFileVisitor(this);
        vcsCommit.walkTree(jff);
    }

    public void addJavaFilePath(String javaFilePath) {
        sourceFilePaths.add(javaFilePath);
    }

    public Set<String> getJavaFilePaths() {
        return sourceFilePaths;
   }
//    
//    
//    public void persist() throws IOException {
//        Kryo kryo = new Kryo();
//        Path objectPath = Paths.get(sourceFolderForThisRevision, PROJECT_OBJECT_NAME);
//        try (Output output = new Output(new FileOutputStream(objectPath.toAbsolutePath().toString()))) {
//            kryo.writeClassAndObject(output, this);
//        }
//    }
//    
//    private static final Logger logger = Logger.getLogger(JavaProject.class.getName());
// 
//    public static SoftwareProject load(String projectPath) {
//        Kryo kryo = new Kryo();
//        String filePath = getAbsolutePathOfProjectObject(projectPath);
//        try (Input input = new Input(new FileInputStream(filePath));) {
//            SoftwareProject project = (JavaProject) kryo.readClassAndObject(input);
//            return project;
//        } catch (Exception ex) {
//            logger.log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }


}
