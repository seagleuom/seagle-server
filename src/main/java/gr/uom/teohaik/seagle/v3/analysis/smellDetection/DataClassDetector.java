//package gr.uom.teohaik.seagle.v3.analysis.smellDetection;
//
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.NOAM;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.NOPA;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.WMC;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.WOC;
//import gr.uom.teohaik.seagle.v3.model.entities.BadSmell;
//import gr.uom.teohaik.seagle.v3.model.entities.Node;
//import gr.uom.teohaik.seagle.v3.model.entities.Project;
//import gr.uom.teohaik.seagle.v3.model.entities.Version;
//import java.util.Collection;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author Thodoris Chaikalis version 0.1 20/10/15
// */
//public class DataClassDetector extends AbstractSmellDetector {
//
//    public static final String MNEMONIC = "DATA_CLASS_DETECTOR";
//    private static final Logger logger = Logger.getLogger(DataClassDetector.class.getName());
//
//    public DataClassDetector() {
//        super( MNEMONIC);
//    }
//
//    @Override
//    public void detectSmells(Project project) {
//       
//    }
//
//}
