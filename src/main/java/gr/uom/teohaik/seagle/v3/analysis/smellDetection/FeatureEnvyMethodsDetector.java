//package gr.uom.teohaik.seagle.v3.analysis.smellDetection;
//
//import gr.uom.teohaik.seagle.v3.SeagleManager;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.ATFD;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.FDP;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.LAA;
//import gr.uom.teohaik.seagle.v3.model.entities.BadSmell;
//import gr.uom.teohaik.seagle.v3.model.entities.Method;
//import gr.uom.teohaik.seagle.v3.model.entities.Node;
//import gr.uom.teohaik.seagle.v3.model.entities.Project;
//import gr.uom.teohaik.seagle.v3.model.entities.Version;
//import java.util.Collection;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author Thodoris Chaikalis version 0.1 20/10/15
// */
//public class FeatureEnvyMethodsDetector extends AbstractSmellDetector {
//
//    public static final String MNEMONIC = "FEATURE_ENVY_DETECTOR";
//    private static final Logger logger = Logger.getLogger(FeatureEnvyMethodsDetector.class.getName());
//
//    public FeatureEnvyMethodsDetector(SeagleManager seagleManager) {
//        super(seagleManager, MNEMONIC);
//    }
//
//    @Override
//    public void detectSmells(Project project) {
//        BadSmell featureEnvyBadSmell = SmellManager.getSmell(BadSmellEnum.FEATURE_ENVY.getMnemonic());
//        for (Version version : project.getVersions()) {
//            logger.log(Level.INFO, "Detecting Feature Envy methods for version : {0}", version.getName());
//
//            Collection<Node> vertices = nodeFacade.findByVersion(version);
//
//            for (Node dbNode : vertices) {
//                if (!dbNode.hasSmell(featureEnvyBadSmell)) {
//                    try {
//                        String nodeName = dbNode.getName();
//                        Collection<Method> methods = dbNode.getMethods();
//
//                        for (Method method : methods) {
//                            Number atfdForMethod = method.getMetricValue(ATFD.MNEMONIC);
//                            Number laaForMethod = method.getMetricValue(LAA.MNEMONIC);
//                            Number fdpForMethod = method.getMetricValue(FDP.MNEMONIC);
//
//                            if (atfdForMethod != null && laaForMethod != null && fdpForMethod != null) {
//                                boolean fewForeignDataProviders = (fdpForMethod.doubleValue() <= few);
//                                boolean highAccessToForeignData = (atfdForMethod.doubleValue() > few);
//                                boolean lowLocalityOfAttributeAccess = (laaForMethod.doubleValue() < featureEnvyLAALevel);
//
//                                if (highAccessToForeignData && fewForeignDataProviders && lowLocalityOfAttributeAccess) {
//                                    method.addBadSmell(featureEnvyBadSmell);
//                                    dbNode.addBadSmell(featureEnvyBadSmell);
//                                    nodesToUpdate.add(dbNode);
//                                    logger.log(Level.INFO, "Feature smell detected. Smelly Method {0} Class {1}", new Object[]{method, nodeName});
//                                }
//                            }
//                        }
//                    } catch (Exception e) {
//                        logger.log(Level.INFO, "exception trhown while detecting Feature Envy smell for node {0} version: {1}", new Object[]{dbNode.getName(), version.getName()});
//                    }
//                }
//            }
//        }
//    }
//}
