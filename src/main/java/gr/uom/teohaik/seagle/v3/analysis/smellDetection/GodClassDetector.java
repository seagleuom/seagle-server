//package gr.uom.teohaik.seagle.v3.analysis.smellDetection;
//
//import gr.uom.teohaik.seagle.v3.SeagleManager;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.ATFD;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.TCC;
//import gr.uom.teohaik.seagle.v3.analysis.metrics.sourceCode.WMC;
//import gr.uom.teohaik.seagle.v3.model.entities.Node;
//import gr.uom.teohaik.seagle.v3.model.entities.Project;
//import gr.uom.teohaik.seagle.v3.model.entities.BadSmell;
//import gr.uom.teohaik.seagle.v3.model.entities.Version;
//import java.util.Collection;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author Thodoris Chaikalis
// */
//public class GodClassDetector extends AbstractSmellDetector {
//
//    private static final Logger logger = Logger.getLogger(GodClassDetector.class.getName());
//
//    public static final String MNEMONIC = "GOD_CLASS_DETECTOR";
//
//    public GodClassDetector(SeagleManager seagleManager) {
//        super(seagleManager, MNEMONIC);
//    }
//
//    @Override
//    public void detectSmells(Project project) {
//        BadSmell godClassSmell = SmellManager.getSmell(BadSmellEnum.GOD_CLASS.getMnemonic());
//
//        for (Version version : project.getVersions()) {
//            logger.log(Level.INFO, "Detecting God Class for version : {0}", version.getName());
//            Map<String, Double> perClassATFD = getMetricValuesPerClass(version, ATFD.MNEMONIC);
//            Map<String, Double> perClassWMC = getMetricValuesPerClass(version, WMC.MNEMONIC);
//            Map<String, Double> perClassTCC = getMetricValuesPerClass(version, TCC.MNEMONIC);
//
//            Collection<Node> vertices = nodeFacade.findByVersion(version);
//
//            if (perClassATFD != null && perClassWMC != null && perClassTCC != null) {
//
//                for (Node dbNode : vertices) {
//                    if (!dbNode.hasSmell(godClassSmell)) {
//                        try{
//                            String nodeName = dbNode.getName();
//
//                            double atfd = perClassATFD.get(nodeName);
//                            double tcc = perClassTCC.get(nodeName);
//                            double wmc = perClassWMC.get(nodeName);
//
//                            List<String> topATFD = getClassesThatAreInTopPercentOfMetric(perClassATFD, ATFD.MNEMONIC, godClassATFD_TOP_VALUES_PERCENT);
//
//                            boolean isInTopATFD = false;
//                            if (topATFD.contains(nodeName)) {
//                                isInTopATFD = true;
//                            }
//
//                            if (isInTopATFD
//                                    && atfd > godClassATFD_LOWER_LIMIT
//                                    && ((wmc > godClassWMC_LOWER_LIMIT) || (tcc < godClassTCC_HIGHER_LIMIT))) {
//                                dbNode.addBadSmell(godClassSmell);
//                                nodesToUpdate.add(dbNode);
//                            }
//                        }catch(Exception e){
//                            logger.log(Level.INFO, "exception trhown while detecting God class smell for node {0} version: {1}", new Object[]{dbNode.getName(), version.getName()});
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//}
