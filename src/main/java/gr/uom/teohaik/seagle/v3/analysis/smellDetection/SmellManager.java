//package gr.uom.teohaik.seagle.v3.analysis.smellDetection;
//
//import gr.uom.teohaik.seagle.v3.SeagleManager;
//import gr.uom.teohaik.seagle.v3.model.entities.BadSmell;
//import gr.uom.teohaik.seagle.v3.model.entities.controllers.BadSmellFacade;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// *
// * @author Thodoris Chaikalis
// */
//public class SmellManager {
//
//    private static final Map<String, BadSmell> smellMap = new HashMap<>();
//    private static final Map<String, AbstractSmellDetector> smellToDetectorsMap = new HashMap<>();
//
//    public static void createSmells(BadSmellFacade smellFacade, SeagleManager seagleManager) {
//        for (BadSmellEnum smellEnum : BadSmellEnum.values()) {
//            List<BadSmell> smellsFound = smellFacade.findByName(smellEnum.getName());
//            if (smellsFound != null && !smellsFound.isEmpty()) {
//                smellMap.put(smellEnum.getMnemonic(), smellsFound.get(0));
//            } else {
//                BadSmell smell = new BadSmell(smellEnum.getName(), smellEnum.getDescription());
//                smellMap.put(smellEnum.getMnemonic(), smell);
//                smellFacade.create(smell);
//            }
//        }
//        createSmellDetectors(seagleManager);
//    }
//
//    private static void createSmellDetectors(SeagleManager seagleManager) {
//        smellToDetectorsMap.put(GodClassDetector.MNEMONIC, new GodClassDetector(seagleManager));
//        smellToDetectorsMap.put(DataClassDetector.MNEMONIC, new DataClassDetector(seagleManager));
//       smellToDetectorsMap.put(FeatureEnvyMethodsDetector.MNEMONIC, new FeatureEnvyMethodsDetector(seagleManager));
//    }
//
//    public static BadSmell getSmell(String mnemonic) {
//        return smellMap.get(mnemonic);
//    }
//
//    public static Collection<AbstractSmellDetector> getDetectionStrategies() {
//        return smellToDetectorsMap.values();
//    }
//
//    public static void putDetectionStrategy(String badSmellMnemonic, AbstractSmellDetector detectionStrategy) {
//        smellToDetectorsMap.put(badSmellMnemonic, detectionStrategy);
//    }
//
//    public static AbstractSmellDetector getDetectionStrategy(String badSmellMnemonic) {
//        return smellToDetectorsMap.get(badSmellMnemonic);
//    }
//
//}
