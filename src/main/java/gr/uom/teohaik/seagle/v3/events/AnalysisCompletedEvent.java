package gr.uom.teohaik.seagle.v3.events;

import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;

/**
 *
 * @author Theodore Chaikalis
 */
public class AnalysisCompletedEvent {
    
    private ProjectDTO project;

    public AnalysisCompletedEvent(ProjectDTO project) {
       this.project = project;
    }

    public ProjectDTO getProject() {
        return project;
    }

    
}