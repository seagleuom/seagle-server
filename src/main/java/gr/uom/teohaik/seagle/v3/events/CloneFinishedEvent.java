package gr.uom.teohaik.seagle.v3.events;

/**
 *
 * @author Theodore Chaikalis
 */
public class CloneFinishedEvent {
    
    private String projectUrl;

    public CloneFinishedEvent(String projectUrl) {
       this.projectUrl = projectUrl;
    }

    public String getProjectUrl() {
        return projectUrl;
    }
    
}