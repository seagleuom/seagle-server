package gr.uom.teohaik.seagle.v3.events;

import gr.uom.teohaik.seagle.v3.model.entities.Project;

/**
 *
 * @author Theodore Chaikalis
 */
public class DBStorageRequest {
    
    private Project project;

    public DBStorageRequest(Project project) {
       this.project = project;
    }

    public Project getProject() {
        return project;
    }

    
}