package gr.uom.teohaik.seagle.v3.events;

import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;

/**
 *
 * @author Theodore Chaikalis
 */
public class ProjectAnalysisRequest {
    
    private final String email;
    private final String requestUrl;
    private ProjectDTO project;

    public ProjectAnalysisRequest(ProjectDTO project) {
        this.project = project;
        this.email = project.getCreatedBy();
        this.requestUrl = project.getRemoteRepoPath();
    }

    public ProjectAnalysisRequest(String email, String requestUrl) {
        this.email = email;
        this.requestUrl = requestUrl;
    }

    public ProjectDTO getProject() {
        return project;
    }

    public String getEmail() {
        return email;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    @Override
    public String toString() {
        return "ProjectAnalysisRequest{" +
                "email='" + email + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                '}';
    }
}