package gr.uom.teohaik.seagle.v3.events;

import gr.uom.teohaik.seagle.v3.model.entities.Project;

/**
 *
 * @author Theodore Chaikalis
 */
public class VersionDeterminationRequest {

    private Project project;
    private final String email;
    private final String requestUrl;
    private boolean triggerAnalysisAfterDeterminingVersions;

    public VersionDeterminationRequest(Project project) {
        this.project = project;
        this.email = project.getCreatedBy();
        this.requestUrl = project.getRemoteRepoPath();
    }

    public VersionDeterminationRequest(String email, String requestUrl) {
        this.email = email;
        this.requestUrl = requestUrl;
    }

    public VersionDeterminationRequest(String email, String requestUrl, boolean triggerAnalysisAfterDeterminingVersions) {
        this.email = email;
        this.requestUrl = requestUrl;
        this.triggerAnalysisAfterDeterminingVersions = triggerAnalysisAfterDeterminingVersions;
    }

    public Project getProject() {
        return project;
    }

    public String getEmail() {
        return email;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public boolean shouldTriggerAnalysisAfterDeterminingVersions() {
        return triggerAnalysisAfterDeterminingVersions;
    }

    public void setTriggerAnalysisAfterDeterminingVersions(boolean triggerAnalysisAfterDeterminingVersions) {
        this.triggerAnalysisAfterDeterminingVersions = triggerAnalysisAfterDeterminingVersions;
    }

    @Override
    public String toString() {
        return "ProjectAnalysisRequest{" +
                "email='" + email + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", triggerAnalysisAfterDeterminingVersions='" + triggerAnalysisAfterDeterminingVersions + '\'' +
                '}';
    }
}