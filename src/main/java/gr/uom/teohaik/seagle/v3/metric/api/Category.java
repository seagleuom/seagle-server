package gr.uom.teohaik.seagle.v3.metric.api;

/**
 *
 * @author Theodore Chaikalis
 */
public class Category {

   /**
    * Used for unspecified category, when a metric belongs to no category.
    */
   public final static String UNSPECIFIED = "UNSPECIFIED";

   /**
    * Metrics related to repository (such as commit count).
    */
   public final static String REPO_METRICS = "REPO_METRICS";

   /**
    * Metrics related to the contents of files within a project.
    * <p>
    * This doesn't necessary require a project checkout on commit, because
    * contents of a file in a given commit can be simply retrieved using a walk
    * on commit.
    */
   public final static String CONTENT_METRICS = "CONTENT_METRICS";

   /**
    * Metrics related to the source code of a project.
    * <p>
    * This family of metrics are strongly connected to the source code of a
    * project artifact. They usually perform static analysis or requires the
    * code to be compilable before he can be analyzed. Therefore metrics of this
    * category requires (usually) the existence of a checkout folder were to
    * read the source code.
    */
   public final static String SRC_METRICS = "SRC_METRICS";
   
      /**
    * Metrics related to graph.
    * <p>
    */
   public final static String GRAPH_METRICS = "GRAPH_METRICS";
   
   public final static String SMELL_DETECTORS = "SMELL_DETECTORS";

   public static enum Enum {

      /**
       * Used for unspecified category, when a metric belongs to no category.
       */
      UNSPECIFIED("Used for unspecified category, when a metric belongs to no category."),
      /**
       * Metrics related to repository (such as commit count).
       */
      REPO_METRICS("Metrics related to repository (such as commit count)."),
      /**
       * Metrics related to the contents of files within a project.
       * <p>
       * This doesn't necessary require a project checkout on commit, because
       * contents of a file in a given commit can be simply retrieved using a
       * walk on commit.
       */
      CONTENT_METRICS(new StringBuilder("Metrics related to the contents of files within a project.\n")
              .append("This doesn't necessary require a project checkout on commit, because\n")
              .append("contents of a file in a given commit can be simply retrieved using a\n")
              .append("walk on commit.").toString()),
      /**
       * Metrics related to the source code of a project.
       * <p>
       * This family of metrics are strongly connected to the source code of a
       * project artifact. They usually perform static analysis or requires the
       * code to be compilable before he can be analyzed. Therefore metrics of
       * this category requires (usually) the existence of a checkout folder
       * were to read the source code.
       */
      SRC_METRICS(new StringBuilder("Metrics related to the source code of a project.\n")
              .append("This family of metrics are strongly connected to the source code of a\n")
              .append("project artifact. They usually perform static analysis or requires the\n")
              .append("code to be compilable before he can be analyzed. Therefore metrics of\n")
              .append("this category requires (usually) the existence of a checkout folder\n")
              .append("were to read the source code.").toString()),

      
      SMELL_DETECTORS("Smell detectors. These classes detect code smells based on metric values"),
      
      GRAPH_METRICS("Graph metrics");
              
      final String description;

      private Enum(String desc) {
         this.description = desc;
      }
      
      public String description() {
         return this.description;
      }
   }
}
