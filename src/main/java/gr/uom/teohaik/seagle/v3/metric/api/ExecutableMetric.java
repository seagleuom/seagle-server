package gr.uom.teohaik.seagle.v3.metric.api;

/**
 * A metric object to describe a metric, just for information when it is used by
 * a metric handler.
 * <p>
 * 
 * @author Theodore Chaikalis
 */
public interface ExecutableMetric {

   /**
    * @return the mnemonic of the metric
    */
   String getMnemonic();

   /**
    * @return the description of the metric
    */
   String getDescription();

   /**
    * @return the name of the metric
    */
   String getName();

   /**
    * @return the category of the metric
    */
   String getCategory();

   /**
    * @return mnemonics of programming languages of the metric
    */
   String[] getLanguages();
}
