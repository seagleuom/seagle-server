package gr.uom.teohaik.seagle.v3.model.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Thodoris Chaikalis
 */
@Entity
@Table(name = "bad_smell")
@NamedQueries({
    @NamedQuery(name = "BadSmell.findAll", query = "SELECT bs FROM BadSmell bs"),
    @NamedQuery(name = "BadSmell.findById", query = "SELECT bs FROM BadSmell bs WHERE bs.id = :id"),
    @NamedQuery(name = "BadSmell.findByName", query = "SELECT bs FROM BadSmell bs WHERE bs.name = :name")})
public class BadSmell extends Thing {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = true)
    @NotNull
    @Lob
    @Column(name = "description")
    private String description;

    public BadSmell(){}

    public BadSmell(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
       
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "gr.uom.teohaik.seagle.v3.model.entities.BadSmell[ id=" + id + " ]";
    }

}
