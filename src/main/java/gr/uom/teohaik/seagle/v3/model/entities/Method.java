package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.java.ast.MethodObject;
import gr.uom.java.ast.TypeObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * A node is an entity within system representing a source file of a project.
 * <p>
 *
 * This entity contains info about a node, such as its path within system, its
 * versions and the computed metrics.
 *
 * @author Elvis Ligu
 */
@Entity
@Table(name = "method")
@NamedQueries({
    @NamedQuery(name = "Method.findAll", query = "SELECT n FROM Method n"),
    @NamedQuery(name = "Method.findById", query = "SELECT n FROM Method n WHERE n.id = :id"),
    @NamedQuery(name = "Method.findByName", query = "SELECT n FROM Method n WHERE n.name = :name")
})
public class Method extends Thing {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "name", length = 1000)
    private String name;

    @Basic(optional = false)
    @NotNull
    @Column(name = "simpleName", length = 100)
    private String simpleName;

    @JoinColumn(name = "node_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Node node;

    @JoinTable(name = "method_smells", joinColumns = {
        @JoinColumn(name = "method_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "smell_id", referencedColumnName = "id")})
    @ManyToMany
    private Collection<BadSmell> badSmells;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MetricValue> metricValues;
    
    private transient Map<String, Number> metricValuesMap;

    private transient String lastInsertedMetric;

    public Method() {
        super();
        this.metricValuesMap = new HashMap<>();
        this.metricValues = new HashSet<>();
        this.badSmells = new HashSet<>();
    }

    public Method(MethodObject methodObject) {
        super();
        this.badSmells = new HashSet<>();
        this.metricValues = new HashSet<>();
        this.metricValuesMap = new HashMap<>();
        String methodName = methodObject.getName();
        this.simpleName = methodName;
        String className = methodObject.getClassName();
        String parameters = "";
        for (TypeObject parameterType : methodObject.getConstructorObject().getParameterTypeList()) {
            parameters += parameterType.getClassType() + " , ";
        }
        parameters = parameters.trim();
        if (parameters.lastIndexOf(",") != -1) {
            parameters = parameters.substring(0, parameters.length() - 1);
        }
        this.name = className + "::" + methodName + "(" + parameters + ")";
        this.lastInsertedMetric = null;
    }

    public void addMetricValue(MetricValue metricValue) {
        metricValues.add(metricValue);
        //this.lastInsertedMetric = metricValue.getMetric().getMnemonic();
    }

    public String getLastInsertedMetric() {
        return lastInsertedMetric;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String fullyQualifiedName) {
        this.name = fullyQualifiedName;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Collection<MetricValue> getMetricValues() {
        return metricValues;
    }

    public void setMetricValues(Collection<MetricValue> metrics) {
        this.metricValues = metrics;
    }

    public void addBadSmell(BadSmell featureEnvyBadSmell) {
        this.badSmells.add(featureEnvyBadSmell);
    }

    public Collection<BadSmell> getBadSmells() {
        return badSmells;
    }

    public void setBadSmells(Collection<BadSmell> badSmells) {
        this.badSmells = badSmells;
    }


    public void putMetricValue(String metricMnemonic, Number value) {
        metricValuesMap.put(metricMnemonic, value);
        setLastInsertedMetric(metricMnemonic);
    }
    
    public Number getMetricValueFromMap(String mnemonic){
        return metricValuesMap.get(mnemonic);
    }

    public void setLastInsertedMetric(String lastInsertedMetric) {
        this.lastInsertedMetric = lastInsertedMetric;
    }
    
    public boolean containsMetric(String metricMnemonic){
        return metricValuesMap.keySet().contains(metricMnemonic);
    }

    public Map<String, Number> getMetricValuesMap() {
        return metricValuesMap;
    }

    @Override
    public String toString() {
        return simpleName;
    }

}
