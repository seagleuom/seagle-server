package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.util.ArgsCheck;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Represents a metric within system.
 * <p>
 * A metric is an instance that describes some quality characteristics of a
 * project. Each metric may have other metric dependencies, for example a CBO 
 * metric applied to a project may have
 * dependency a source file metric.
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "metric",
        indexes = {@Index(name = "mnemonic_index",  columnList="mnemonic", unique = true)})
public class Metric extends Thing {

    private static final long serialVersionUID = 1L;
    /**
     * Id of this instance.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * A unique keyword of this metric.
     * <p>
     * Max of 45 characters. May not be null or empty.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "mnemonic", unique = true)
    private String mnemonic;

    /**
     * Name of this metric.
     * <p>
     * Max of 255 characters. May not be null or empty.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name")
    private String name;

    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "implementationClass")
    private String implementationClass;

    /**
     * Description of this metric.
     * <p>
     * Max of 65535 characters.
     */
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;

    /**
     * Programming languages this metric support.
     * <p>
     * Optional property.
     */
    @ManyToMany(mappedBy = "metrics", fetch = FetchType.LAZY)
    private Collection<ProgrammingLanguage> programmingLanguages;

    /**
     * The dependencies of this metric.
     * <p>
     * Optional property.
     */
    @JoinTable(name = "metric_dependency", joinColumns = {
        @JoinColumn(name = "metric_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "metric_dependency_id", referencedColumnName = "id")})
   @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Metric> metricDependencies;

    /**
     * Projects this metric is registered to.
     * <p>
     */
    @JoinTable(name = "project_registered_metrics", joinColumns = {
        @JoinColumn(name = "metric_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "project_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Project> registeredProjects;

    /**
     * Category where this metric belongs to.
     * <p>
     */
    @JoinColumn(name = "metric_category_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MetricCategory category;

    /**
     * Create an instance of this metric.
     * <p>
     */
    public Metric() {
        super();
    }

    /**
     * @return the id of this metric
     */
    public Long getId() {
        return id;
    }

    /**
     * @return a unique keyword for this metric
     */
    public String getMnemonic() {
        return mnemonic;
    }

    /**
     * Set a unique keyword for this metric.
     * <p>
     * @param mnemonic must not be null not or empty. Max of 45 characters.
     */
    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    /**
     * @return the name of this metric
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this metric.
     * <p>
     * @param name of this metric. Max of 255 characters.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description of this metric
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of this metric.
     * <p>
     * @param description A large text max of 65K characters
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the programming languages this metric applies to.
     */
    //@XmlTransient
    public Collection<ProgrammingLanguage> getProgrammingLanguages() {
        return programmingLanguages;
    }

    /**
     * Set the programming languages this metric applies to.
     * <p>
     * @param programmingLanguages the languages this metric applies to
     */
    public void setProgrammingLanguages(Collection<ProgrammingLanguage> programmingLanguages) {
        this.programmingLanguages = programmingLanguages;
    }

    /**
     * Add a language where this metric applies to.
     * <p>
     * @param language this metric applies to. Must not be null.
     */
    public void addProgrammingLanguage(ProgrammingLanguage language) {
        ArgsCheck.notNull("language", language);
        if (programmingLanguages == null) {
            programmingLanguages = new HashSet<>();
        }
        programmingLanguages.add(language);
    }

    /**
     * Remove a language where this metric applies to.
     * <p>
     * @param language to be removed. Must not be null.
     */
    public void removeProgrammingLanguage(ProgrammingLanguage language) {
        ArgsCheck.notNull("language", language);
        if (programmingLanguages != null) {
            programmingLanguages.remove(language);
        }
    }

    /**
     * @return the metrics this metric depends on.
     */
    public Collection<Metric> getMetricDependencies() {
        return metricDependencies;
    }

    /**
     * @param metricDependencies set the dependencies of this metric.
     */
    public void setMetricDependencies(Collection<Metric> metricDependencies) {
        this.metricDependencies = metricDependencies;
    }

    /**
     * Add a dependency for this metric.
     * <p>
     * @param metric dependency of this metric. Must not be null.
     */
    public void addDependency(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (metricDependencies == null) {
            metricDependencies = new HashSet<>();
        }
        metricDependencies.add(metric);
    }

    /**
     * Remove a dependency from this metic.
     * <p>
     * @param metric the dependency of this metric to be removed. Must not be
     * null.
     */
    public void removeDependency(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (metricDependencies != null) {
            metricDependencies.remove(metric);
        }
    }

    /**
     * @return the projects registering to metric
     */
    public Collection<Project> getRegisteredProjects() {
        return registeredProjects;
    }

    /**
     * Set the projects that this metric is registered for future calculation.
     * <p>
     * When a project is inserted into the system, a list of metrics will be
     * registered to this project. After an event of calculate metrics all
     * registered metrics for a given project will be calculated.
     *
     * @param registeredProjects the project this metric is registered to.
     */
    public void setRegisteredProjects(Collection<Project> registeredProjects) {
        this.registeredProjects = registeredProjects;
    }


    /**
     * @return the category of this metric 
     */
    public MetricCategory getCategory() {
        return category;
    }

    /**
     * Set the category of this metric.
     * @param category where this metric belongs to.
     */
    public void setCategory(MetricCategory category) {
        this.category = category;
    }
    
    public String getImplementationClass() {
        return implementationClass;
    }

    public void setImplementationClass(String implementationClass) {
        this.implementationClass = implementationClass;
    }


    @Override
    public String toString() {
        return "db.Metric[ id=" + id + " ],["+name+"]";
    }

}
