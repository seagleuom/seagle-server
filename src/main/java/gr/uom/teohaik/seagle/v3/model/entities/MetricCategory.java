package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.util.ArgsCheck;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class represent the category of a metric.
 * <p>
 * @author Theodore Chaikalis
 * 
 */
@Entity
@Table(name = "metric_category")
public class MetricCategory extends Thing {

    private static final long serialVersionUID = 1L;
    
    public enum CATEGORY_NAMES {SourceCodeMetrics, GraphMetrics};

    /**
     * The id of this entity.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * The category of this metric.
     * <p>
     * Should be a unique single word, max of 255 characters.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "categoryName", unique = true)
    private String categoryName;

    /**
     * The description of this metric.
     * <p>
     * May be a large text max of 65K characters.
     */
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;

    /**
     * Metrics of this category.
     * <p>
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category", orphanRemoval = true)
    private Collection<Metric> metrics;

    /**
     * Create an empty instance.
     * <p>
     */
    public MetricCategory() {
        super();
        metrics = new HashSet<>();
    }

    /**
     * @return the id of this category
     */
    public Long getId() {
        return id;
    }

    /**
     * @return a unique keyword of this category
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Set a unique keyword of this category.
     * <p>
     * @param categoryName a unique keyword of this category, max of 45 characters
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * @return the description of this category
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of this category.
     * <p>
     * @param description of this category, max of 65K characters
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the metrics of this category 
     */
    public Collection<Metric> getMetrics() {
        return metrics;
    }

    /**
     * Set the metrics of this category.
     * <p>
     * @param metrics of this category 
     */
    public void setMetrics(Collection<Metric> metrics) {
        this.metrics = metrics;
    }

    /**
     * Add a metric to this category.
     * <p>
     * @param metric to be added to this category, must not be null.
     */
    public void addMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        metrics.add(metric);
    }
    
    public void removeMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if(metrics != null) {
            metrics.remove(metric);
        }
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.db.persistence.v2.MetricCategory[ id=" + id + " ]";
    }

}
