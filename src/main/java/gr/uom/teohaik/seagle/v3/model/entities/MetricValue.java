package gr.uom.teohaik.seagle.v3.model.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Represents a computed value for a given metric.
 * <p>
 * In order to allow for extendibility the system specifies a generic value
 * entity that can be used for all kind of metrics. Each value is related to a
 * metric and to an entity that it was computed for. For example a metric that
 * measures the complexity of a given project is computed for a given project
 * version. The computed value is an instance of this type and contains the
 * project version, for which it was computed.
 * <p>
 * Although, some metrics have integer values, all the metrics have a type of
 * double, to support the widest possible range.
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "metric_value")
public class MetricValue extends Thing {

   private static final long serialVersionUID = 1L;

   /**
    * The id of this value.
    * <p>
    */
   @Id
   @GeneratedValue(strategy = GenerationType.SEQUENCE)
   @Column(name = "id")
   private Long id;

   /**
    * The computed value.
    * <p>
    */
   @Basic(optional = false)
   @NotNull
   @Column(name = "value")
   private double value;


   /**
    * The metric type of this value.
    * <p>
    */
   @NotNull
   @Column(name = "metric_id")
   private Long metricId;

   /**
    * The node id
    * <p></p>
    */
   @Column(name = "node_id")
   private Long nodeId;

   /**
    * The version this value was computed for.
    * <p>
    */
   @Column(name = "version_id")
   private Long versionId;

   /**
    * The projects this value was computed for.
    * <p>
    */
   @Column(name = "project_id")
   private Long projectId;

   /**
    * Create an empty instance.
    * <p>
    */
   public MetricValue() {
      super();
   }

   /**
    * Create an instance given its id.
    * <p>
    * 
    * @param id
    *           of this instance. Auto-generated value.
    */
   public MetricValue(Long id) {
      super();
      this.id = id;
   }

   /**
    * @return the id of this value
    */
   public Long getId() {
      return id;
   }

   /**
    * @return value of this entity
    */
   public double getValue() {
      return value;
   }

   /**
    * Set the value of this entity.
    * <p>
    * 
    * @param value
    *           of this entity
    */
   public void setValue(double value) {
      this.value = value;
   }

   public Long getMetricId() {
      return metricId;
   }

   public void setMetricId(Long metricId) {
      this.metricId = metricId;
   }

   public Long getNodeId() {
      return nodeId;
   }

   public void setNodeId(Long nodeId) {
      this.nodeId = nodeId;
   }

   public Long getVersionId() {
      return versionId;
   }

   public void setVersionId(Long versionId) {
      this.versionId = versionId;
   }

   public Long getProjectId() {
      return projectId;
   }

   public void setProjectId(Long projectId) {
      this.projectId = projectId;
   }

   @Override
   public String toString() {
      return "MetricValue{" +
              "id=" + id +
              ", value=" + value +
              ", metricId=" + metricId +
              ", nodeId=" + nodeId +
              ", versionId=" + versionId +
              ", projectId=" + projectId +
              '}';
   }
}
