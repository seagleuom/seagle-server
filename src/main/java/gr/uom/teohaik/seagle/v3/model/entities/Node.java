package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.util.ArgsCheck;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * A node is an entity within system representing a source file of a project.
 * <p>
 *
 * This entity contains info about a node, such as its path within system, its
 * versions and the computed metrics.
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "node")
public class Node extends Thing {

    private static final long serialVersionUID = 1L;

    /**
     * Node's id.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * The fully qualified name within its package.
     * <p>
     * See Java source files.
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "name", length = 512)
    private String name;

    /**
     * The name of the node.
     * <p>
     * If it is a source file, the file name.
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "simpleName")
    private String simpleName;

    /**
     * The full path within repository of this node.
     * <p>
     */
    @Basic(optional = false)
    @NotNull
    @Column(name = "sourceFilePath", length = 1024)
    private String sourceFilePath;

    /**
     * The version of this node.
     * <p>
     */
    @JoinColumn(name = "version_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Version version;


    @JoinTable(name = "node_smells", joinColumns = { 
        @JoinColumn(name = "node_id", referencedColumnName = "id") }, 
            inverseJoinColumns = { @JoinColumn(name = "smell_id", referencedColumnName = "id") })
    @ManyToMany
    private Collection<BadSmell> badSmells;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "node", orphanRemoval = true)
    private Collection<Method> methods;

    /**
     * Create an empty node.
     * <p>
     */
    public Node() {
        badSmells = new HashSet<>();
        methods = new HashSet<>();
    }

    /**
     * @return the id of this node
     */
    public Long getId() {
        return id;
    }

    /**
     * Get the name of this node.
     * <p>
     * If the node is a class file then this is the name of the class.
     *
     * @return the name of this node.
     */
    public String getSimpleName() {
        return simpleName;
    }

    /**
     * Set the name of this node.
     * <p>
     * If this node is a class file, then the name of the class.
     *
     * @param name of this node, must not be null
     */
    public void setSimpleName(String name) {
        this.simpleName = name;
    }

    /**
     * Get the qualified name of this node.
     * <p>
     * The qualified name is the unique name identifying the node within the
     * project. (See Java classes)
     *
     * @return the fully qualified name of this node
     */
    public String getName() {
        return name;
    }

    /**
     * Set the fully qualified name of this node.
     * <p>
     * The qualified name is the unique name identifying the node within the
     * project. (See Java classes)
     *
     * @param fullyQualifiedName the fully qualified name of this node
     */
    public void setName(String fullyQualifiedName) {
        this.name = fullyQualifiedName;
    }

    /**
     * Get the path within repository of this node.
     * <p>
     * The path identifies uniquely the resource within repository.
     *
     * @return the path within repository
     */
    public String getSourceFilePath() {
        return sourceFilePath;
    }

    /**
     * Set the path within repository.
     * <p>
     * The path identifies uniquely the resource within repository.
     *
     * @param sourceFilePath the path within repository of this node. Must not
     * be null.
     */
    public void setSourceFilePath(String sourceFilePath) {
        this.sourceFilePath = sourceFilePath;
    }

    /**
     * @return the versions of this node
     */
    public Version getVersion() {
        return version;
    }

    /**
     * Set the versions of this node.
     * <p>
     *
     * @param version of this node
     */
    public void setVersion(Version version) {
        this.version = version;
    }

    public void addBadSmell(BadSmell smell){
        ArgsCheck.notNull("smell", smell);
        badSmells.add(smell);
    }
    
    public boolean hasSmell(BadSmell smell){
        return badSmells.contains(smell);
    } 

    public Collection<BadSmell> getBadSmells() {
        return badSmells;
    }
    
    public void addMethod(Method m){
        methods.add(m);
        m.setNode(this);
    }

    public Collection<Method> getMethods() {
        return methods;
    }

    
    public static Map<String, Node> nodeListToTreeMap(List<Node> list) {
        Map<String, Node> map = new TreeMap<>();
        list.stream().forEach((n) -> {
            map.put(n.getName(), n);
        });
        return map;
    }

    @Override
    public String toString() {
        return super.toString() + " Node{" +
                "id=" + id +
                ", simpleName='" + simpleName + '\'' +
                '}';
    }
}
