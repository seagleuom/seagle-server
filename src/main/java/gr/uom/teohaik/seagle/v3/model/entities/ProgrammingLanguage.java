package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.util.ArgsCheck;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An entity specifying a programming language within system.
 * <p>
 * The identification of a programming language is a useful feature of of the
 * system, especially in computing different metrics.
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "programming_language")
public class ProgrammingLanguage extends Thing {

   private static final long serialVersionUID = 1L;

   /**
    * Language id.
    * <p>
    */
   @Id
   @GeneratedValue(strategy = GenerationType.SEQUENCE)
   @Column(name = "id")
   private Long id;

   /**
    * The string representation of the language.
    * <p>
    */
   @NotNull
   @Size(min = 1, max = 100)
   @Column(name = "name")
   private String name;

   /**
    * A list of projects using this language.
    * <p>
    */
   @JoinTable(name = "project_language", joinColumns = { @JoinColumn(name = "programming_language_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "project_id", referencedColumnName = "id") })
   @ManyToMany(fetch = FetchType.LAZY)
   private Collection<Project> projects;

   /**
    * The list of metrics that can be computed against this language.
    * <p>
    */
   @JoinTable(name = "language_applied", joinColumns = { @JoinColumn(name = "prog_lang_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "metric_id", referencedColumnName = "id") })
   @ManyToMany(fetch = FetchType.LAZY)
   private Collection<Metric> metrics;

   /**
    * Create an empty language.
    * <p>
    */
   public ProgrammingLanguage() {
   }

   /**
    * @return the language id
    */
   public Long getId() {
      return id;
   }


   /**
    * Get the string of this language.
    * <p>
    * 
    * @return the string of this language
    */
   public String getName() {
      return name;
   }

   /**
    * Set the string of this language.
    * <p>
    * 
    * @param name
    *           the string of this language. Not null, max of 100 chars.
    */
   public void setName(String name) {
      this.name = name;
   }

   /**
    * Get the projects using this language.
    * <p>
    * 
    * @return the projects using this language.
    */
   public Collection<Project> getProjects() {
      return projects;
   }

   /**
    * Set the projects using this language.
    * <p>
    * 
    * @param projects
    *           the projects using this language.
    */
   public void setProjects(Collection<Project> projects) {
      this.projects = projects;
   }

   /**
    * Add a project using this language.
    * <p>
    * 
    * @param project
    *           using this language. Must not be null.
    */
   public void addProject(Project project) {
      ArgsCheck.notNull("project", project);
      if (projects == null) {
         projects = new HashSet<>();
      }
      projects.add(project);
   }

   /**
    * Remove a project from the list.
    * <p>
    * 
    * @param project
    *           to be removed. Must not be null.
    */
   public void removeProject(Project project) {
      ArgsCheck.notNull("project", project);
      if (projects != null) {
         projects.remove(project);
      }
   }

   /**
    * @return the metrics applying to this language.
    */
   public Collection<Metric> getMetrics() {
      return metrics;
   }

   /**
    * Set the metrics applying to this language.
    * <p>
    * 
    * @param metrics
    *           applied to this language.
    */
   public void setMetrics(Collection<Metric> metrics) {
      this.metrics = metrics;
   }

   /**
    * Add a metric that apply to this language.
    * <p>
    * 
    * @param metric
    *           to add to this language.
    */
   public void addMetric(Metric metric) {
      ArgsCheck.notNull("metric", metric);
      if (metrics == null) {
         metrics = new HashSet<>();
      }
      metrics.add(metric);
   }

   /**
    * Remove a metric from the list of metrics.
    * <p>
    * 
    * @param metric
    *           to be removed
    */
   public void removeMetric(Metric metric) {
      ArgsCheck.notNull(" metric", metric);
      if (metrics != null) {
         metrics.remove(metric);
      }
   }

   @Override
   public String toString() {
      return "gr.uom.java.seagle.db.persistence.v2.ProgrammingLanguage[ id="
            + id + " ]";
   }

}
