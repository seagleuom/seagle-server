package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.util.ArgsCheck;
import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This is the project entity.
 * <p>
 * The project entity contains useful information about a system software that
 * is analyzed by seagle.
 * <p>
 * - An extended information about a project can be obtained calling
 * {@link #getProjectInfo()}
 * <p>
 * - In order to retrieve the programming languages a project uses use
 * {@link #getProgrammingLanguages()}
 * <p>
 * - To retrieve info about the owners of the project call {@link #getOwners()}
 * <p>
 * - To retrieve the metrics that are registered on the next calculation for a
 * project use {@link #getRegisteredMetrics()}
 * <p>
 * - To retrieve project event such as INSERT, UPDATE etc use
 * {@link #getTimeLines()}
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "project")
public class Project extends Thing {

    private static final long serialVersionUID = 1L;

    /**
     * The id of the project.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * The name of the project.
     * <p>
     * A unique identifier of the project. Max of 255 characters.
     */
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "name", unique = true)
    private String name;

    /**
     * A description for the given project.
     * <p>
     * Max of 65K characters.
     */
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;

    /**
     * A remote URL from where this project is cloned.
     * <p>
     */
    @Column(name = "remoteRepoPath", unique = true)
    private String remoteRepoPath;

    /**
     * An image path of this project.
     * <p>
     */
    @Lob
    @Size(max = 65535)
    @Column(name = "imagePath")
    private String imagePath;

    /**
     * The programming languages this project is written.
     * <p>
     */
    @ManyToMany(mappedBy = "projects", fetch = FetchType.LAZY)
    private Collection<ProgrammingLanguage> programmingLanguages;

    /**
     * The owners of this project.
     * <p>
     */
    @ManyToMany(mappedBy = "ownProjects", fetch = FetchType.LAZY)
    private Collection<Developer> owners;

    /**
     * The metrics that should be calculated on the next execution.
     * <p>
     */
    @ManyToMany(fetch = FetchType.LAZY)
    private Collection<Metric> registeredMetrics;

    /**
     * The versions of this project.
     * <p>
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project", orphanRemoval = true)
    private Collection<Version> versions;

    /**
     * The info of this project.
     * <p>
     */
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "project")
    private ProjectInfo projectInfo;

    /**
     * The actions performed on this project.
     * <p>
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project", orphanRemoval = true)
    private Set<ProjectTimeline> timeLines;

    /**
     * Calculated values of metrics related to this project.
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project", orphanRemoval = true)
    private Set<ProjectMetric> projectMetrics;

    /**
     * Create an empty project.
     * <p>
     */
    public Project() {
        super();
        versions = new HashSet<>();
        timeLines = new HashSet<>();
        projectMetrics = new HashSet<>();
    }

    /**
     * Create an empty project given its id.
     * <p>
     *
     * @param id of this project
     */
    public Project(Long id) {
        super();
        this.id = id;
        versions = new HashSet<>();
        timeLines = new HashSet<>();
        projectMetrics = new HashSet<>();
    }

    /**
     * Create a project given its id and its name.
     * <p>
     *
     * @param id of this project
     * @param name of this project
     */
    public Project(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @return the id of this project
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the name of this project
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of this project.
     * <p>
     * Max of 255 characters. Must not be null. Must be unique.
     *
     * @param name of this project
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description of this project
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of this project.
     * <p>
     *
     * @param description of this project
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the remote URL from where this project were cloned.
     */
    public String getRemoteRepoPath() {
        return remoteRepoPath;
    }

    /**
     * Set the remote path of this project.
     * <p>
     * The remote path is the URL from where this project were cloned.
     *
     * @param remoteRepoPath the remote URL of this project
     */
    public void setRemoteRepoPath(String remoteRepoPath) {
        this.remoteRepoPath = remoteRepoPath;
    }

    /**
     * @return the path of the image for this project
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * Set the image path for this project.
     *
     * @param imagePath the path of image for this project
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * @return the programming languages this project is written
     */
    public Collection<ProgrammingLanguage> getProgrammingLanguages() {
        return programmingLanguages;
    }

    /**
     * Set the programming languages this project is written.
     * <p>
     *
     * @param programmingLanguages of this project
     */
    public void setProgrammingLanguages(
            Collection<ProgrammingLanguage> programmingLanguages) {
        this.programmingLanguages = programmingLanguages;
    }

    /**
     * Add the given programming language.
     * <p>
     *
     * @param language the language to be added, must not be null
     */
    public void addProgrammingLanguage(ProgrammingLanguage language) {
        ArgsCheck.notNull("language", language);
        if (programmingLanguages == null) {
            programmingLanguages = new HashSet<>();
        }
        programmingLanguages.add(language);
    }

    /**
     * Remove the given programming language.
     * <p>
     *
     * @param language to be removed
     */
    public void removeProgrammingLanguage(ProgrammingLanguage language) {
        ArgsCheck.notNull("language", language);
        if (programmingLanguages != null) {
            programmingLanguages.remove(language);
        }
    }

    /**
     * @return the owners of this project
     */
    public Collection<Developer> getOwners() {
        return owners;
    }

    /**
     * Set the owners of this project.
     * <p>
     *
     * @param owners the owners of this project
     */
    public void setOwners(Collection<Developer> owners) {
        this.owners = owners;
    }

    /**
     * Add an owner to this project.
     * <p>
     *
     * @param owner to be added to this project. Must not be null
     */
    public void addOwner(Developer owner) {
        ArgsCheck.notNull("owner", owner);
        if (owners == null) {
            owners = new HashSet<>();
        }
        owners.add(owner);
    }

    /**
     * Remove the given owner from this project.
     * <p>
     *
     * @param owner the owner to be removed
     */
    public void removeOwner(Developer owner) {
        ArgsCheck.notNull("owner", owner);
        if (owners != null) {
            owners.remove(owner);
        }
    }

    /**
     * Get the registered metrics for this project. The metrics registered to be
     * computed next time when a calculation is required for this project.
     * <p>
     *
     * @return the registered metrics
     */
    public Collection<Metric> getRegisteredMetrics() {
        return registeredMetrics;
    }

    /**
     * Set the metrics to be calculated next time a calculation takes place.
     * <p>
     *
     * @param registeredMetrics the metrics to be calculated next time for this
     * project
     */
    public void setRegisteredMetrics(Collection<Metric> registeredMetrics) {
        this.registeredMetrics = registeredMetrics;
    }

    /**
     * Register this metric to be calculated next time a calculation takes
     * place.
     * <p>
     *
     * @param metric to be registered for the next time to be calculated. Must
     * not be null
     */
    public void registerMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        boolean add = false;
        if (registeredMetrics == null) {
            registeredMetrics = new HashSet<>();
            add = true;
        }
        if (!add) {
            // Check first if it there
            if (registeredMetrics.contains(metric)) {
                return;
            }
        }
        registeredMetrics.add(metric);
    }

    public boolean isRegisteredMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (registeredMetrics == null || registeredMetrics.isEmpty()) {
            return false;
        }
        return registeredMetrics.contains(metric);
    }

    /**
     * Remove the metric from the list of those to be calculated next time.
     * <p>
     *
     * @param metric to be removed, must not be null
     */
    public void deregisterMetric(Metric metric) {
        ArgsCheck.notNull("metric", metric);
        if (registeredMetrics != null) {
            registeredMetrics.remove(metric);
        }
    }

    /**
     * @return the versions of this project
     */
    public Collection<Version> getVersions() {
        return versions;
    }

    /**
     * Set the versions of this project.
     * <p>
     *
     * @param versions of this project
     */
    public void setVersions(Collection<Version> versions) {
        this.versions = versions;
    }

    /**
     * Add the specified version to this project.
     * <p>
     *
     * @param version to be added, must not be null
     */
    public void addVersion(Version version) {
        ArgsCheck.notNull("version", version);
        if (versions == null) {
            versions = new HashSet<>();
        }
        versions.add(version);
        version.setProject(this);
        
    }

    /**
     * Remove the specified version from this project.
     * <p>
     *
     * @param version to be removed
     */
    public void removeVersion(Version version) {
        ArgsCheck.notNull("version", version);
        if (versions != null) {
            versions.remove(version);
        }
    }

    /**
     * @return project info
     */
    // @XmlTransient
    public ProjectInfo getProjectInfo() {
        return projectInfo;
    }

    /**
     * Set the project info.
     * <p>
     *
     * @param projectInfo
     */
    public void setProjectInfo(ProjectInfo projectInfo) {
        this.projectInfo = projectInfo;
    }

    /**
     * Get the timelines for this project.
     * <p>
     * Timeline is a past action that has been performed on this project.
     *
     * @return the project actions
     */
    public Collection<ProjectTimeline> getTimeLines() {
        return timeLines;
    }

    /**
     * Set the timelines for this project.
     * <p>
     * Timeline is a past action that has been performed on this project.
     *
     * @param timeLines for this project
     */
    public void setTimeLines(Set<ProjectTimeline> timeLines) {
        this.timeLines = timeLines;
    }

    /**
     * Add a timeline event to this project.
     * <p>
     *
     * @param timeline to be added.
     */
    public void addTimeLine(ProjectTimeline timeline) {
        ArgsCheck.notNull("timeline", timeline);
        if (timeLines == null) {
            timeLines = new HashSet<>();
        }
        timeLines.add(timeline);
        timeline.setProject(this);
    }

    /**
     * Remove the timeline from this project.
     * <p>
     *
     * @param timeline to be removed
     */
    public void removeTimeLine(ProjectTimeline timeline) {
        ArgsCheck.notNull("timeline", timeline);
        if (timeLines != null) {
            timeLines.remove(timeline);
        }
    }

    /**
     * Get the metrics that relates to this project.
     * <p>
     * Note these metrics are not version specific.
     *
     * @return project metrics
     */
    public Collection<ProjectMetric> getProjectMetrics() {
        return projectMetrics;
    }

    /**
     * Set the metrics that realates to this project.
     * <p>
     * Note these metrics are not version specific.
     *
     * @param projectMetrics metrics
     */
    public void setProjectMetrics(Set<ProjectMetric> projectMetrics) {
        this.projectMetrics = projectMetrics;
    }

    /**
     * Add a computed metric to this project.
     * <p>
     * Note these metrics are not version specific.
     *
     * @param metric to be added to project
     */
    public void addProjectMetric(ProjectMetric metric) {
        ArgsCheck.notNull("metric", metric);
        if (this.projectMetrics == null) {
            projectMetrics = new HashSet<>();
        }
        projectMetrics.add(metric);
    }

    public ProjectTimeline getDateAnalyzed() {
        ProjectTimeline timeline = null;
        for (ProjectTimeline tl : timeLines) {
            if (tl.getActionType().equals(ActionType.ANALYSED)) {
                timeline = tl;
            }
        }
        return timeline;
    }

    public ProjectTimeline getDateInserted() {
        ProjectTimeline timeline = null;
        for (ProjectTimeline tl : timeLines) {
            if (tl.getActionType().equals(ActionType.CLONED)) {
                timeline = tl;
            }
        }
        return timeline;
    }

    public ProjectTimeline getDateUpdated() {
        ProjectTimeline timeline = null;
        for (ProjectTimeline tl : timeLines) {
            if (tl.getActionType().equals(ActionType.UPDATED)) {
                timeline = tl;
            }
        }
        return timeline;
    }

    @Override
    public String toString() {
        return "Project{" + "id=" + id + ", name=" + name + ", remoteRepoPath=" + remoteRepoPath + ", versions=" + versions.size() + '}';
    }
}
