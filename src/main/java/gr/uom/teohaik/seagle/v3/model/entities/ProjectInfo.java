package gr.uom.teohaik.seagle.v3.model.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 * Project's info entity.
 * <p>
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "project_info")
public class ProjectInfo extends Thing {

    private static final long serialVersionUID = 1L;

    /**
     * The id of this entity.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * The date the project was created.
     * <p>
     */
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;

    /**
     * The date the project was last updated.
     * <p>
     */
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    /**
     * The date the project was inserted into system.
     * <p>
     */
    @Column(name = "date_inserted")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;

    /**
     * The last commit id.
     * <p>
     */
    @Size(max = 1024)
    @Column(name = "lastCommitID")
    private String lastCommitID;

    /**
     * The project this info is about.
     * <p>
     */
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Project project;

    /**
     * Create an empty project info.
     * <p>
     */
    public ProjectInfo() {
        super();
    }


    /**
     * @return the id of the project
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the date when this project was created
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * Set the date when this project was created.
     * <p>
     * @param dateCreated the date when the project was created
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the date when this project was last updated
     */
    public Date getLastUpdate() {
        return lastUpdate;
    }

    /**
     * Set the date this project was last updated.
     * <p>
     * @param lastUpdate the date this project was last updated
     */
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * @return the date when this project was inserted into the system
     */
    public Date getDateInserted() {
        return dateInserted;
    }

    /**
     * Set the date when this project was inserted into the system.
     * <p>
     * @param dateInserted the date when this project were inserted into the
     * system.
     */
    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    /**
     * @return the last commit id
     */
    public String getLastCommitID() {
        return lastCommitID;
    }

    /**
     * Set the last commit id of this project.
     * <p>
     * @param lastCommitID the last commit id of this project
     */
    public void setLastCommitID(String lastCommitID) {
        this.lastCommitID = lastCommitID;
    }

    /**
     * @return the project this info is about
     */
    public Project getProject() {
        return project;
    }

    /**
     * Set the project this info is about.
     * <p>
     * @param project this info is about
     */
    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.db.persistence.v2.ProjectInfo[ projectId=" + id + " ]";
    }

}
