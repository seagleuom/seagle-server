package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.util.ArgsCheck;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * An entity that links a metric to a project.
 * <p>
 * This is useful when we have metrics that are not related to versions but to a project in general.
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "project_metric")
public class ProjectMetric extends Thing {

    private static final long serialVersionUID = 1333924927893533504L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    /**
     * The project entity.
     * <p>
     */
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Project project;

    /**
     * The metric value.
     * <p>
     */
    @OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
    private MetricValue metricValue;

    /**
     *
     */
    public ProjectMetric() {
        super();
    }

    public ProjectMetric(Project project, MetricValue value) {
        super();
        ArgsCheck.notNull("project", project);
        ArgsCheck.notNull("value", value);
        this.project = project;
        this.metricValue = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public MetricValue getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(MetricValue metricValue) {
        this.metricValue = metricValue;
    }

    @Override
    public String toString() {
        return "ProjectMetric{" +
                "id=" + id +
                ", project=" + project +
                '}';
    }
}
