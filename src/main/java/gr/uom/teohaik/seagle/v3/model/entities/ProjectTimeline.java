package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The project action info entity.
 * <p>
 * When an event is registered an action is performed. If this action is related
 * to a project an entity is created of this type to keep track of the actions
 * within system.
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "project_timeline")
public class ProjectTimeline extends Thing {

    private static final long serialVersionUID = 1L;

    /**
     * The id of this entity.
     * <p>
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    private Date dateCreated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", nullable = false)
    private Date dateUpdated;

    /**
     * The project on which the action was performed.
     * <p>
     */
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Project project;

    /**
     * The type of this action.
     * <p>
     */
    @Enumerated(EnumType.STRING)
    @Column(length = 30)
    private ActionType actionType;

    /**
     * Create an empty instance.
     * <p>
     */
    public ProjectTimeline() {
        super();
    }
    
    
    @PrePersist
    protected void onCreate() {
        dateUpdated = dateCreated = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        dateUpdated = new Date();
    }


    /**
     * @return the id of this project
     */
    public Long getId() {
        return id;
    }

    /**
     * @return the project this action was performed on.
     */
    public Project getProject() {
        return project;
    }

    /**
     * Set the project this action was performed on.
     * <p>
     *
     * @param project of this action
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the action type
     */
    public ActionType getActionType() {
        return actionType;
    }

    /**
     * Set the action type.
     * <p>
     *
     * @param actionType type of this action
     */
    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }


    @Override
    public String toString() {
        return "ProjectTimeline[ id=" + id
                + "actionType = " +actionType.name()
                + " ]";
    }

}
