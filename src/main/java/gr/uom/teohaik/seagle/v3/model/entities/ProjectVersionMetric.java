package gr.uom.teohaik.seagle.v3.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
/**
 * Entity linking a project with a version and a calculated metric.
 * <p>
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "project_version_metric")
public class ProjectVersionMetric extends Thing {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Version version;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private MetricValue metricValue;

    public ProjectVersionMetric() {
        super();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public MetricValue getMetricValue() {
        return metricValue;
    }

    public void setMetricValue(MetricValue metricValue) {
        this.metricValue = metricValue;
    }

    @Override
    public String toString() {
        return "ProjectVersionMetric{" + "id=" + id + ", version=" + version.getName() + ", Value=" + metricValue.getValue() + '}';
    }

}
