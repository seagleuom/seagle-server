package gr.uom.teohaik.seagle.v3.model.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Theodore Chaikalis
 * 
 */
@Entity
@Table(name = "property")
public class Property extends Thing {

   /**
    * 
    */
   private static final long serialVersionUID = 2694820885006935359L;

   @Id
   @Column(name = "id", length = 64)
   String id;

   @Basic(optional = false)
   private String domain;
   
   @Basic(optional = false)
   @Column(length = 4096)
   private String name;
   
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 4096)
   @Column(name = "value")
   private String value;
   

   /**
    *
    */
   public Property() {
      super();
   }
   
   /**
    * @return the id
    */
   public String getId() {
      return id;
   }
   
   /**
    * Do not set this, it will be generated.
    * @param id the id to set
    */
   public void setId(String id) {
      this.id = id;
   }
   

   public String getDomain() {
      return domain;
   }


   public void setDomain(String domain) {
      this.domain = domain;
   }


   public String getName() {
      return name;
   }


   public void setName(String name) {
      this.name = name;
   }


   public String getValue() {
      return value;
   }


   public void setValue(String value) {
      this.value = value;
   }

   @Override
   public String toString() {
      return "Property [domain=" + domain + ", name=" + name + "]";
   }
}
