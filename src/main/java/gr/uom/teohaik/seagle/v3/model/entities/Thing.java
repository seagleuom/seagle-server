package gr.uom.teohaik.seagle.v3.model.entities;

import gr.uom.teohaik.seagle.v3.model.enums.StatusEnum;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PreUpdate;

@MappedSuperclass
public abstract class Thing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "uuid", updatable = false, columnDefinition = "VARCHAR(40)")
    private String uuid;

    @Column(name = "CREATED_BY", length = 30)
    private String createdBy;

    @Column(name = "CREATION_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDate;

    @Column(name = "LAST_UPDATED_DATE", columnDefinition = "TIMESTAMP")
    private LocalDateTime lastUpdatedDate;

    @Column(name = "STATUS", length = 1)
    private String status;

    @PreUpdate
    protected void onUpdate() {
        lastUpdatedDate = LocalDateTime.now();
    }

    public Thing() {
        status = StatusEnum.ACTIVE.getId();
        creationDate = LocalDateTime.now();
        setUuid(UUID.randomUUID().toString());
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastUpdatedDate() {
        return this.lastUpdatedDate;
    }

    public void setLastUpdatedDate(LocalDateTime lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        if (this == o){
            return true;
        }
        if (this.getClass() != o.getClass()){
            return false;
        }
        Thing other = (Thing) o;
        return uuid.equals(other.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return "ThingT{" +
                "uuid=" + uuid +
                ", createdBy='" + createdBy + '\'' +
                ", creationDate=" + creationDate +
                ", lastUpdatedDate=" + lastUpdatedDate +
                ", status='" + status + '\'' +
                '}';
    }
}
