package gr.uom.teohaik.seagle.v3.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gr.uom.teohaik.seagle.util.ArgsCheck;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Theodore Chaikalis
 */
@Entity
@Table(name = "version")
public class Version extends Thing implements Comparable<Version> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "name")
    private String name;

    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "commitID")
    private String commitID;

    @Column(name = "analyzed")
    private Boolean analyzed;

    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private Project project;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "version", orphanRemoval = true)
    private Collection<ProjectVersionMetric> projectVersionMetrics;

    @ManyToMany(mappedBy = "authorVersions", fetch = FetchType.LAZY)
    private Collection<Developer> authors;

    @ManyToMany(mappedBy = "committerVersions", fetch = FetchType.LAZY)
    private Collection<Developer> committers;

    public Version() {
        super();
        committers = new HashSet<>();
        authors = new HashSet<>();
        projectVersionMetrics = new HashSet<>();
    }

    public Version(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCommitID() {
        return commitID;
    }

    public void setCommitID(String commitID) {
        this.commitID = commitID;
    }

    public Collection<Developer> getAuthors() {
        return authors;
    }

    public void setAuthors(Collection<Developer> authors) {
        this.authors = authors;
    }

    public Collection<Developer> getCommitters() {
        return committers;
    }

    public void setCommitters(Collection<Developer> committers) {
        this.committers = committers;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Collection<ProjectVersionMetric> getProjectVersionMetrics() {
        return projectVersionMetrics;
    }

    public void setProjectVersionMetrics(
            Collection<ProjectVersionMetric> projectVersionMetrics) {
        this.projectVersionMetrics = projectVersionMetrics;
    }

    public Boolean getAnalyzed() {
        return analyzed;
    }

    public void setAnalyzed(Boolean analyzed) {
        this.analyzed = analyzed;
    }

    public void addProjectVersionMetric(ProjectVersionMetric metric) {
        ArgsCheck.notNull("version metric", metric);
        if (this.projectVersionMetrics == null) {
            projectVersionMetrics = new HashSet<>();
        }
        projectVersionMetrics.add(metric);
    }

    @Override
    public String toString() {
        return "Version[" + id + "] ,["+name+ "]";
    }

    @Override
    public int compareTo(Version other) {
        return this.id.compareTo(other.id);
    }

}
