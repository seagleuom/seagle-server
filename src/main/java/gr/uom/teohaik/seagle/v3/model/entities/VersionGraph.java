package gr.uom.teohaik.seagle.v3.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Thodoris
 * 23/8/2017
 
*/

@Entity
@Table(name = "version_graph")
public class VersionGraph extends Thing {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Version version;
    
    @Column(name="jsonGraph")
    @Lob
    private String jsonGraph;
    
    public VersionGraph(Version ver, String graph){
        super();
        version = ver;
        jsonGraph = graph;
    }
    
    public VersionGraph(){
        super();
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public String getJsonGraph() {
        return jsonGraph;
    }

    public void setJsonGraph(String jsonGraph) {
        this.jsonGraph = jsonGraph;
    }

    @Override
    public String toString() {
        return "VersionGraph{" +
                "id=" + id +
                '}';
    }
}
