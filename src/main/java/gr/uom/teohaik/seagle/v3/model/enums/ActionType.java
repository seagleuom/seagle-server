package gr.uom.teohaik.seagle.v3.model.enums;

public enum ActionType {

    CLONED, ANALYSED, DELETED, UPDATED

}
