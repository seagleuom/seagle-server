package gr.uom.teohaik.seagle.v3.model.enums;


public enum StatusEnum {

	ACTIVE("1", "Active"),
	DELETED("0", "Deleted"),
	DRAFT("2", "Draft"),

	;

	StatusEnum(String id, String description) {
		this.id = id;
		this.description = description;
	}

	private String id;
	private String description;

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public static StatusEnum get(String id) {
		for (StatusEnum et : values()) {
			if (et.id.equals(id)) {
				return et;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return description;
	}
}
