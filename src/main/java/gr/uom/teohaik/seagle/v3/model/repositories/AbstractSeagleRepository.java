package gr.uom.teohaik.seagle.v3.model.repositories;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;

public abstract class AbstractSeagleRepository<E, PK> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected EntityManager em;

    public EntityManager getEntityManager(){
        return em;
    }

    private Class<E> getEntityClass() {
        Type type = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        return (Class<E>) type;
    }

    /**
     * Return the id of the entity. Returns null if the entity does not yet have an id.
     *
     * @param entity Sample entity.
     * @return id of the entity
     */
    public PK getPrimaryKey(E entity) {
        Object o = getEntityManager().getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
        return o == null ? null : (PK) o;
    }

    /**
     * Convenience access to {@link javax.persistence.EntityManager#merge(Object)}.
     *
     * @param entity Entity to merge.
     */
    public E merge(E entity) {
        return getEntityManager().merge(entity);
    }

    /**
     * Convenience access to {@link javax.persistence.EntityManager#persist(Object)}.
     *
     * @param entity Entity to persist.
     */
    public void persist(E entity) {
        getEntityManager().persist(entity);
    }

    /**
     * Convenience access to {@link javax.persistence.EntityManager#remove(Object)}.
     *
     * @param entity Entity to remove.
     */
    public void remove(E entity) {
        getEntityManager().remove(entity);
    }

    /**
     * Entity lookup by primary key. Convenicence method around {@link javax.persistence.EntityManager#find(Class, Object)}.
     *
     * @param primaryKey DB primary key.
     * @return Entity identified by primary or null if it does not exist.
     */
    public E findBy(PK primaryKey) {
        return getEntityManager().find(getEntityClass(), primaryKey);
    }

    /**
     * Lookup all existing entities of entity class {@code <E>}.
     *
     * @return List of entities, empty if none found.
     */
    public List<E> findAll() {
        Class<E> entityClass = getEntityClass();
        return getEntityManager().createQuery("select t from " + entityClass.getSimpleName() + " t", entityClass)
                 .getResultList();
    }

    /**
     * Persist (new entity) or merge the given entity. The distinction on calling either method is done based on the primary key field being null or not.
     *
     * @param entity Entity to save.
     * @return Returns the modified entity.
     */
    public E save(E entity) {
        PK id = getPrimaryKey(entity);
        if (id == null) {
            getEntityManager().persist(entity);
            return entity;
        } else {
            return getEntityManager().merge(entity);
        }
    }

    /**
     * See {@link javax.persistence.EntityManager#find(Class, Object, java.util.Map)}.
     */
    public E find(Object primaryKey, Map<String, Object> properties) {
        return getEntityManager().find(getEntityClass(), primaryKey, properties);
    }

    /**
     * See {@link javax.persistence.EntityManager#find(Class, Object, LockModeType)}.
     */
    public E find(Object primaryKey, LockModeType lockMode) {
        return getEntityManager().find(getEntityClass(), primaryKey, lockMode);
    }

    /**
     * See {@link javax.persistence.EntityManager#find(Class, Object, LockModeType, Map)}.
     */
    public E find(Object primaryKey, LockModeType lockMode, Map<String, Object> properties) {
        return getEntityManager().find(getEntityClass(), primaryKey, lockMode, properties);
    }

    /**
     * See {@link javax.persistence.EntityManager#getReference(Class, Object)}.
     */
    public E getReference(Object primaryKey) {
        return getEntityManager().getReference(getEntityClass(), primaryKey);
    }

    /**
     * See {@link javax.persistence.EntityManager#setFlushMode(FlushModeType)}.
     */
    public void setFlushMode(FlushModeType flushMode) {
        getEntityManager().setFlushMode(flushMode);
    }

    /**
     * See {@link javax.persistence.EntityManager#getFlushMode()}.
     */
    public FlushModeType getFlushMode() {
        return getEntityManager().getFlushMode();
    }

    /**
     * See {@link javax.persistence.EntityManager#lock(Object, LockModeType)}.
     */
    public void lock(Object entity, LockModeType lockMode) {
        getEntityManager().lock(entity, lockMode);
    }

    /**
     * See {@link javax.persistence.EntityManager#lock(Object, LockModeType, Map)}.
     */
    public void lock(Object entity, LockModeType lockMode, Map<String, Object> properties) {
        getEntityManager().lock(entity, lockMode, properties);
    }

    /**
     * See {@link javax.persistence.EntityManager#refresh(Object, Map)}.
     */
    public void refresh(E entity, Map<String, Object> properties) {
        getEntityManager().refresh(entity, properties);
    }

    /**
     * See {@link javax.persistence.EntityManager#refresh(Object, LockModeType)}.
     */
    public void refresh(E entity, LockModeType lockMode) {
        getEntityManager().refresh(entity, lockMode);
    }

    /**
     * See {@link javax.persistence.EntityManager#refresh(Object, LockModeType, Map)}.
     */
    public void refresh(E entity, LockModeType lockMode, Map<String, Object> properties) {
        getEntityManager().refresh(entity, lockMode, properties);
    }

    /**
     * See {@link javax.persistence.EntityManager#clear()}.
     */
    public void clear() {
        getEntityManager().clear();
    }

    /**
     * See {@link javax.persistence.EntityManager#detach(Object)}.
     */
    public void detach(E entity) {
        getEntityManager().detach(entity);
    }

    /**
     * See {@link javax.persistence.EntityManager#contains(Object)}.
     */
    public boolean contains(E entity) {
        return getEntityManager().contains(entity);
    }

    /**
     * See {@link javax.persistence.EntityManager#getLockMode(Object)}.
     */
    public LockModeType getLockMode(E entity) {
        return getEntityManager().getLockMode(entity);
    }

    /**
     * See {@link javax.persistence.EntityManager#setProperty(String, Object)}.
     */
    public void setProperty(String propertyName, Object value) {
        getEntityManager().setProperty(propertyName, value);
    }

    /**
     * See {@link javax.persistence.EntityManager#getProperties()}.
     */
    public Map<String, Object> getProperties() {
        return getEntityManager().getProperties();
    }

    /**
     * See {@link javax.persistence.EntityManager#joinTransaction()}.
     */
    public void joinTransaction() {
        getEntityManager().joinTransaction();
    }

    /**
     * See {@link javax.persistence.EntityManager#unwrap(Class)}.
     */
    public <T> T unwrap(Class<T> cls) {
        return getEntityManager().unwrap(cls);
    }

    /**
     * See {@link javax.persistence.EntityManager#getDelegate()}.
     */
    public Object getDelegate() {
        return getEntityManager().getDelegate();
    }

    /**
     * See {@link javax.persistence.EntityManager#close()}.
     */
    public void close() {
        getEntityManager().close();
    }

    /**
     * See {@link javax.persistence.EntityManager#isOpen()}.
     */
    public boolean isOpen() {
        return getEntityManager().isOpen();
    }

    /**
     * See {@link javax.persistence.EntityManager#getTransaction()}.
     */
    public EntityTransaction getTransaction() {
        return getEntityManager().getTransaction();
    }

    /**
     * See {@link javax.persistence.EntityManager#getEntityManagerFactory()}.
     */
    public EntityManagerFactory getEntityManagerFactory() {
        return getEntityManager().getEntityManagerFactory();
    }

    /**
     * See {@link javax.persistence.EntityManager#getCriteriaBuilder()}.
     */
    public CriteriaBuilder getCriteriaBuilder() {
        return getEntityManager().getCriteriaBuilder();
    }

    /**
     * See {@link javax.persistence.EntityManager#getMetamodel()}.
     */
    public Metamodel getMetamodel() {
        return getEntityManager().getMetamodel();
    }

    /**
     * {@link #save(Object)} the given entity and flushes the persistence context afterwards.
     *
     * @param entity Entity to save.
     * @return Returns the modified entity.
     */
    public E saveAndFlush(E entity) {
        E returnedEntity = save(entity);
        getEntityManager().flush();
        return returnedEntity;
    }

    /**
     * {@link #save(Object)}s the given entity and flushes the persistence context afterwards, followed by a refresh (e.g. to load DB trigger modifications).
     *
     * @param entity Entity to save.
     * @return Returns the modified entity.
     */
    public E saveAndFlushAndRefresh(E entity) {
        E returnedEntity = save(entity);
        getEntityManager().flush();
        getEntityManager().refresh(returnedEntity);
        return returnedEntity;
    }

    /**
     * Convenience access to {@link javax.persistence.EntityManager#remove(Object)} with a following flush.
     *
     * @param entity Entity to remove.
     */
    public void removeAndFlush(E entity) {
        getEntityManager().remove(entity);
        getEntityManager().flush();
    }

    /**
     * Convenience access to {@link javax.persistence.EntityManager#refresh(Object)}.
     *
     * @param entity Entity to refresh.
     */
    public void refresh(E entity) {
        getEntityManager().refresh(entity);
    }

    /**
     * Convenience access to {@link javax.persistence.EntityManager#flush()}.
     */
    public void flush() {
        getEntityManager().flush();
    }

    /**
     * Count all existing entities of entity class {@code <E>}.
     *
     * @return Counter.
     */
    public Long count() {
        Class<E> entityClass = getEntityClass();
        return getEntityManager().createQuery("select count(*) from " + entityClass.getSimpleName() + " t", Long.class)
                 .getSingleResult();
    }

    /**
     * Lookup a range of existing entities of entity class {@code <E>} with support for pagination.
     *
     * @param start The starting position.
     * @param max   The maximum number of results to return
     * @return List of entities, empty if none found.
     */
    public List<E> findAll(int start, int max) {
        Class<E> entityClass = getEntityClass();
        return getEntityManager().createQuery("select t from " + entityClass.getSimpleName() + " t", entityClass)
                 .setMaxResults(max)
                 .setFirstResult(start)
                 .getResultList();
    }


    /**
     * Convenience access to {@link javax.persistence.EntityManager#remove(Object)}
     * with an detached entity.
     * @param entity Entity to remove.
     */
    public void attachAndRemove(E entity) {
        if (!this.getEntityManager().contains(entity)) {
            entity = this.getEntityManager().merge(entity);
        }

        this.remove(entity);
    }
}
