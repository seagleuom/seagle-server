package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.Method;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public abstract class MethodRepository implements EntityRepository<Method, String>, EntityManagerDelegate<Method> {

    public void saveMethods(List<Method> methodsToCreate){
        methodsToCreate.forEach(method -> this.save(method));
    }
    
    
}
