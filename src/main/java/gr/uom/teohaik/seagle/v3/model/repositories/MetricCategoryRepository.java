package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.MetricCategory;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public interface MetricCategoryRepository extends EntityRepository<MetricCategory, String>, EntityManagerDelegate<MetricCategory> {

    public abstract Optional<MetricCategory> findOptionalByCategoryName(String categoryName);

}
