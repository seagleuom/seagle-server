package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Metric;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@ApplicationScoped
public class MetricRepository extends AbstractSeagleRepository<Metric, Long> {

    @Inject
    @SeagleLogger
    private Logger logger;

    @Inject
    private EntityManager entityManager;

    private Map<String, Long> metricCache;

    @PostConstruct
    public void init(){
        metricCache = new HashMap<>();
    }

    public Long findMetricIdByMnemonicCached(String mnemonic){
        logger.debug("Finding metric with mnemonic = [{}]", mnemonic);
        Long metricId = metricCache.get(mnemonic);
        if(metricId == null) {
            logger.debug("Metric cache miss for [{}], should run query", mnemonic);
            try {
                Metric metric = entityManager.createQuery("select  m from Metric m where m.mnemonic = ?1", Metric.class)
                                  .setParameter(1, mnemonic)
                                  .getSingleResult();
                metricId = metric.getId();

                logger.debug("Metric {} fetched. id = [{}]", mnemonic, metricId);
                metricCache.put(mnemonic, metricId);
            }catch (NoResultException nre){
                logger.warn("metric with mnemonic {} does not exist!", mnemonic);
                return null;
            }
        }
        return metricId;
    }

    public Optional<Metric> findOptionalByMnemonic(String mnemonic){
        return entityManager.createQuery("select  m from Metric m where m.mnemonic = ?1", Metric.class)
          .setParameter(1, mnemonic)
          .getResultList().stream().findFirst();
    }

    public List<Metric> findByCategory(Long categoryId){
        return entityManager.createQuery("select  m from Metric m where m.category.id = ?1", Metric.class)
          .setParameter(1, categoryId)
          .getResultList();
    }

    public List<Metric> findByCategory_categoryName(String categoryName){
        return entityManager.createQuery("select  m from Metric m where m.category.categoryName = ?1", Metric.class)
                 .setParameter(1, categoryName)
                 .getResultList();
    }

    public Metric findMetricById(Long metricId) {
        return entityManager.createQuery("select m from Metric m where m.id = ?1", Metric.class)
                 .setParameter(1, metricId).getSingleResult();
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }
}
