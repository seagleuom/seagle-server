package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.MetricValue;
import gr.uom.teohaik.seagle.v3.ws.rest.model.MetricValueDTO;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public abstract class MetricValueRepository implements EntityRepository<MetricValue, String>, EntityManagerDelegate<MetricValue> {

    @Inject
    private EntityManager em;

    public List<MetricValue> findByMetricIdAndVersionId(Long metricId, Long versionId) {
        return em.createQuery("select mv from MetricValue mv "
                        + " where mv.metricId = ?1 "
                        + "and mv.versionId = ?2"
                , MetricValue.class)
                 .setParameter(1, metricId)
                 .setParameter(2, versionId)
                 .getResultList();
    }

    public List<MetricValueDTO> findByMetricValueDTOsByMetricIdAndVersionId(Long metricId, Long versionId) {
        return em.createQuery("select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                        + "MetricValueDTO(m.mnemonic, mv.value, node.name) "
                        + " from MetricValue mv "
                        + " join Node node on node.id = mv.nodeId"
                        + " join Metric m on m.id = mv.metricId"
                        + " where mv.metricId = ?1 "
                        + " and mv.versionId = ?2",
                MetricValueDTO.class)
                 .setParameter(1, metricId)
                 .setParameter(2, versionId)
                 .getResultList();
    }


    public List<MetricValueDTO> findByMetricValueDTOsByNodeIdAndVersionId(Long nodeId, Long metricId, Long versionId) {
        return em.createQuery("select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                        + "MetricValueDTO(metric.mnemonic, mv.value, node.name) "
                        + " from MetricValue mv "
                        + " join Node node on node.id = mv.nodeId"
                        + " join Metric metric on metric.id = mv.metricId"
                        + " where mv.nodeId = :nodeIdParam  "
                        + " and ((:metricIdParam is null) or ( :metricIdParam is not null and metric.id = :metricIdParam)) "
                        + " and mv.versionId = :versionIdParam "
                        + "  ",
                MetricValueDTO.class)
                 .setParameter("nodeIdParam", nodeId)
                 .setParameter("metricIdParam", metricId)
                 .setParameter("versionIdParam", versionId)
                 .getResultList();
    }
}
