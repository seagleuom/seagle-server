package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Node;
import gr.uom.teohaik.seagle.v3.ws.rest.model.NodeDTO;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public abstract class NodeRepository implements EntityRepository<Node, String>, EntityManagerDelegate<Node> {
    
    @Inject
    EntityManager em;

    @Inject
    @SeagleLogger
    private Logger logger;
    
    public abstract List<Node> findByVersion_id(Long versionId);
    
    public List<NodeDTO> findNodesByVersion(Long versionId){
        return em.createQuery("select "
                + " new gr.uom.teohaik.seagle.v3.ws.rest.model."
                + " NodeDTO(n.name, n.simpleName) from Node n"
                + " where n.version.id = ?1",
                NodeDTO.class)
                .setParameter(1, versionId)
                .getResultList();
    }

    public NodeDTO findNodesByClassNameAndVersion(String className, Long versionId){
        try {
            return em.createQuery("select "
                            + " new gr.uom.teohaik.seagle.v3.ws.rest.model."
                            + " NodeDTO(n.id, version.id, n.name, n.simpleName) "
                            + " from Node n "
                            + " join n.version version "
                            + " where n.name = ?1 and "
                            + " version.id = ?2",
                    NodeDTO.class)
                     .setParameter(1, className)
                     .setParameter(2, versionId)
                     .getSingleResult();
        }catch (NoResultException nre) {
            logger.warn("Cannot find node with class name = [{}] and version id = [{}]",className, versionId);
            return null;
        }
    }
}
