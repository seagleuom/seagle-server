package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectMetric;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public abstract class ProjectMetricRepository implements EntityRepository<ProjectMetric, String>, 
        EntityManagerDelegate<ProjectMetric> {

 
    public abstract List<ProjectMetric> findByVersion(Version version);
    
    public abstract List<ProjectMetric> findByProject(Project project);
    
    public abstract List<ProjectMetric> findByVersion_id(Long versionID);
    
    public abstract List<ProjectMetric> findByProject_id(Long projectID);
}
