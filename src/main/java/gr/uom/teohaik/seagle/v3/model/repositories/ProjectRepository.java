package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;

/**
 * @author Theodore Chaikalis
 */
@ApplicationScoped
public class ProjectRepository extends AbstractSeagleRepository<Project, Long>  {

    @Inject
    private EntityManager entityManager;

    @Inject
    @SeagleLogger
    private Logger logger;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Project findByNameExact(String name) {
        try {
            return entityManager.createQuery("select p from Project p where p.name like ?1 ",
                    Project.class)
                     .setParameter(1, name)
                     .getSingleResult();
        } catch (NoResultException nre) {
            logger.warn("Cannot find project with name [{}]",name);
            return null;
        }
    }

    public List<Project> findByNameStartingLike(String name) {
        try {
            return entityManager.createQuery("select p from Project p where p.name like CONCAT(:projectName, '%') ",
                    Project.class)
                     .setParameter("projectName", name)
                     .getResultList();
        } catch (NoResultException nre) {
            logger.warn("Cannot find project starting with name [{}]", name);
            return null;
        }
    }


    public List<Project> findByNameEndingLike(String name) {
        try {
            return entityManager.createQuery("select p from Project p where p.name like CONCAT('%', :projectName) ",
                    Project.class)
                     .setParameter("projectName", name)
                     .getResultList();
        } catch (NoResultException nre) {
            logger.warn("Cannot find project ending with name [{}]",name);
            return null;
        }
    }


    public List<Project> findByNameThanContainsLike(String name) {
        try {
            return entityManager.createQuery("select p from Project p where p.name like CONCAT('%', :projectName, '%') ",
                    Project.class)
                     .setParameter("projectName", name)
                     .getResultList();
        } catch (NoResultException nre) {
            logger.warn("Cannot find project containg name [{}]",name);
            return null;
        }
    }


    public Optional<Project> findByRemoteRepoPath(String remoteRepoPath) {
        return entityManager.createQuery("select p from Project p "
                + "left join fetch p.versions versions where p.remoteRepoPath = ?1", Project.class)
                 .setParameter(1, remoteRepoPath)
                 .getResultList().stream().findFirst();
    }

    public ProjectDTO findDTOByRemoteRepoPath(String remoteRepoPath) {
        try {
            return entityManager.createQuery("Select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                    + "ProjectDTO(p.id, "
                    + "p.name, "
                    + "p.remoteRepoPath,  "
                    + "p.createdBy, "
                    + "p.creationDate,"
                    + "timeline.dateCreated, "
                    + "count(version.project.id)"
                    + ") from Project p "
                    + " join p.timeLines timeline "
                    + " join p.versions version "
                    + " where timeline.actionType = :actionType "
                    + " and p.remoteRepoPath = :remoteRepoParam "
                    + " group by p.id, timeline.dateCreated", ProjectDTO.class)
                     .setParameter("actionType", ActionType.ANALYSED)
                     .setParameter("remoteRepoParam", remoteRepoPath)
                     .getSingleResult();
        }catch (NoResultException nre){
            logger.warn("No Analyzed Project found for remoteRepoPath {}", remoteRepoPath);
            return null;
        }
    }

    public ProjectDTO findDTOByName(String name) {
        try {
            return entityManager.createQuery("Select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                    + "ProjectDTO(p.id, "
                    + "p.name, "
                    + "p.remoteRepoPath,  "
                    + "p.createdBy, "
                    + "p.creationDate,"
                    + "timeline.dateCreated, "
                    + "count(version.project.id)"
                    + ") from Project p "
                    + " join p.timeLines timeline "
                    + " join p.versions version "
                    + " where timeline.actionType = :actionType "
                    + " and p.name = :nameParam "
                    + " group by p.id, timeline.dateCreated", ProjectDTO.class)
                     .setParameter("actionType", ActionType.ANALYSED)
                     .setParameter("nameParam", name)
                     .getSingleResult();
        }catch (NoResultException nre){
            logger.warn("No Analyzed Project found for name {}", name);
            return null;
        }
    }

    public String getProjectName(String projectUrl) {
        TypedQuery<String> query = entityManager.createQuery("select p.name from Project p where p.remoteRepoPath = ?1", String.class).setParameter(1, projectUrl);
        List<String> resultList = query.getResultList();
        if (resultList.isEmpty())
            return null;
        return resultList.get(0);
    }

    public Project findById(Long projectId) {
        return entityManager.createQuery("select p from Project p join fetch p.versions versions where p.id = ?1", Project.class)
                 .setParameter(1, projectId)
                 .getSingleResult();
    }

    public List<Project> findByNameStart(String name) {
        return findByNameStartingLike(name);
    }

    public long getNumberOfVersions(Project project) {
        return entityManager.createQuery("select count(v.id) from Version v where v.project.id = ?1", Long.class)
                .setParameter(1, project.getId())
                .getSingleResult();
    }


    public List<ProjectDTO> findAllByActionTypeStatus(ActionType actionType) {
        return entityManager.createQuery("Select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                + "ProjectDTO(p.id, "
                + "p.name, "
                + "p.remoteRepoPath,  "
                + "p.createdBy, "
                + "p.creationDate,"
                + "timeline.dateCreated, "
                + "count(version.project.id)"
                + ") from Project p "
                + " join p.timeLines timeline "
                + " join p.versions version "
                + " where timeline.actionType = :actionType"
                + " group by p.id, timeline.dateCreated", ProjectDTO.class)
                .setParameter("actionType", actionType)
                .getResultList();
    }

}
