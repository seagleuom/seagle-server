package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.ProjectTimeline;
import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public abstract class ProjectTimelineRepository implements EntityRepository<ProjectTimeline, String>, EntityManagerDelegate<ProjectTimeline> {

    @Inject
    private EntityManager em;


    public List<ProjectTimeline> findByProjectURLAndType(String remoteRepoPath, ActionType type){
        return em.createQuery("SELECT tline from ProjectTimeline  tline "
                + "where tline.project.remoteRepoPath = ?1 "
                + " and tline.actionType = ?2",
                ProjectTimeline.class)
                 .setParameter(1, remoteRepoPath)
                 .setParameter(2, type)
                 .getResultList();
    }

    public List<ProjectTimeline> findByProjectIdAndType(Long projectId, ActionType type){
        return em.createQuery("SELECT tline from ProjectTimeline  tline "
                        + "where tline.project.id = ?1 "
                        + " and tline.actionType = ?2",
                ProjectTimeline.class)
                 .setParameter(1, projectId)
                 .setParameter(2, type)
                 .getResultList();
    }

}
