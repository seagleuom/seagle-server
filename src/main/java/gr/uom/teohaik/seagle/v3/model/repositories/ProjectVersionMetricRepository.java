package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.MetricValue;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectVersionMetric;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public abstract class ProjectVersionMetricRepository implements EntityRepository<ProjectVersionMetric, String>, 
                                                        EntityManagerDelegate<ProjectVersionMetric> 
{

    public ProjectVersionMetric saveProjectVersionMetric(Version version, MetricValue metricValue){
        ProjectVersionMetric pvm = new ProjectVersionMetric();
        pvm.setVersion(version);
        pvm.setMetricValue(metricValue);
        ProjectVersionMetric saved = save(pvm);
        return saved;
    }
    
     public abstract List<ProjectVersionMetric> findByVersion(Version versionID);
     
      public abstract List<ProjectVersionMetric> findByVersion_id(Long versionID);
    
    
}
