package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.v3.model.entities.VersionGraph;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import org.apache.deltaspike.data.api.EntityManagerDelegate;
import org.apache.deltaspike.data.api.EntityRepository;
import org.apache.deltaspike.data.api.Repository;

/**
 *
 * @author Theodore Chaikalis
 */
@Repository
@ApplicationScoped
public interface VersionGraphRepository extends EntityRepository<VersionGraph, String>, EntityManagerDelegate<VersionGraph> {

    Optional<VersionGraph> findOptionalById(Long id);

    Optional<VersionGraph> findOptionalByVersion_id(Long versionId);

}
