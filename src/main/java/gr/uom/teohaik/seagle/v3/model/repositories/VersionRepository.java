package gr.uom.teohaik.seagle.v3.model.repositories;

import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * @author Theodore Chaikalis
 */
@ApplicationScoped
public class VersionRepository extends AbstractSeagleRepository<Version, Long> {

    @Inject
    @SeagleLogger
    private org.slf4j.Logger logger;

    @Inject
    private EntityManager entityManager;

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    public List<VersionDTO> findAllVersionDTOsAnalyzed(Long projectId) {
        return entityManager.createQuery("Select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                + "VersionDTO(ver.id, ver.commitID, ver.name, ver.date) "
                + "from Version ver "
                + " join ver.project project "
                + " where project.id = :projectId "
                + " and ver.analyzed = :analyzed ", VersionDTO.class)
                 .setParameter("projectId", projectId)
                 .setParameter("analyzed", true)
                 .getResultList();
    }

    public List<Version> findAllAnalyzed(Long projectId) {
        return entityManager.createQuery("Select ver "
                + "from Version ver "
                + " join ver.project project "
                + " where project.id = :projectId "
                + " and ver.analyzed = :analyzed ", Version.class)
                 .setParameter("projectId", projectId)
                 .setParameter("analyzed", true)
                 .getResultList();
    }

    public List<Version> findAll(Long projectId) {
        return entityManager.createQuery("Select ver "
                + "from Version ver "
                + " join ver.project project "
                + " where project.id = :projectId ", Version.class)
                 .setParameter("projectId", projectId)
                 .getResultList();
    }

    public List<VersionDTO> findAllVersionDTOs(Long projectId) {
        return entityManager.createQuery("Select new gr.uom.teohaik.seagle.v3.ws.rest.model."
                + "VersionDTO(ver.id, ver.commitID, ver.name, ver.date) "
                + "from Version ver "
                + " join ver.project project "
                + " where project.id = :projectId ", VersionDTO.class)
                 .setParameter("projectId", projectId)
                 .getResultList();
    }

    public List<Version> findByProject_remoteRepoPath(String remoteRepoPath) {
        return entityManager.createQuery("Select ver "
                + "from Version ver "
                + " join ver.project project "
                + " where project.remoteRepoPath = :remoteRepoPath", Version.class)
                 .setParameter("remoteRepoPath", remoteRepoPath)
                 .getResultList();
    }

    public Optional<Version> findOptionalByCommitID(String commitId){
        return entityManager.createQuery("select ver "
                + "from Version ver "
                + "where ver.commitID = :commitId ",Version.class)
                .setParameter("commitId", commitId)
                .getResultList().stream().findFirst();
    }

    public Optional<Version> findOptionalById(Long versionId){
        return entityManager.createQuery("select ver "
                + "from Version ver "
                + "where ver.id = :versionId ",Version.class)
                 .setParameter("versionId", versionId)
                 .getResultList().stream().findFirst();
    }



}
