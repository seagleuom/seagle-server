//package gr.uom.teohaik.seagle.v3.project.repository;
//
//import gr.uom.se.util.module.annotations.Module;
//import gr.uom.se.util.module.annotations.Property;
//import gr.uom.se.util.module.annotations.ProvideModule;
//import gr.uom.se.util.validation.ArgsCheck;
//import gr.uom.se.vcs.VCSBranch;
//import gr.uom.se.vcs.VCSRepository;
//import gr.uom.se.vcs.analysis.version.provider.BranchTagProvider;
//import gr.uom.se.vcs.analysis.version.provider.ConnectedTagVersionNameProvider;
//import gr.uom.se.vcs.analysis.version.provider.ConnectedTagVersionProvider;
//import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionNameProvider;
//import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
//import gr.uom.se.vcs.analysis.version.provider.DefaultVersionNameResolver;
//import gr.uom.se.vcs.analysis.version.provider.TagProvider;
//import gr.uom.se.vcs.analysis.version.provider.VersionNameResolver;
//import gr.uom.se.vcs.analysis.version.provider.VersionString;
//import gr.uom.se.vcs.analysis.version.provider.VersionTagReachabilityValidator;
//import gr.uom.se.vcs.analysis.version.provider.VersionValidator;
//import gr.uom.se.vcs.exceptions.VCSRepositoryException;
//
//import java.util.Collection;
//import java.util.Comparator;
//
///**
// * An internal class to deal with different version provider implementations.
// * <p>
// * This implementation will provide VersionNameProvider and VersionProvider
// * implementations.
// * 
// * @author Elvis Ligu
// */
//@Module(provider = DefaultVersionProvider.class)
//public class DefaultVersionProvider {
//
//   private static final Object lock = new Object();
//   private static DefaultVersionProvider INSTANCE;
//
//   /**
//    * This is the provider method for repository provider instance.
//    * <p>
//    * This method should be used by modules API and will is not required to be
//    * used directly by clients. However clients may use this method, and it is
//    * not required each time to pass a repo manager after the instance is
//    * created.
//    * 
//    * @param repoManager
//    *           the repo manager to initialize the instance if it is not
//    *           initialized. May be null if the instance is initialized.
//    * @return this instance to resolve repositories.
//    */
//   @ProvideModule
//   public static DefaultVersionProvider getInstance() {
//      if (INSTANCE != null) {
//         return INSTANCE;
//      }
//      synchronized (lock) {
//         // Double check here
//         if (INSTANCE == null) {
//            INSTANCE = new DefaultVersionProvider();
//         }
//      }
//      return INSTANCE;
//   }
//
//   /**
//    * Used by module loader to load this implementation.
//    */
//   private DefaultVersionProvider() {
//   }
//
//   /**
//    * Get a version name resolver implementation.
//    * <p>
//    * This will return the default implementation. This method may be used by
//    * modules API in order to resolve a version name resolver dynamically.
//    * 
//    * @return a version name resolver implementation.
//    */
//   @ProvideModule
//   public VersionNameResolver getNameResolver() {
//      return new DefaultVersionNameResolver();
//   }
//
//   /**
//    * Get a version name provider implementation.
//    * <p>
//    * This will return a connected version name provider, by selecting the
//    * branch with the maximum number of connected tags (versions). The operation
//    * is very resource hungry and should be used wisely.
//    * <p>
//    * This implementation may be used by modules API in order to resolve a
//    * provider dynamically. However keep in mind that in order for repository to
//    * be injected should exist a repository provider, or a repository context
//    * which will provide the repository.
//    * 
//    * @param repo
//    *           from where the tags will be read
//    * @param resolver
//    *           version name resolver (usually will be injected by modules API)
//    * @param versionComparator
//    *           the comaparator of version strings to order versions.
//    * @return a tag provider implementation
//    * @throws VCSRepositoryException
//    *            if a problem occurs while reading repository.
//    */
//   @ProvideModule
//   public ConnectedVersionProvider getVersionProvider(
//         VCSRepository repo,
//         VersionNameResolver resolver,
//         @Property(name = "versionStringComparator") Comparator<VersionString> versionComparator)
//         throws VCSRepositoryException {
//      // Check if repository has any tags, if not throw an exception
//      ArgsCheck.notEmpty("tags collection", repo.getTags());
//      Collection<VCSBranch> branches = repo.getBranches();
//      ConnectedVersionNameProvider tagProvider = null;
//      int maxSize = 0;
//
//      for (VCSBranch branch : branches) {
//         TagProvider bprovider = getTagProvider(repo, branch.getName());
//         ConnectedTagVersionNameProvider versionProvider = new ConnectedTagVersionNameProvider(
//               bprovider, resolver, versionComparator);
//         int size = versionProvider.getCommits().size();
//         if (size > maxSize) {
//            tagProvider = versionProvider;
//            maxSize = size;
//         }
//      }
//      return new ConnectedTagVersionProvider(tagProvider);
//   }
//
//   /**
//    * Return a tag provider based on the given repository and given branch.
//    * <p>
//    * This implementation may be used by modules API in order to resolve a
//    * provider dynamically. However keep in mind that in order for repository to
//    * be injected should exist a repository provider, or a repository context
//    * which will provide the repository.
//    * 
//    * @param repo
//    *           from where the tags will be read
//    * @param branch
//    *           the name of the branch from where to retrieve tags. If the name
//    *           is null a default branch will be picked up.
//    * @return a tag provider implementation.
//    * @throws VCSRepositoryException
//    */
//   @ProvideModule
//   public TagProvider getTagProvider(VCSRepository repo,
//         @Property(name = "branchName") String branch)
//         throws VCSRepositoryException {
//      return new BranchTagProvider(repo, branch);
//   }
//
//   /**
//    * Return a version validator required to validate versions.
//    * <p>
//    * The returned version validator will be a tag reachability validator so
//    * using the version names should be tags.
//    * <p>
//    * This implementation may be used by modules API in order to resolve a
//    * provider dynamically. However keep in mind that in order for repository to
//    * be injected should exist a repository provider, or a repository context
//    * which will provide the repository.
//    * 
//    * @param repo
//    *           from where the tags will be read
//    * @param resolver
//    *           the version name resolver to check for names.
//    * @return a version validator implementations.
//    */
//   @ProvideModule
//   public VersionValidator getVersionValidator(VCSRepository repo,
//         VersionNameResolver resolver) {
//      return new VersionTagReachabilityValidator(resolver, repo);
//   }
//}
