package gr.uom.teohaik.seagle.v3.project.repository.nameResolvers;

import gr.uom.se.util.validation.ArgsCheck;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

/**
 *
 * @author Theodore Chaikalis
 */
public class GoogleCodeNameResolver implements ProjectNameResolver {

   private GoogleCodeNameResolver() {
   }
   
   private static ProjectNameResolver INSTANCE = new GoogleCodeNameResolver();
   
   public static ProjectNameResolver getInstance() {
      return INSTANCE;
   }
   
    @Override
    public String resolveName(String gitPath) {
       ArgsCheck.notNull("path", gitPath);
       URI uri = tryGetURI(gitPath);
       // Uri can not be resolved so a name can not to
       if (uri == null) {
          return null;
       }
       String scheme = uri.getScheme();
       if (scheme == null) {
          return null;
       }
       if (!(scheme.equalsIgnoreCase("http")
             || scheme.equalsIgnoreCase("https") || scheme
                .equalsIgnoreCase("git"))) {
          return null;
       }
       String host = uri.getHost();
       if(!host.equalsIgnoreCase("code.google.com")) {
          return null;
       }
       
       String name = null;
       String path = uri.getPath();
       if(path == null || path.isEmpty() || path.equals("/")) {
          return null;
       }
       
       
       int index = path.lastIndexOf('/');
       if(index > 0) {
          name = path.substring(index + 1);
       } else {
          // No code.google.com/p/repo found?
          return null;
       }
       
       if(name.endsWith(".git")) {
          int len = name.length();
          name = name.substring(0, len - 4);
       }
       if(name.isEmpty()) {
          name = null;
       }
       return name;
    }

    private URI tryGetURI(String path) {
       try {
          return new URI(path.trim());
       } catch (URISyntaxException e) {
          return null;
       }
    }
    
    /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.getClass().getName());
   }
    
    /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object obj) {
       if(obj == this) return true;
       if((obj instanceof GoogleCodeNameResolver) == false) return false;
      return Objects.equals(obj,this);
   }
}
