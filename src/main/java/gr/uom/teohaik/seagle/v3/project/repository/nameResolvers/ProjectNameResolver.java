package gr.uom.teohaik.seagle.v3.project.repository.nameResolvers;
/**
 *
 * @author Theodore Chaikalis
 */
public interface ProjectNameResolver {

    public abstract String resolveName(String gitPath);
}
