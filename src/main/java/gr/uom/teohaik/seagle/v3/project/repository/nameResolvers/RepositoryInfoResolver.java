package gr.uom.teohaik.seagle.v3.project.repository.nameResolvers;

/**
 * @author Elvis Ligu
 */
public interface RepositoryInfoResolver {


   /**
    * Try to resolve a project name based on the repository uri.
    * <p>
    * This will try to extract the project name based on the given repository
    * location. Keep in mind that a URI may be a remote location or a local one.
    * Generally speaking if this is a remote location it may analyze the remote
    * url in order to extract a name for the given project. If the uri is local
    * it may need to resolve a repository type and then open a repository to
    * read properties such as project name.
    * 
    * @param uri
    *           the repository uri where the system can locate the repository.
    * @return a project name for the given repository uri or null if it can not
    * extract a project name.
    */
   String tryResolveName(String uri);
}
