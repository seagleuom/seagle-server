package gr.uom.teohaik.seagle.v3.project.repository.nameResolvers;

import gr.uom.se.util.validation.ArgsCheck;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * A factory for RepositoryInfoResolver implementations.
 * <p>
 * This instance is a singleton and can be used to register all info resolvers
 * for known repositories. Clients must use instances of this factory in order
 * to resolve info for a given repository based on its remote url. They also
 * must use this factory in order to register other resolver implementations.
 * <p>
 * The factory implements RepositoryInfoResolver, though each time its methods
 * are called they are delegated to all registered resolver implementations.
 * Also this factory is registered as a manager into seagle manager so clients
 * may retrieve the instance of this factory by using seagle manager.
 * <p>
 * Generally speaking clients should not construct instances of this factory.
 * They should be provided by the system. If an object has a dependency on a
 * repository resolver he must specify in its constructor e parameter of type of
 * RepositoryInfoResolver and it will be injected by the system.
 * 
 * @author Elvis Ligu
 * @author Theodore Chaikalis
 */
public class RepositoryInfoResolverFactory implements RepositoryInfoResolver {

   private final Set<RepositoryInfoResolver> resolvers = new LinkedHashSet<>();
   private final ReadWriteLock lock = new ReentrantReadWriteLock();

   /**
    * Register known implementations. Used for internal purposes and only by the
    * module loader API.
    */
   private RepositoryInfoResolverFactory() {
      resolvers.add(RemoteGitRepositoryInfoResolver.getInstance());
   }

   private static final RepositoryInfoResolverFactory INSTANCE = new RepositoryInfoResolverFactory();

   /**
    * Get an instance of this factory.
    * <p>
    * To avoid coupling with this implementation use:
    * 
    * <pre>
    * RepositoryInfoResolver resolver = seagleManager
    *       .getManager(RepositoryInfoResolver.class);
    * </pre>
    * 
    * @return the instance of this factory.
    */
   public static RepositoryInfoResolverFactory getInstance() {
      return INSTANCE;
   }

   /**
    * Add a resolver implementation to this factory.
    * <p>
    * Each added resolver will be system wide resolver, as long as this factory
    * is used as the manager of resolver implementations.
    * 
    * @param resolver
    *           to be added to this factory.
    */
   public void addResolver(RepositoryInfoResolver resolver) {
      ArgsCheck.notNull("resolver", resolver);
      lock.writeLock().lock();
      try {
         resolvers.add(resolver);
      } finally {
         lock.writeLock().unlock();
      }
   }

   /**
    * Remove the given resolver from this factory.
    * <p>
    * If this resolver is removed, this will be reflected system wide as long as
    * this factory is used as a manager of resolvers.
    * 
    * @param resolver
    *           to be removed from this factory.
    */
   public void removeResolver(RepositoryInfoResolver resolver) {
      ArgsCheck.notNull("resolver", resolver);
      lock.writeLock().lock();
      try {
         resolvers.remove(resolver);
      } finally {
         lock.writeLock().unlock();
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String tryResolveName(String path) {
      lock.readLock().lock();
      try {
         for (RepositoryInfoResolver pnr : resolvers) {
            String name = pnr.tryResolveName(path);
            if (name != null) {
               return name;
            }
         }
         return null;
      } finally {
         lock.readLock().unlock();
      }
   }

}
