package gr.uom.teohaik.seagle.v3.project.repository.nameResolvers;

import gr.uom.se.util.validation.ArgsCheck;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

/**
 *
 * @author Theodore Chaikalis
 */
public class SourceForgeNameResolver implements ProjectNameResolver {

   /**
    * 
    */
   private SourceForgeNameResolver() {
   }

   private static ProjectNameResolver INSTANCE = new SourceForgeNameResolver();

   public static ProjectNameResolver getInstance() {
      return INSTANCE;
   }

   @Override
   public String resolveName(String gitPath) {
      ArgsCheck.notNull("path", gitPath);
      URI uri = tryGetURI(gitPath);
      // Uri can not be resolved so a name can not to
      if (uri == null) {
         return null;
      }
      String scheme = uri.getScheme();
      if (scheme == null) {
         return null;
      }
      if (!(scheme.equalsIgnoreCase("https") || scheme.equalsIgnoreCase("http") || scheme
            .equalsIgnoreCase("git"))) {
         return null;
      }
      String host = uri.getHost();
      if (!host.equalsIgnoreCase("git.code.sf.net")) {
         return null;
      }

      String name = null;
      String path = uri.getPath();
      if (path == null || path.isEmpty() || path.equals("/")) {
         return null;
      }

      if (path.startsWith("/")) {
         path = path.substring(1);
      }
      if (path.endsWith("/")) {
         path = path.substring(0, path.length() - 1);
      }

      String[] paths = path.split("/");
      if (paths.length == 3) {
         name = paths[1];
      }
      return name;
   }

   /**
    * {@inheritDoc}
    */
    /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.getClass().getName());
   }
    

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object obj) {
      return Objects.equals(this, obj);
   }

   private URI tryGetURI(String path) {
      try {
         return new URI(path.trim());
      } catch (URISyntaxException e) {
         return null;
      }
   }

}
