package gr.uom.teohaik.seagle.v3.ws.rest;

import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectRepository;
import java.util.Optional;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Theodore Chaikalis
 */

public class AbstractRestService {

    protected static final String DEFAULT_PURL = "NO_URL";

    protected static final String DEFAULT_METRIC = "NO_METRIC";

    protected static final String DEFAULT_VERSION = "NO_VERSION";

    protected static final String DEFAULT_CLASS_NAME = "NO_CLASS_NAME";

    @Context
    protected UriInfo uriInfo;

    @Inject
    ProjectRepository projectFacade;

    protected static String getErrorResponseForProject(String pUrl) {
        return "Required project: " + pUrl;
    }

    /**
     *
     */
    public AbstractRestService() {
        super();
    }

    /**
     * This method should be used in cases a bad request is received from
     * client.
     *
     * @param errMsg an error message to be return to the client.
     */
    protected void illegalRequest(String errMsg) {
        throw new RestApiException(Response.Status.BAD_REQUEST.getStatusCode(),
                Response.Status.BAD_REQUEST.getStatusCode(), errMsg, errMsg,
                uriInfo.getAbsolutePath().toString());
    }

    protected void projectExists(String errMsg) {
        throw new RestApiException(Response.Status.FOUND.getStatusCode(),
                Response.Status.FOUND.getStatusCode(), errMsg, errMsg,
                uriInfo.getAbsolutePath().toString());
    }

    /**
     * This method should be called in cases a NOT FOUND status code should be
     * returned to the client.
     * <p>
     *
     * @param msg an error message to be returned to the client.
     */
    protected void notFoundException(String msg) {
        if (msg == null) {
            msg = "resource not found";
        }
        throw new NotFoundException(msg, notFoundResponse());
    }

    /**
     * Create a NOT FOUND response.
     *
     * @return
     */
    protected Response notFoundResponse() {
        return Response.status(Status.NOT_FOUND)
                .location(uriInfo.getRequestUri()).build();
    }

    protected Project findProjectByName(String name) {
        return projectFacade.findByNameExact(name);
    }

    protected Project findProjectByUrl(String purl) {
        Optional<Project> optionalProject = projectFacade.findByRemoteRepoPath(purl);
        return (optionalProject.isPresent())? optionalProject.get() : new Project();
    }

}

