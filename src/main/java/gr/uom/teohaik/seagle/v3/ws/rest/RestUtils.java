package gr.uom.teohaik.seagle.v3.ws.rest;

import static gr.uom.teohaik.seagle.v3.SeagleConstants.NO_PARAM;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;


/**
 *
 * @author Theodore Chaikalis
 */

public class RestUtils {

   public static Response buildDefault(ResponseBuilder builder) {
      return builder.build();
   }


   public static void parameterCheck(String projectUrl) {
      if (projectUrl.equals(NO_PARAM) || projectUrl.length() == 0) {
         String errMsg = "Required project: purl must be specified";
         throw new RuntimeException(errMsg);
      }
      if (!projectUrl.contains("github")) {
         String errMsg = "Currently, only projects from Github can be analyzed";
         throw new RuntimeException(errMsg);
      }
   }


}
