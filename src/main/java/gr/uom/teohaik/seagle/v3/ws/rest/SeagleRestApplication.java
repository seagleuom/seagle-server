package gr.uom.teohaik.seagle.v3.ws.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Theodore Chaikalis
 */
@ApplicationPath("rs")
public class SeagleRestApplication extends Application {
// no configuration required
}