package gr.uom.teohaik.seagle.v3.ws.rest.model;


/**
 *
 * @author Theodore Chaikalis
 */

public class MetricValueDTO {

   private String mnemonic;
   private String className;
   private double value;


   /**
    * 
    */
   public MetricValueDTO() {
   }

   public MetricValueDTO(String mnemonic, double value) {
      this.mnemonic = mnemonic;
      this.value = value;
      className = " ";
   }
   
   public MetricValueDTO(String mnemonic, double value, String className) {
      this.mnemonic = mnemonic;
      this.value = value;
      this.className = className;
   }
   
   public String getMnemonic() {
      return mnemonic;
   }

   public void setMnemonic(String metric) {
      this.mnemonic = metric;
   }

   public double getValue() {
      return value;
   }

   public void setValue(double value) {
      this.value = value;
   }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

   @Override
   public String toString() {
      return "MetricValueDTO{" +
              "mnemonic='" + mnemonic + '\'' +
              ", className='" + className + '\'' +
              ", value=" + value +
              '}';
   }
}
