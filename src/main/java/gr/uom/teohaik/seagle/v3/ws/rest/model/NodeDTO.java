
package gr.uom.teohaik.seagle.v3.ws.rest.model;

/**
 *
 * @author Theodore Chaikalis
 */
public class NodeDTO {

    private Long nodeId;
    private Long versionId;
    private String className;
    private String simpleName;
    private String filePath;

    public NodeDTO(String className, String simpleName) {
        this.className = className;
        this.simpleName = simpleName;
    }

    public NodeDTO(String className, String simpleName, String filePath) {
        this.className = className;
        this.simpleName = simpleName;
        this.filePath = filePath;
    }


    public NodeDTO(Long nodeId, Long versionId, String className, String simpleName) {
        this.nodeId = nodeId;
        this.versionId = versionId;
        this.className = className;
        this.simpleName = simpleName;
    }

    public NodeDTO(Long nodeId, Long versionId, String className, String simpleName, String filePath) {
        this.nodeId = nodeId;
        this.versionId = versionId;
        this.className = className;
        this.simpleName = simpleName;
        this.filePath = filePath;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getVersionId() {
        return versionId;
    }


    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }

    @Override
    public String toString() {
        return "NodeDTO{" +
                "nodeId=" + nodeId +
                ", versionId=" + versionId +
                ", className='" + className + '\'' +
                ", simpleName='" + simpleName + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
