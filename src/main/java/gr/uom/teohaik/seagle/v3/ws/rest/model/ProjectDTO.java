package gr.uom.teohaik.seagle.v3.ws.rest.model;

import gr.uom.teohaik.seagle.v3.analysis.project.evolution.SoftwareProject;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Theodore Chaikalis
 */
public class ProjectDTO implements Comparable<ProjectDTO> {

    private Long id;
    private String name;
    private String remoteRepoPath;
    private String createdBy;
    private String dateCreated;
    private String updated;
    private String analyzed;

    private Long versionCount;
    private Collection<VersionDTO> versions;

    private Collection<MetricValueDTO> metrics;

    private transient List<? extends SoftwareProject> softwareProjects;

    public ProjectDTO(Long id, String name, String url) {
        this.id = id;
        this.name = name;
        this.remoteRepoPath = url;
        this.metrics = new ArrayList<>();
        this.versions = new ArrayList<>();
        this.softwareProjects = new ArrayList<>();
    }

    public ProjectDTO() {
        this.metrics = new ArrayList<>();
        this.versions = new ArrayList<>();
        this.softwareProjects = new ArrayList<>();
    }

    public ProjectDTO(Long id,
            String name,
            String remoteRepoPath,
            String createdBy,
            LocalDateTime dateCreated,
            Date analyzed,
            Long versionCount) {
        this.id = id;
        this.name = name;
        this.remoteRepoPath = remoteRepoPath;
        this.createdBy = createdBy;
        this.dateCreated = dateCreated.toString();
        this.analyzed = analyzed.toString();
        this.versionCount = versionCount;

        this.metrics = new ArrayList<>();
        this.versions = new ArrayList<>();
        this.softwareProjects = new ArrayList<>();
    }

    public void addMetric(MetricValueDTO metric) {
        this.metrics.add(metric);
    }

    public void addVersion(VersionDTO restVersion) {
        versions.add(restVersion);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemoteRepoPath() {
        return remoteRepoPath;
    }

    public void setRemoteRepoPath(String remoteRepoPath) {
        this.remoteRepoPath = remoteRepoPath;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getAnalyzed() {
        return analyzed;
    }

    public void setAnalyzed(String analyzed) {
        this.analyzed = analyzed;
    }

    public void setVersionCount(Long versionCount) {
        this.versionCount = versionCount;
    }

    public Long getVersionCount() {
        return versionCount;
    }

    public void setVersionCount(long versionCount) {
        this.versionCount = versionCount;
    }

    public Collection<VersionDTO> getVersions() {
        return versions;
    }

    public Collection<MetricValueDTO> getMetrics() {
        return metrics;
    }

    public void setVersions(Collection<VersionDTO> versions) {
        this.versions = versions;
    }

    public void setMetrics(Collection<MetricValueDTO> metrics) {
        this.metrics = metrics;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public List<? extends SoftwareProject> getSoftwareProjects() {
        return softwareProjects;
    }

    public void setSoftwareProjects(List<? extends SoftwareProject> softwareProjects) {
        this.softwareProjects = softwareProjects;
    }

    @Override
    public String toString() {
        return "ProjectDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", remoteRepoPath='" + remoteRepoPath + '\'' +
                ", dateCreated='" + dateCreated + '\'' +
                ", updated='" + updated + '\'' +
                ", analyzed='" + analyzed + '\'' +
                ", versionCount=" + versionCount +
                '}';
    }


    @Override
    public int compareTo(ProjectDTO o) {
        return 0;
    }

}
