package gr.uom.teohaik.seagle.v3.ws.rest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;



/**
 *
 * @author Theodore Chaikalis
 */

public class RESTProjectCollection {

   private Collection<ProjectDTO> projects;

   public RESTProjectCollection() {
      this.projects = new ArrayList<>();
   }
   
   public RESTProjectCollection(Collection<ProjectDTO> projects) {
      this.projects = new ArrayList<>();
      this.setProjects(projects);
   }
   
   public Collection<ProjectDTO> getProjects() {
      return projects;
   }

   public void setProjects(Collection<ProjectDTO> projects) {
      if(projects == null) {
         this.projects = new TreeSet<>();
      } else {
         this.projects = projects;
      }
   }
   
   public void add(ProjectDTO project) {
      this.projects.add(project);
   }

   
}
