package gr.uom.teohaik.seagle.v3.ws.rest.model;

import java.util.ArrayList;
import java.util.Collection;



/**
 *
 * @author Theodore Chaikalis
 */
public class RESTVersionCollection {

    private Collection<VersionDTO> versions;

    /**
     *
     */
    public RESTVersionCollection() {
        this(null);
    }

    public RESTVersionCollection(Collection<VersionDTO> versions) {
        this.versions = new ArrayList<>();
        this.setVersions(versions);
    }

    public Collection<VersionDTO> getVersions() {
        return versions;
    }

    public void setVersions(Collection<VersionDTO> versions) {
        if (versions == null) {
            versions = new ArrayList<>();
        }
        this.versions = versions;
    }

    public void add(VersionDTO version) {
        this.versions.add(version);
    }

}
