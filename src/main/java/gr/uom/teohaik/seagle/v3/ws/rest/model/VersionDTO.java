package gr.uom.teohaik.seagle.v3.ws.rest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
/**
 *
 * @author Theodore Chaikalis
 */
public class VersionDTO {

    private Long versionDbId;
    private String commitHashId;
    private String versionName;
    private Date created;
    private Integer nodeCount;

    private Collection<MetricValueDTO> metrics;
    private Collection<NodeDTO> nodeList;

    public VersionDTO() {
        this.metrics = new ArrayList<>();
        this.nodeList = new ArrayList<>();
    }

    public VersionDTO(Long id, String commitId, String versionName, Date date) {
        this.versionDbId = id;
        this.commitHashId = commitId;
        this.versionName = versionName;
        this.created = date;
        this.metrics = new ArrayList<>();
        this.nodeList = new ArrayList<>();
    }
    
    public void addNodeDTO(NodeDTO nodeDto){
        nodeList.add(nodeDto);
    }

    public void addMetric(MetricValueDTO metric) {
        this.metrics.add(metric);
    }

    public Long getVersionDbId() {
        return versionDbId;
    }

    public String getCommitHashId() {
        return commitHashId;
    }

    public Date getDate() {
        return created;
    }

    public String getName() {
        return versionName;
    }

    public void setName(String vname) {
        this.versionName = vname;
    }

    public void setVersionDbId(Long versionDbId) {
        this.versionDbId = versionDbId;
    }

    public void setDate(Date cdate) {
        this.created = cdate;
    }

    public void setCommitHashId(String id) {
        this.commitHashId = id;
    }

    public Collection<MetricValueDTO> getMetrics() {
        return metrics;
    }

    public void setMetrics(Collection<MetricValueDTO> metrics) {
        this.metrics = metrics;
    }

    public Collection<NodeDTO> getNodeList() {
        return nodeList;
    }

    public void setNodeList(Collection<NodeDTO> nodeList) {
        this.nodeList = nodeList;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(Integer nodeCount) {
        this.nodeCount = nodeCount;
    }

    @Override
    public int hashCode() {
        return versionDbId.hashCode()+commitHashId.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        }
        if (this == o){
            return true;
        }
        if (this.getClass() != o.getClass()){
            return false;
        }
        VersionDTO other = (VersionDTO) o;
        return commitHashId.equals(other.commitHashId);
    }

    @Override
    public String toString() {
        return "VersionDTO{" +
                "versionDbId=" + versionDbId +
                ", commitHashId='" + commitHashId + '\'' +
                ", versionName='" + versionName + '\'' +
                ", created=" + created +
                ", nodeCount=" + nodeCount +
                '}';
    }
}
