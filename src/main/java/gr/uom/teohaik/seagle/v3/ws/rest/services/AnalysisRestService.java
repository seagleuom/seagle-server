package gr.uom.teohaik.seagle.v3.ws.rest.services;

import static gr.uom.teohaik.seagle.v3.SeagleConstants.NO_PARAM;
import static gr.uom.teohaik.seagle.v3.ws.rest.RestUtils.parameterCheck;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import gr.uom.teohaik.seagle.services.ProjectService;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.events.ProjectAnalysisRequest;
import gr.uom.teohaik.seagle.v3.events.RequestForWorkEvent;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectTimelineRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.VersionRepository;
import gr.uom.teohaik.seagle.v3.ws.rest.AbstractRestService;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;

/**
 *
 * @author Theodore Chaikalis
 */
@Path("/analysis")
public class AnalysisRestService extends AbstractRestService {

    @Inject
    ProjectTimelineRepository projectTimelineRepository;

    @Inject
    VersionRepository versionRepository;

    @Inject
    ProjectService projectService;

    @Inject
    private Event<ProjectAnalysisRequest> cloneRequestEvent;

    @Resource
    ManagedExecutorService mes;

    @Inject
    @SeagleLogger
    Logger logger;

    @Inject
    private Event<RequestForWorkEvent> workEvent;

   
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public void requestAnalysis(@Suspended AsyncResponse asyncResponse,
            final @QueryParam("requestorEmail") @DefaultValue(NO_PARAM) String email,
            final @DefaultValue(NO_PARAM) @QueryParam("purl") String projectUrl) {

        logger.info("Analysis requested for project with remote repo: {}", projectUrl);
        try {
            parameterCheck(projectUrl);
            ProjectDTO projectDTO = new ProjectDTO();
            projectDTO.setCreatedBy(email);
            projectDTO.setRemoteRepoPath(projectUrl);
            ProjectDTO project = notifyAnalysisRequest(projectDTO);
            asyncResponse.resume(Response.ok("ok").entity(project).build());
        } catch (Exception e) {
            asyncResponse.resume(Response.status(BAD_REQUEST).entity(e).build());
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void requestAnalysisForVersions(@Suspended AsyncResponse asyncResponse, ProjectDTO project) {

        logger.info("Analysis requested for specific versions of a project ");
        try {
            notifyAnalysisRequest(project);
            asyncResponse.resume(Response.ok("ok").entity(project).build());
        } catch (Exception e) {
            asyncResponse.resume(Response.status(BAD_REQUEST).entity(e).build());
        }
    }

    private ProjectDTO notifyAnalysisRequest(ProjectDTO projectDTO) {
        ProjectAnalysisRequest analysisInfo = new ProjectAnalysisRequest(projectDTO);

        Project project = projectService.createProject(projectDTO.getCreatedBy(), projectDTO.getRemoteRepoPath());

        logger.info("Analysis async request fired. Url: {}", analysisInfo);
        cloneRequestEvent.fireAsync(analysisInfo);

        analysisInfo.getProject().setAnalyzed("ANALYSIS_IN_PROGRESS");
        return analysisInfo.getProject();
    }



//    /**
//     * {@inheritDoc}
//     */
//    @Path("/batch")
//    @POST
//    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
//    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
//    public Response doBatchAnalysis(final RESTBatchAnalysis batchAnalysis) {
//
//        if (batchAnalysis == null) {
//            String errMsg = "no project urls defined in request";
//            logger.log(Level.INFO, errMsg);
//            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
//        }
//        final Collection<String> rurls = batchAnalysis.getUrls();
//        if (rurls == null || rurls.isEmpty()) {
//            String errMsg = "no project urls defined in request";
//            logger.log(Level.INFO, errMsg);
//            return Response.status(Response.Status.BAD_REQUEST).entity(errMsg).build();
//        }
//
//        final Set<String> urls = new HashSet<>();
//        urls.addAll(rurls);
//        // Clear duplicate urls and clone first
//        doBatch(batchAnalysis);
//        return Response.ok(urls).build();
//    }
//
//    private void doBatch(final RESTBatchAnalysis batchAnalysis) {
//        final Collection<String> rurls = batchAnalysis.getUrls();
//
//        final Set<String> urls = new HashSet<>();
//        urls.addAll(rurls);
//        rurls.clear();
//
//        for (String url : urls) {
//            try {
//                cloneRepo(url);
//                rurls.add(url);
//            } catch (Exception ex) {
//                logger.log(Level.WARNING, "Project with url " + url + " could not be downloaded. Probably needs authentication");
//            }
//        }
//
//        // If no url remained just return
//        if (rurls.isEmpty()) {
//            illegalRequest("no project urls defined in request");
//        } else {
//            // Now try to schedule an analysis for each url
//            urls.clear();
//            try {
//                for (String url : rurls) {
//                    notifyAnalysisRequest(batchAnalysis.getEmail(), url);
//                    urls.add(url);
//                }
//            } catch (Exception ex) {
//            }
//        }
//
//        // If no url was scheduled just return a bad request
//        if (urls.isEmpty()) {
//            illegalRequest("No project was scheduled for analysis");
//        }
//    }
}
