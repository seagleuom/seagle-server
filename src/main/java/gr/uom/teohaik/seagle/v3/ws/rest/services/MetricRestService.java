package gr.uom.teohaik.seagle.v3.ws.rest.services;

import gr.uom.teohaik.seagle.services.GraphService;
import gr.uom.teohaik.seagle.services.MetricService;
import gr.uom.teohaik.seagle.services.ProjectService;
import gr.uom.teohaik.seagle.services.VersionService;
import gr.uom.teohaik.seagle.util.SeagleLogger;
import gr.uom.teohaik.seagle.v3.model.entities.Metric;
import gr.uom.teohaik.seagle.v3.model.entities.MetricValue;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectMetric;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectVersionMetric;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.ws.rest.AbstractRestService;
import gr.uom.teohaik.seagle.v3.ws.rest.model.MetricValueDTO;
import gr.uom.teohaik.seagle.v3.ws.rest.model.NodeDTO;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;

/**
 * @author Theodore Chaikalis
 */
@Stateless
@Path("/metric")
public class MetricRestService extends AbstractRestService {

    @Inject
    @SeagleLogger
    private Logger logger;

    @Inject
    private VersionService versionService;

    @Inject
    private GraphService graphService;

    @Inject
    private ProjectService projectService;

    @Inject
    private MetricService metricService;

    @GET
    @Path("/classes/project")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectClassesByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_VERSION) @QueryParam("versionID") String versionID) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (versionID.equals(DEFAULT_VERSION)) {
            String errMsg = getErrorResponseForProject("A version ID must be specified");
            illegalRequest(errMsg);
        }

        return getAllClassesForAVersion(versionID);
    }

    @GET
    @Path("/project/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectMetricsByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform her
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByName(name);
        if (project == null) {
            notFoundException("Project not found with name: " + name);
        }
        return getMetrics(project);
    }

    @GET
    @Path("/values/project/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectMetricValuesByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {
        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (name.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("name path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        ProjectDTO project = projectService.findProjectDTOByName(name);
        if (project == null) {
            notFoundException("Project not found with name: " + name);
        }
        return getAllMetricsAndValuesForAProject(project);
    }

    @GET
    @Path("/values/project")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectMetricValuesByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db

        ProjectDTO project = projectService.findProjectDTOByRepoUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getAllMetricsAndValuesForAProject(project);
    }

    @GET
    @Path("/single/values/project")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectSpecificMetricValuesByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (purl.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("metricMnemonic must be specified");
            illegalRequest(errMsg);
        }
        ProjectDTO project = projectService.findProjectDTOByRepoUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getSpecificMetricValuesForAProject(project, metricMnemonic);
    }

    @GET
    @Path("/single/values/project/forVersion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectSpecificMetricValuesForSpecificVersionByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic,
            @DefaultValue(DEFAULT_VERSION) @QueryParam("versionID") String versionID) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (metricMnemonic.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("metricMnemonic must be specified");
            illegalRequest(errMsg);
        }
        if (versionID.equals(DEFAULT_VERSION)) {
            String errMsg = getErrorResponseForProject("A version ID must be specified");
            illegalRequest(errMsg);
        }
        ProjectDTO project = projectService.findProjectDTOByRepoUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }

        Long metricId = metricService.findMetricIdByMnemonic(metricMnemonic);
        if (metricId == null) {
            String errMsg = getErrorResponseForProject("metricMnemonic "+metricMnemonic+" does not exist!");
            illegalRequest(errMsg);
        }

        Version version = versionService.findVersionByCommitId(versionID);
        if (version  == null) {
            version = versionService.findVersionById(Long.valueOf(versionID));
            if(version == null) {
                String errMsg = getErrorResponseForProject("version  does not exist!");
                illegalRequest(errMsg);
            }
        }
        return getSpecificMetricValuesForAProjectAndForAVersion(project, metricId, version);
    }

    @GET
    @Path("/single/values/project/forClass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectSpecificMetricValuesForSpecificClassByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl,
            @DefaultValue(DEFAULT_METRIC) @QueryParam("metricMnemonic") String metricMnemonic,
            @DefaultValue(DEFAULT_CLASS_NAME) @QueryParam("className") String className) {

        if (purl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("purl path must be specified");
            illegalRequest(errMsg);
        }
        if (metricMnemonic.equals(DEFAULT_METRIC)) {
            String errMsg = getErrorResponseForProject("metricMnemonic must be specified");
            illegalRequest(errMsg);
        }
        if (className.equals(DEFAULT_CLASS_NAME)) {
            String errMsg = getErrorResponseForProject("A fully qualified class name must be specified");
            illegalRequest(errMsg);
        }
        ProjectDTO project = projectService.findProjectDTOByRepoUrl(purl);
        if (project == null) {
            notFoundException("Project not found with url: " + purl);
        }
        return getSpecificMetricValuesForAProjectAndForAClass(project, metricMnemonic, className);
    }

    @GET
    @Path("/values/forClass")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMetricValuesForSpecificClass(
            @DefaultValue(DEFAULT_VERSION) @QueryParam("commitID") String commitID,
            @DefaultValue(DEFAULT_CLASS_NAME) @QueryParam("className") String className) {

        if (commitID.equals(DEFAULT_VERSION)) {
            String errMsg = getErrorResponseForProject("versionID must be specified");
            illegalRequest(errMsg);
        }
        if (className.equals(DEFAULT_CLASS_NAME)) {
            String errMsg = getErrorResponseForProject("A fully qualified class name must be specified");
            illegalRequest(errMsg);
        }
        Version version = versionService.findVersionByCommitId(commitID);
        if (version == null) {
            notFoundException("No project found with commit: " + commitID);
        }
        return getAllMetricValuesForAClassForAVersion(className, version);
    }

    @GET
    @Path("/project-url")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectMetricsByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String url) {

        // If the client didn't specified a project name we should
        // return a 400 (Bad Request) code to inform it
        if (url.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponseForProject("'purl' parameter must be specified");
            illegalRequest(errMsg);
        }
        // Look up the project in db
        Project project = findProjectByUrl(url);
        if (project == null) {
            notFoundException("Project not found with url: " + url);
        }
        return getMetrics(project);
    }


    private Response getSpecificMetricValuesForAProjectAndForAVersion(ProjectDTO project,
            Long metricId,
            Version version) {
        ProjectDTO restProject = createRestProject(project);
        addNodeMetric(version, restProject, metricId);
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private Response getAllMetricValuesForAClassForAVersion(String className, Version version) {
        ProjectDTO project = projectService.findProjectDTOByRepoUrl(version.getProject().getRemoteRepoPath());
        ProjectDTO restProject = createRestProject(project);
        VersionDTO restVersion = createVersionDTO(version);
        restProject.addVersion(restVersion);

        List<MetricValueDTO> metricValues =
                metricService.findMetricValueDTOsByNodeNameAndVersionId(className, null, version.getId());

        restProject.getMetrics().addAll(metricValues);

        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private Response getSpecificMetricValuesForAProjectAndForAClass(ProjectDTO project,
            String metricMnemonic, String className) {
        ProjectDTO restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionService.findAnalyzedByProject(project.getRemoteRepoPath())) {
            addNodeMetricForClass(version, restProject, metricMnemonic, className);
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private void addNodeMetricForClass(Version version, ProjectDTO restProject,
            String metricMnemonic, String className) {
        VersionDTO restVersion =  createVersionDTO(version);
        restProject.addVersion(restVersion);

        List<MetricValueDTO> metricValueDTOsByNodeNameAndVersionId =
                metricService.findMetricValueDTOsByNodeNameAndVersionId(className, metricMnemonic, version.getId());
        restVersion.getMetrics().addAll(metricValueDTOsByNodeNameAndVersionId);
    }

    private Response getMetrics(Project project) {
        Collection<Metric> metrics = project.getRegisteredMetrics();
        return Response.ok(metrics, MediaType.APPLICATION_JSON).build();
    }

    private Response getAllMetricsAndValuesForAProject(ProjectDTO project) {
        ProjectDTO restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionService.findAnalyzedByProject(project.getRemoteRepoPath())) {
            addAllMetrics(version, restProject);
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private Response getSpecificMetricValuesForAProject(ProjectDTO project, String metricMnemonic) {
        ProjectDTO restProject = createRestProject(project);
        // Get project versions and for each version get its metrics
        for (Version version : versionService.findAnalyzedByProject(project.getRemoteRepoPath())) {
            addMetric(version, restProject, metricMnemonic);
        }
        return Response.ok(restProject, MediaType.APPLICATION_JSON).build();
    }

    private void addAllMetrics(Version version, ProjectDTO restProject) {
        VersionDTO restVersion = createVersionDTO(version);
        restProject.addVersion(restVersion);
        // Set project version metrics (such as repo metrics)
        for (ProjectVersionMetric pvm : metricService.findProjectVersionMetricsByVersion(version)) {
            MetricValue value = pvm.getMetricValue();
            String mnemonic = metricService.findMetricById(value.getMetricId()).getMnemonic();
            MetricValueDTO mv = new MetricValueDTO(mnemonic, value.getValue());
            restVersion.addMetric(mv);
        }
    }

    private Response getAllClassesForAVersion(String versionID) {
        Version version = versionService.findVersionByCommitId(versionID);
        List<NodeDTO> nodeDTOs = graphService.findNodesDTOByVersion(version.getId());
        VersionDTO versionDTO =  createVersionDTO(version);
        versionDTO.setNodeList(nodeDTOs);
        versionDTO.setNodeCount(nodeDTOs.size());
        return Response.ok(versionDTO, MediaType.APPLICATION_JSON).build();
    }

    private void addNodeMetric(Version version, ProjectDTO restProject, Long metricId) {
        VersionDTO restVersion = createVersionDTO(version);
        restProject.addVersion(restVersion);
        // Set node version metrics
        List<MetricValueDTO> metricValueDTOs = metricService.findMetricValueDTOsByMetricIdAndVersionId(metricId, version.getId());
        restProject.getMetrics().addAll(metricValueDTOs);
    }

    private VersionDTO createVersionDTO(Version version) {
        VersionDTO restVersion = new VersionDTO();
        restVersion.setVersionDbId(version.getId());
        restVersion.setCommitHashId(version.getCommitID());
        restVersion.setName(version.getName());
        restVersion.setDate(version.getDate());
        restVersion.setVersionName(version.getName());
        restVersion.setCreated(Date.from(version.getCreationDate().atZone(ZoneId.systemDefault())
                                                .toInstant()));
        return restVersion;
    }

    private void addMetric(Version version, ProjectDTO restProject, String metricMnemonic) {
        VersionDTO restVersion =  createVersionDTO(version);
        restProject.addVersion(restVersion);
        // Set project version metrics (such as repo metrics)
        for (ProjectVersionMetric pvm : metricService.findProjectVersionMetricsByVersion(version)) {
            MetricValue value = pvm.getMetricValue();
            MetricValueDTO mv = new MetricValueDTO(metricMnemonic, value.getValue());
            restVersion.addMetric(mv);
        }
    }

    private ProjectDTO createRestProject(ProjectDTO project) {
        // Create a rest project to return to client
        // Get project metrics first
        for (ProjectMetric pm : metricService.findProjectMetricByProject(project.getId())) {
            MetricValue value = pm.getMetricValue();
            String mnemonic = metricService.findMetricById(value.getMetricId()).getMnemonic();
            MetricValueDTO mv = new MetricValueDTO(mnemonic, value.getValue());
            project.addMetric(mv);
        }
        return project;
    }

}
