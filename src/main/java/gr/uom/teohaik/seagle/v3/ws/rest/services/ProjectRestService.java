package gr.uom.teohaik.seagle.v3.ws.rest.services;

import static gr.uom.teohaik.seagle.v3.SeagleConstants.NO_PARAM;
import static gr.uom.teohaik.seagle.v3.ws.rest.RestUtils.parameterCheck;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import gr.uom.teohaik.seagle.services.ProjectService;
import gr.uom.teohaik.seagle.v3.events.VersionDeterminationRequest;
import gr.uom.teohaik.seagle.v3.model.entities.Project;
import gr.uom.teohaik.seagle.v3.model.entities.ProjectTimeline;
import gr.uom.teohaik.seagle.v3.model.enums.ActionType;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectRepository;
import gr.uom.teohaik.seagle.v3.model.repositories.ProjectTimelineRepository;
import gr.uom.teohaik.seagle.v3.ws.rest.AbstractRestService;
import gr.uom.teohaik.seagle.v3.ws.rest.RestApiException;
import gr.uom.teohaik.seagle.v3.ws.rest.model.ProjectDTO;
import gr.uom.teohaik.seagle.v3.ws.rest.model.RESTProjectCollection;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Theodore Chaikalis
 */

@Path("/project")
@Stateless
public class ProjectRestService extends AbstractRestService {

    private static final Logger logger = LoggerFactory.getLogger(ProjectRestService.class.getName());

    @EJB
    ProjectService projectService;

    @Inject
    ProjectRepository projectRepository;

    @Inject
    ProjectTimelineRepository timelineRepository;

    @Inject
    private Event<VersionDeterminationRequest> versionDeterminationRequestEvent;

    private void illegalPurl() {
        String errMsg = "purl parameter must be specified";
        illegalRequest(errMsg);
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public void registerProject(@Suspended AsyncResponse asyncResponse,
            final @QueryParam("requestorEmail") @DefaultValue(NO_PARAM) String email,
            final @DefaultValue(NO_PARAM) @QueryParam("purl") String pUrl) {

        logger.info("Registration requested for project with remote repo: {}", pUrl);
        try {
            parameterCheck(pUrl);

            // First check if the version exists
            Project project = projectService.findProjectByRepoUrl(pUrl);
            if (project == null) {
                project = projectService.createProject(email, pUrl);

                logger.info("Project Created. Id = [{}]", project.getId());

                VersionDeterminationRequest verDeterminRequest =
                        new VersionDeterminationRequest(project);
                versionDeterminationRequestEvent.fireAsync(verDeterminRequest);

                ProjectDTO projectDTO = projectService.convertProjectToDTO(project);
                asyncResponse.resume(Response.status(Status.CREATED).entity(projectDTO).build());

            } else {
                asyncResponse.resume(Response.status(Status.FOUND).build());
            }
        } catch (Exception e) {
            asyncResponse.resume(Response.status(BAD_REQUEST).entity(e).build());
        }
    }

    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectByName(
            @DefaultValue(DEFAULT_PURL) @PathParam("name") String name) {
        // Just call project manager to clone the project
        List<Project> projects = findProjectByNameStart(name);
        if (projects == null) {
            logger.info("project with name like {} was not found", name);
            return Response
                    .ok(new RESTProjectCollection(), MediaType.APPLICATION_JSON).build();
        }
        return Response
                .ok(getAnalyzedProjects(projects), MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProjectByUrl(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String purl) {
        // If the client didn't specified a repository url we should
        // return all projects
        if (purl.equals(DEFAULT_PURL)) {
            return Response.ok(getAnalyzedProjects(), MediaType.APPLICATION_JSON).build();
        }

        String name = null;
        try {
            name = projectService.resolveProjectName(purl);
        } catch (Exception ex) {
            String errMsg = "Project with url " + purl + " does not exist";
            logger.info(errMsg);
            return Response.status(Response.Status.NOT_FOUND).entity(errMsg).build();
        }

        // Just call project facade to get the project
        Project project = findProjectByName(name);
        if (project == null || project.getProjectInfo().getDateCreated() == null) {
            logger.info("REST call for project with purl = {}, not found", purl);
            return Response.ok(new RESTProjectCollection(), MediaType.APPLICATION_JSON).build();
        }
        RESTProjectCollection collection = new RESTProjectCollection();
        collection.add(createRestProject(project));
        return Response
                .ok(collection, MediaType.APPLICATION_JSON).build();
    }

    private List<Project> findProjectByNameStart(String name) {
        // Get the project remote url from DB
        List<Project> list = projectRepository.findByNameStart(name);
        if (!list.isEmpty()) {
            return list;
        }
        return null;
    }

    @GET
    public Response getAllAnalyzedProjects() {
        RESTProjectCollection analyzedProjects = getAnalyzedProjects();
        return Response.ok(analyzedProjects, MediaType.APPLICATION_JSON).build();
    }


    public RESTProjectCollection getAnalyzedProjects() {
        List<ProjectDTO> projectDTOS = projectService.findAllAnalyzed();
        RESTProjectCollection restProjectCollection = new RESTProjectCollection();
        restProjectCollection.setProjects(projectDTOS);
        return restProjectCollection;
    }


    public RESTProjectCollection getAnalyzedProjects(Collection<Project> projects) {
        RESTProjectCollection restProjectCollection = new RESTProjectCollection();
        for(Project p : projects) {
            restProjectCollection.add(createRestProject(p));
        }
        return restProjectCollection;
    }


    private ProjectDTO createRestProject(Project project) {
        String url = project.getRemoteRepoPath();
        ProjectDTO rproject = new ProjectDTO(project.getId(), project.getName(), url);
        // Set the number of versions
        long count = projectRepository.getNumberOfVersions(project);
        rproject.setVersionCount(count);
        return rproject;
    }

    private ProjectTimeline getFirstTimeline(Project project, ActionType type) {
        List<ProjectTimeline> timelines = timelineRepository.findByProjectURLAndType(project.getRemoteRepoPath(), type);
        if (timelines != null && !timelines.isEmpty()) {
            ProjectTimeline timeline = timelines.get(0);
            return timeline;
        }
        return null;
    }


    protected void illegalRequest(String errMsg) {
        throw new RestApiException(Response.Status.BAD_REQUEST.getStatusCode(),
                Response.Status.BAD_REQUEST.getStatusCode(), errMsg, errMsg,
                uriInfo.getAbsolutePath().toString());
    }
}
