package gr.uom.teohaik.seagle.v3.ws.rest.services;

import gr.uom.teohaik.seagle.services.ProjectService;
import gr.uom.teohaik.seagle.services.VersionService;
import gr.uom.teohaik.seagle.v3.events.ProjectAnalysisRequest;
import gr.uom.teohaik.seagle.v3.model.entities.Version;
import gr.uom.teohaik.seagle.v3.ws.rest.AbstractRestService;
import gr.uom.teohaik.seagle.v3.ws.rest.model.RESTVersionCollection;
import gr.uom.teohaik.seagle.v3.ws.rest.model.VersionDTO;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Theodore Chaikalis
 */
@Path("/versions")
@Stateless
public class VersionRestService extends AbstractRestService {

    @Inject
    ProjectService projectService;

    @Inject
    VersionService versionFacade;

    @Inject
    private Event<ProjectAnalysisRequest> analysisRequestEvent;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public Response getVersionsWS(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String pUrl) {
        // If the client didn't specified a repository url we should
        // return a 400 (Bad Request) code to inform it
        if (pUrl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponse("'purl' parameter must be specified");
            illegalRequest(errMsg);
        }

        RESTVersionCollection list = getVersions(pUrl);
        // Check if versions list is null
        // if so that means the repository was not found on
        // this server and we must return a response with
        // code 404
        if (list == null) {
            String errMsg = getErrorResponse(pUrl)
                    + " was not found on this server.";
            notFoundException(errMsg);
        }

        // We found versions here, we must return the contents to
        // the client
        return Response.ok(list).build();
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/analyzed")
    public Response getAnalyzedVersionsWS(
            @DefaultValue(DEFAULT_PURL) @QueryParam("purl") String pUrl) {

        if (pUrl.equals(DEFAULT_PURL)) {
            String errMsg = getErrorResponse("'purl' parameter must be specified");
            illegalRequest(errMsg);
        }

        RESTVersionCollection list = getAnalyzedVersions(pUrl);

        if (list == null) {
            String errMsg = getErrorResponse(pUrl)
                    + " was not found on this server.";
            notFoundException(errMsg);
        }

        return Response.ok(list).build();
    }

    private static String getErrorResponse(String pUrl) {
        return "Required repository: " + pUrl;
    }

    public RESTVersionCollection getVersions(String pUrl) {

        RESTVersionCollection versions = new RESTVersionCollection();
        for (Version v : versionFacade.findAllByProject(pUrl)) {
            versions.add(new VersionDTO(v.getId(), v.getCommitID(), v.getName(), v.getDate()));
        }

        return versions;
    }

    public RESTVersionCollection getAnalyzedVersions(String pUrl) {

        RESTVersionCollection versions = new RESTVersionCollection();
        for (Version v : versionFacade.findAnalyzedByProject(pUrl)) {
            versions.add(new VersionDTO(v.getId(), v.getCommitID(), v.getName(), v.getDate()));
        }

        return versions;
    }
}
