//package gr.uom.teohaik.seagle.test;
//
//import java.io.ByteArrayInputStream;
//import static java.nio.charset.StandardCharsets.UTF_8;
//import static org.eclipse.jgit.diff.DiffEntry.ChangeType.MODIFY;
//import static org.eclipse.jgit.diff.DiffEntry.ChangeType.RENAME;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.util.List;
//
//import org.apache.commons.io.IOUtils;
//
//import org.eclipse.jgit.api.BlameCommand;
//import org.eclipse.jgit.api.Git;
//
//import org.eclipse.jgit.api.errors.GitAPIException;
//import org.eclipse.jgit.blame.BlameResult;
//import org.eclipse.jgit.diff.DiffEntry;
//import org.eclipse.jgit.diff.DiffFormatter;
//import org.eclipse.jgit.diff.Edit.Type;
//import org.eclipse.jgit.diff.EditList;
//import org.eclipse.jgit.diff.RawTextComparator;
//import org.eclipse.jgit.lib.ObjectId;
//import org.eclipse.jgit.lib.ObjectLoader;
//import org.eclipse.jgit.lib.ObjectReader;
//import org.eclipse.jgit.lib.Repository;
//import org.eclipse.jgit.patch.FileHeader;
//import org.eclipse.jgit.revwalk.RevCommit;
//import org.eclipse.jgit.revwalk.RevTree;
//import org.eclipse.jgit.revwalk.RevWalk;
//import org.eclipse.jgit.treewalk.AbstractTreeIterator;
//import org.eclipse.jgit.treewalk.CanonicalTreeParser;
//import org.eclipse.jgit.treewalk.TreeWalk;
//import org.eclipse.jgit.treewalk.filter.PathFilter;
//import org.eclipse.jgit.util.io.DisabledOutputStream;
//import org.eclipse.jgit.util.io.NullOutputStream;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Ignore;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.TemporaryFolder;
//
//public class DiffLearningDemo {
//
//    @Rule
//    public TemporaryFolder tempFolder = new TemporaryFolder();
//
//    private Git git;
//
//    @Before
//    public void setUp() throws GitAPIException {
//        git = Git.init().setDirectory(tempFolder.getRoot()).call();
//    }
//
//    @After
//    public void tearDown() {
//        git.getRepository().close();
//    }
//
//    @Test
//    public void simplestDiffCommand() throws Exception {
//        writeFile("file.txt", "existing line\n");
//        git.add().addFilepattern("file.txt").call();
//        writeFile("file.txt", "existing line\nadded line");
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//
//        List<DiffEntry> diffEntries = git.diff().setOutputStream(outputStream).call();
//
//        assertTrue(outputStream.toByteArray().length > 0);
//        assertEquals(1, diffEntries.size());
//        assertEquals("file.txt", diffEntries.get(0).getOldPath());
//        assertEquals("file.txt", diffEntries.get(0).getNewPath());
//        assertEquals(MODIFY, diffEntries.get(0).getChangeType());
//    }
//
//    @Test
//    public void diffCommandWithoutOutput() throws Exception {
//        writeFile("file.txt", "existing line\n");
//        git.add().addFilepattern("file.txt").call();
//        writeFile("file.txt", "existing line\nadded line");
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//
//        List<DiffEntry> diffEntries = git.diff()
//                .setOutputStream(outputStream)
//                .setShowNameAndStatusOnly(true)
//                .call();
//
//        assertEquals(0, outputStream.toByteArray().length);
//        assertEquals(1, diffEntries.size());
//    }
//
//    @Test
//    public void diffCommandWithoutChanges() throws Exception {
//        List<DiffEntry> diffEntries = git.diff().call();
//
//        assertEquals(0, diffEntries.size());
//    }
//
//    @Test
//    public void diffUnchanged() throws Exception {
//        writeFile("file.txt", "existing line\n");
//        git.add().addFilepattern("file.txt").call();
//
//        List<DiffEntry> diffEntries = git.diff().call();
//
//        assertTrue(diffEntries.isEmpty());
//    }
//
//    @Test
//    public void diffTwoCommits() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt",
//                "existing line1\n"
//                + "existing line2"
//                + "\n");
//        RevCommit newCommit = commitFile("file.txt",
//                "existing line1\n"
//                + "existing line2 with a change\n"
//                + "added line1\nadded line2"
//                + "\n");
//
//        List<DiffEntry> diffEntries = git.diff()
//                .setOldTree(getCanonicalTreeParser(oldCommit))
//                .setNewTree(getCanonicalTreeParser(newCommit))
//                .call();
//
//        assertEquals(1, diffEntries.size());
//        assertEquals("file.txt", diffEntries.get(0).getOldPath());
//        assertEquals(MODIFY, diffEntries.get(0).getChangeType());
//
//        DiffFormatter formatter = new DiffFormatter(System.out);
//        formatter.setRepository(git.getRepository());
//
//        List<DiffEntry> diffEntries2 = formatter.scan(getCanonicalTreeParser(oldCommit), getCanonicalTreeParser(newCommit));
//
//        for (DiffEntry dif : diffEntries2) {
//
//            System.out.println(dif.toString());
//            formatter.format(dif);
//
//        }
//
//        BlameCommand blamer = new BlameCommand(git.getRepository());
//        ObjectId commitID = git.getRepository().resolve("HEAD");
//        blamer.setStartCommit(commitID);
//        blamer.setFilePath("file.txt");
//        BlameResult blame = blamer.call();
//
//        // read the number of lines from the given revision, this excludes changes from the last two commits due to the "~~" above
//        int lines = countLinesOfFileInCommit(git.getRepository(), commitID, "file.txt");
//        for (int i = 0; i < lines; i++) {
//            RevCommit commit = blame.getSourceCommit(i);
//            System.out.println("Line: " + i + ": " + commit.name());
//        }
//
//        System.out.println("Displayed commits responsible for " + lines + " lines");
//    }
//
//    private static int countLinesOfFileInCommit(Repository repository, ObjectId commitID, String name) throws IOException {
//        try (RevWalk revWalk = new RevWalk(repository)) {
//            RevCommit commit = revWalk.parseCommit(commitID);
//            RevTree tree = commit.getTree();
//            System.out.println("Having tree: " + tree);
//
//            // now try to find a specific file
//            try (TreeWalk treeWalk = new TreeWalk(repository)) {
//                treeWalk.addTree(tree);
//                treeWalk.setRecursive(true);
//                treeWalk.setFilter(PathFilter.create(name));
//                if (!treeWalk.next()) {
//                    throw new IllegalStateException("Did not find expected file " + name);
//                }
//
//                ObjectId objectId = treeWalk.getObjectId(0);
//                ObjectLoader loader = repository.open(objectId);
//
//                // load the content of the file into a stream
//                ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                loader.copyTo(stream);
//
//                revWalk.dispose();
//
//                return IOUtils.readLines(new ByteArrayInputStream(stream.toByteArray()), "UTF-8").size();
//            }
//        }
//    }
//
//    @Test
//    public void diffWithPathFilter() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt", "content");
//        RevCommit newCommit = commitFile("file.txt", "changed content");
//
//        List<DiffEntry> diffEntries = git.diff()
//                .setOldTree(getCanonicalTreeParser(oldCommit))
//                .setNewTree(getCanonicalTreeParser(newCommit))
//                .setPathFilter(PathFilter.create("other-file.txt"))
//                .call();
//
//        assertEquals(0, diffEntries.size());
//    }
//
//    @Test
//    public void insertLine() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt", "line1\nline3\n");
//        RevCommit newCommit = commitFile("file.txt", "line1\nline2\nline3\n");
//        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit);
//        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit);
//
//        EditList editList = computeEditList(oldTreeIterator, newTreeIterator);
//
//        assertEquals(1, editList.size());
//        assertEquals(Type.INSERT, editList.get(0).getType());
//        assertEquals(1, editList.get(0).getBeginA());
//        assertEquals(1, editList.get(0).getEndA());
//        assertEquals(1, editList.get(0).getBeginB());
//        assertEquals(2, editList.get(0).getEndB());
//    }
//
//    @Test
//    public void deleteLine() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt", "line1\nline2\nline3\n");
//        RevCommit newCommit = commitFile("file.txt", "line1\nline3\n");
//        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit);
//        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit);
//
//        EditList editList = computeEditList(oldTreeIterator, newTreeIterator);
//
//        assertEquals(1, editList.size());
//        assertEquals(Type.DELETE, editList.get(0).getType());
//        assertEquals(1, editList.get(0).getBeginA());
//        assertEquals(2, editList.get(0).getEndA());
//        assertEquals(1, editList.get(0).getBeginB());
//        assertEquals(1, editList.get(0).getEndB());
//    }
//
//    @Test
//    public void modifyLine() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt", "line1\nline2\nline3\n");
//        RevCommit newCommit = commitFile("file.txt", "line1\nline2 and more\nline3\n");
//        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit);
//        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit);
//
//        EditList editList = computeEditList(oldTreeIterator, newTreeIterator);
//
//        assertEquals(1, editList.size());
//        assertEquals(Type.REPLACE, editList.get(0).getType());
//        assertEquals(1, editList.get(0).getBeginA());
//        assertEquals(2, editList.get(0).getEndA());
//        assertEquals(1, editList.get(0).getBeginB());
//        assertEquals(2, editList.get(0).getEndB());
//    }
//
//    @Test
//    public void diffFormatter() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt", "existing line\n");
//        RevCommit newCommit = commitFile("file.txt", "existing line\nadded line\n");
//        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit);
//        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit);
//
//        DiffFormatter diffFormatter = new DiffFormatter(NullOutputStream.INSTANCE);
//        diffFormatter.setRepository(git.getRepository());
//        List<DiffEntry> diffEntries = diffFormatter.scan(oldTreeIterator, newTreeIterator);
//        diffFormatter.close();
//
//        assertEquals(1, diffEntries.size());
//    }
//
//    @Test
//    public void diffFormatterWithRename() throws Exception {
//        writeFile("oldfile.txt", "unchanged content");
//        git.add().addFilepattern("oldfile.txt").call();
//        RevCommit oldCommit = git.commit().setMessage("old commit").call();
//        writeFile("newfile.txt", "unchanged content");
//        git.rm().addFilepattern("oldfile.txt").call();
//        git.add().addFilepattern("newfile.txt").call();
//        RevCommit newCommit = git.commit().setMessage("new commit").call();
//        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit);
//        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit);
//
//        DiffFormatter diffFormatter = new DiffFormatter(NullOutputStream.INSTANCE);
//        diffFormatter.setRepository(git.getRepository());
//        diffFormatter.setDetectRenames(true);
//        List<DiffEntry> diffEntries = diffFormatter.scan(oldTreeIterator, newTreeIterator);
//        diffFormatter.close();
//
//        assertEquals(1, diffEntries.size());
//        assertEquals(RENAME, diffEntries.get(0).getChangeType());
//        assertEquals("oldfile.txt", diffEntries.get(0).getOldPath());
//        assertEquals("newfile.txt", diffEntries.get(0).getNewPath());
//    }
//
//    @Ignore // see https://dev.eclipse.org/mhonarc/lists/jgit-dev/msg03166.html
//    @Test
//    public void diffFormatterWithIgnoreWhitespace() throws Exception {
//        RevCommit oldCommit = commitFile("file.txt", "first line\n");
//        RevCommit newCommit = commitFile("file.txt", "first line \n");
//        AbstractTreeIterator oldTreeIterator = getCanonicalTreeParser(oldCommit);
//        AbstractTreeIterator newTreeIterator = getCanonicalTreeParser(newCommit);
//
//        DiffFormatter diffFormatter = new DiffFormatter(NullOutputStream.INSTANCE);
//        diffFormatter.setRepository(git.getRepository());
//        diffFormatter.setDiffComparator(RawTextComparator.WS_IGNORE_ALL);
//        List<DiffEntry> diffEntries = diffFormatter.scan(oldTreeIterator, newTreeIterator);
//        diffFormatter.close();
//
//        assertEquals(0, diffEntries.size());
//    }
//
//    private RevCommit commitFile(String name, String content) throws IOException, GitAPIException {
//        writeFile(name, content);
//        git.add().addFilepattern(name).call();
//        return git.commit().setMessage("commit message").call();
//    }
//
//    private EditList computeEditList(AbstractTreeIterator oldTreeIterator,
//            AbstractTreeIterator newTreeIterator)
//            throws IOException {
//        try (DiffFormatter diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE)) {
//            diffFormatter.setRepository(git.getRepository());
//            List<DiffEntry> diffEntries = diffFormatter.scan(oldTreeIterator, newTreeIterator);
//            FileHeader fileHeader = diffFormatter.toFileHeader(diffEntries.get(0));
//            return fileHeader.toEditList();
//        }
//    }
//
//    private File writeFile(String name, String content) throws IOException {
//        File file = new File(git.getRepository().getWorkTree(), name);
//        try (FileOutputStream outputStream = new FileOutputStream(file)) {
//            outputStream.write(content.getBytes(UTF_8));
//        }
//        return file;
//    }
//
//    private AbstractTreeIterator getCanonicalTreeParser(ObjectId commitId) throws IOException {
//        try (RevWalk walk = new RevWalk(git.getRepository())) {
//            RevCommit commit = walk.parseCommit(commitId);
//            ObjectId treeId = commit.getTree().getId();
//            try (ObjectReader reader = git.getRepository().newObjectReader()) {
//                return new CanonicalTreeParser(null, reader, treeId);
//            }
//        }
//    }
//
//}
