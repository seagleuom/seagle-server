package gr.uom.teohaik.seagle.test;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class GenericTests {


    @Test
    public void testCounter(){
        int[] counter = new int[1];

        for(int i=0; i<50; i++){
            System.out.println("counter = "+counter[0]);
            increaseCounterAndFlush(counter);
            System.out.println("counter after = "+counter[0]);
        }
        assertNotNull (counter);
    }



    private void increaseCounterAndFlush(int[] counter) {
        if ( ++counter[0] % 5 == 0 ) {
            System.out.println("Counter = "+counter[0]+" FLUSH!");
        }
    }

}
